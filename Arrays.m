//
//  Arrays.m
//  mutnav2
//
//  Created by Shaima Magdi on 11/17/14.
//
//

#import "Arrays.h"

@implementation Arrays
/************************************* ZYARA ********************************************************/
+ (NSArray *)zyara
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"fadlMsgdNabawy",@"ahkamWadab",@"akhtaSha3a",nil];
    }
    return theArray;
}
// arrays zyara array content
+ (NSArray *)fadlMsgdNabawy
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"zyarafadelMasged1",@"zyarafadelMasged2",@"zyarafadelMasged3",@"zyarafadelMasged4",nil];
    }
    return theArray;
}
+ (NSArray *)ahkamWadab
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"zyaraA7kam1",@"zyaraA7kam2",@"zyaraA7kam3",@"zyaraA7kam4",@"zyaraA7kam5",@"zyaraA7kam6",@"zyaraA7kam7",nil];
    }
    return theArray;
}
+ (NSArray *)akhtaSha3a
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"zyaraA5taa1",@"zyaraA5taa2",@"zyaraA5taa3",@"zyaraA5taa4",@"zyaraA5taa5",@"zyaraA5taa6",@"zyaraA5taa7",@"zyaraA5taa8",nil];
    }
    return theArray;
}
/******************************************** SE7A **************************************************/
+ (NSArray *)se7a
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"healthyGuide",@"hospitals",nil];
    }
    return theArray;
}
// arrays of zyara array content

+ (NSArray *)healthyGuide
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"beforeHajj",@"inHajj",@"amradElHajj",@"specialCases" ,@"womanAndHajj",nil];
    }
    return theArray;
}
+ (NSArray *)hospitals
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"modryaSho2onS7yaName",@"markMakaTebyName",@"malk3abdElAzezName",@"malikFisalName",@"atfalHospitalName",@"ebnSenaHospitalName",@"ahlySoudiHospitalName",@"agiadHospitalName",@"hara2HospitalName",@"elRfe3HospitalName",@"alSalamHospitalName",@"elShfa2HospitalName",@"tonisiHospitalName",@"omElQoraHospitalName",@"pashra7ilyHospitalName",nil];
    }
    return theArray;
}
// arrays of hospitals array content then Dictionary of this arrays

+ (NSArray *)modryaSho2onS7yaName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"modryaSho2onS7yaDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)modryaSho2onS7yaDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"modryaSho2onS7yaName",@"modryaSho2onS7yaPhone",@"modryaSho2onS7yaFax",@"modryaSho2onS7yaAddress",@"modryaSho2onS7yaBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)markMakaTebyName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"markMakaTebyDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)markMakaTebyDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"markMakaTebyName",@"markMakaTebyPhone",@"markMakaTebyFax",@"markMakaTebyAddress",@"markMakaTebyBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)malk3abdElAzezName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"malk3abdElAzezDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)malk3abdElAzezDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"malk3abdElAzezName",@"malk3abdElAzezPhone",@"malk3abdElAzezFax",@"malk3abdElAzezAddress",@"malk3abdElAzezBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}

+ (NSArray *)malikFisalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"malikFisalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)malikFisalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"malikFisalName",@"malikFisalPhone",@"malikFisalFax",@"malikFisalAddress",@"malikFisalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)atfalHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"atfalHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)atfalHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"atfalHospitalName",@"atfalHospitalPhone",@"atfalHospitalFax",@"atfalHospitalAddress",@"atfalHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}

+ (NSArray *)ebnSenaHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"ebnSenaHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)ebnSenaHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"ebnSenaHospitalName",@"ebnSenaHospitalPhone",@"ebnSenaHospitalFax",@"ebnSenaHospitalAddress",@"ebnSenaHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}

+ (NSArray *)ahlySoudiHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"ahlySoudiHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)ahlySoudiHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"ahlySoudiHospitalName",@"ahlySoudiHospitalPhone",@"ahlySoudiHospitalFax",@"ahlySoudiHospitalAddress",@"ahlySoudiHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)agiadHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"agiadHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)agiadHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"agiadHospitalName",@"agiadHospitalPhone",@"agiadHospitalFax",@"agiadHospitalAddress",@"agiadHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)hara2HospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"hara2HospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)hara2HospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"hara2HospitalName",@"hara2HospitalPhone",@"hara2HospitalFax",@"hara2HospitalAddress",@"hara2HospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)elRfe3HospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"elRfe3HospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)elRfe3HospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"elRfe3HospitalName",@"elRfe3HospitalPhone",@"elRfe3HospitalFax",@"elRfe3HospitalAddress",@"elRfe3HospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)alSalamHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"alSalamHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)alSalamHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"alSalamHospitalName",@"alSalamHospitalPhone",@"alSalamHospitalFax",@"alSalamHospitalAddress",@"alSalamHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)elShfa2HospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"elShfa2HospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)elShfa2HospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"elShfa2HospitalName",@"elShfa2HospitalPhone",@"elShfa2HospitalFax",@"elShfa2HospitalAddress",@"elShfa2HospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)tonisiHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"tonisiHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)tonisiHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"tonisiHospitalName",@"tonisiHospitalPhone",@"tonisiHospitalFax",@"tonisiHospitalAddress",@"tonisiHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)omElQoraHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"omElQoraHospitalDic",nil];
    }
    return theArray;
}
+ (NSDictionary *)omElQoraHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"omElQoraHospitalName",@"omElQoraHospitalPhone",@"omElQoraHospitalFax",@"omElQoraHospitalAddress",@"omElQoraHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}
+ (NSArray *)pashra7ilyHospitalName
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"info",@"pashra7ilyHospitalDic",nil];
    }
    return theArray;
}

+ (NSDictionary *)pashra7ilyHospitalDic
{
    static NSDictionary *theDictionary;
    if (!theDictionary)
    {
        theDictionary =[NSDictionary dictionaryWithObjects:@[@"pashra7ilyHospitalName",@"pashra7ilyHospitalPhone",@"pashra7ilyHospitalFax",@"pashra7ilyHospitalAddress",@"pashra7ilyHospitalBeds"] forKeys:@[@"name",@"phone",@"fax",@"address",@"beds"]];
    }
    return theDictionary;
}

//arrays of healthyGuide content then arrays of its strings
/********beforeHaJ********/
+ (NSArray *)beforeHajj
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"tat3emat",@"HakebaTbya",@"btakaTbya",@"mwadNdafa" ,@"mzalaShamsya",@"mlabsMonasba",nil];
    }
    return theArray;
}
+ (NSArray *)tat3emat
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"homaShawkyaTat3em",@"flu",@"elthabR2way",@"homaSafra2",nil];
    }
    return theArray;
}

+ (NSArray *)homaShawkyaTat3em
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"homaShawkyaTat3emStr",nil];
    }
    return theArray;
}

+ (NSArray *)flu
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"fluStr",nil];
    }
    return theArray;
}
+ (NSArray *)elthabR2way
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"elthabR2wayStr",nil];
    }
    return theArray;
}
+ (NSArray *)homaSafra2
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"homaSafra2Str",nil];
    }
    return theArray;
}
+ (NSArray *)HakebaTbya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"HakebaTbyaStr",nil];
    }
    return theArray;
}
+ (NSArray *)btakaTbya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"btakaTbyaStr",nil];
    }
    return theArray;
}
+ (NSArray *)mwadNdafa
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"mwadNdafaStr",nil];
    }
    return theArray;
}
+ (NSArray *)mzalaShamsya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"mzalaShamsyaStr",nil];
    }
    return theArray;
}
+ (NSArray *)mlabsMonasba
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"mlabsMonasbaStr",nil];
    }
    return theArray;
}

/********inHajj********/
+ (NSArray *)inHajj
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"ta3am",@"meyah",@"amnWsalama" ,@"nadafaSh5sya",@"eghad",@"tad5en",@"adwya",@"Helaqa",nil];
    }
    return theArray;
}
+ (NSArray *)ta3am
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"ta3amStr",nil];
    }
    return theArray;
}
+ (NSArray *)meyah
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"meyahStr",nil];
    }
    return theArray;
}

+ (NSArray *)amnWsalama
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"hara2eq",@"kosor",@"Hawadeth",nil];
    }
    return theArray;
}
+ (NSArray *)hara2eq
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"hara2eqStr",nil];
    }
    return theArray;
}
+ (NSArray *)kosor
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"kosorStr",nil];
    }
    return theArray;
}
+ (NSArray *)Hawadeth
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"HawadethStr",nil];
    }
    return theArray;
}
+ (NSArray *)nadafaSh5sya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"nadafaSh5syaStr",nil];
    }
    return theArray;
}
+ (NSArray *)eghad
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"eghadStr",nil];
    }
    return theArray;
}
+ (NSArray *)tad5en
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"tad5enStr",nil];
    }
    return theArray;
}
+ (NSArray *)adwya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"adwyaStr",nil];
    }
    return theArray;
}
+ (NSArray *)Helaqa
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"HelaqaStr",nil];
    }
    return theArray;
}


/********amradElHajj********/
+ (NSArray *)amradElHajj
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"elgehazTanfosy",@"nazlatM3wya",@"amradGldya",@"esabatHrarya",@"homaShawkyaAmradHaj" ,@"eghad3adly",@"maradAfterHajj",nil];
    }
    return theArray;
}
+ (NSArray *)elgehazTanfosy
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"elgehazTanfosyStr",nil];
    }
    return theArray;
}
+ (NSArray *)nazlatM3wya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"nazlatM3wyaStr",nil];
    }
    return theArray;
}

+ (NSArray *)amradGldya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"amradGldyaStr",nil];
    }
    return theArray;
}

+ (NSArray *)esabatHrarya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"esabatHraryaStr",nil];
    }
    return theArray;
}

+ (NSArray *)homaShawkyaAmradHaj
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"homaShawkyaAmradHajStr",nil];
    }
    return theArray;
}

+ (NSArray *)eghad3adly
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"eghad3adlyStr",nil];
    }
    return theArray;
}

+ (NSArray *)maradAfterHajj
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"maradAfterHajjStr",nil];
    }
    return theArray;
}



/********specialCases********/

+ (NSArray *)specialCases
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"sokary",@"rabw",@"heart",@"hasasya",@"kela" ,@"sar3",nil];
    }
    return theArray;
}
+ (NSArray *)sokary
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"sokaryStr",nil];
    }
    return theArray;
}
+ (NSArray *)rabw
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"rabwStr",nil];
    }
    return theArray;
}
+ (NSArray *)heart
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"heartStr",nil];
    }
    return theArray;
}
+ (NSArray *)hasasya
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"hasasyaStr",nil];
    }
    return theArray;
}
+ (NSArray *)kela
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"kelaStr",nil];
    }
    return theArray;
}
+ (NSArray *)sar3
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"sar3Str",nil];
    }
    return theArray;
}
/********womanAndHajj********/

+ (NSArray *)womanAndHajj
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"period",@"pregnancy",nil];
    }
    return theArray;
}
+ (NSArray *)period
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"periodStr",nil];
    }
    return theArray;
}
+ (NSArray *)pregnancy
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"pregnancyStr",nil];
    }
    return theArray;
}
////**********************************GANA2EZ************************************************////
+ (NSArray *)gna2ez
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"list",@"fadlSalatELGnaza",@"hokmSalatELGnaza",@"kayfytSalatELGnaza",@"fawatElTakberat",nil];
    }
    return theArray;
}
+ (NSArray *)fadlSalatELGnaza
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"fadlSalatELGnazaStr",nil];
    }
    return theArray;
}
+ (NSArray *)hokmSalatELGnaza
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"hokmSalatELGnazaStr",nil];
    }
    return theArray;
}
+ (NSArray *)kayfytSalatELGnaza
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"kayfytSalatELGnazaStr1",@"kayfytSalatELGnazaStr2",@"kayfytSalatELGnazaStr3",@"kayfytSalatELGnazaStr4",@"kayfytSalatELGnazaStr5",nil];
    }
    return theArray;
}
+ (NSArray *)fawatElTakberat
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"text",@"fawatElTakberatStr",nil];
    }
    return theArray;
}
////********************************** MAPS ****************************************maps********////
+ (NSArray *)maps
{
    static NSArray *theArray;
    if (!theArray)
    {
        //theArray = [[NSArray alloc] initWithObjects:@"list",@"mwaqetElE7ram",@"makaMap",@"menaMap",@"mozdlifaMap",@"arafatMap",@"mo5ttElMash3er",nil];
        theArray = [[NSArray alloc] initWithObjects:@"list",@"makaMap",@"menaMap",@"mozdlifaMap",@"arafatMap",nil];
    }
    return theArray;
}
+ (NSArray *)mwaqetElE7ram
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"map",@"mwaqetElE7ram",nil];
    }
    return theArray;
}
+ (NSArray *)makaMap
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"map",@"makaMap",nil];
    }
    return theArray;
}
+ (NSArray *)menaMap
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"map",@"menaMap",nil];
    }
    return theArray;
}
+ (NSArray *)mozdlifaMap
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"map",@"mozdlifaMap",nil];
    }
    return theArray;
}
+ (NSArray *)arafatMap
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"map",@"arafatMap",nil];
    }
    return theArray;
}
+ (NSArray *)mo5ttElMash3er
{
    static NSArray *theArray;
    if (!theArray)
    {
        theArray = [[NSArray alloc] initWithObjects:@"map",@"mo5ttElMash3er",nil];
    }
    return theArray;
}


@end
