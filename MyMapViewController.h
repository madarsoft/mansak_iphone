//
//  MyMapViewController.h
//  mutnav2
//
//  Created by Shaima Magdi on 11/20/14.
//
//

/*!
 @discussion This class display map of selected index it can be mapKit or image display map.
        every map has plist file hold "CLLocationCoordinate2D" for target places.
 
 
 use this class :-
 1- You just need to import it to your view.
 2- set index+1 to selectedRow.. (the same logic first index hodl type "map").
 3- if you want to reimplement this class --> change location and switch cases acording to your modification .. you need to modify plist file and images.
 4-set the other items (mapTitle) before push this view.
 
 */

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MyMapViewController : UIViewController<MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

/**
 *  @brief MapViewController NSString this display map name .. was set before push this view  .
 */
@property(strong,nonatomic)NSString *mapTitle;

/**
 *  @brief MapViewController int this indicate which user select (map or image)  .
 */
@property int selectedRow;

@end
