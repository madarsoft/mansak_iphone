//
//  HamalListViewController.m
//  mutnav2
//
//  Created by waleed on 6/4/13.
//
//

#import "HamalListViewController.h"
#import "HamalDetailViewController.h"
#import "draw2D.h"

@interface HamalListViewController ()

@end

@implementation HamalListViewController
@synthesize bgImage,mainTable,dataArr,infoArr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    for (int i=0; i<5; i++) {
        dataArr = [[NSMutableArray alloc]init];
        UIView *vv = [self drawItemCell];
        [dataArr addObject:vv];
        
    }
	// Do any additional setup after loading the view.
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,420*highf);
    [self.view addSubview:bgImage];
    
    UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 40*highf)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor greenColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"نتائج البحث";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLabel];
    
    UIImageView *hheadImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hHead.png"]];
    hheadImg.frame=CGRectMake(0, 40, 320, 30*highf);
    [self.view addSubview:hheadImg];
    
    UIImageView *hh2Img =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hHead2.png"]];
    hh2Img.frame=CGRectMake(0, 70, 320, 15*highf);
    [self.view addSubview:hh2Img];
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0,90,320,320) style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.rowHeight=110;
  //  mainTable.separatorColor = [UIColor blackColor];
    [self.view addSubview:self.mainTable];
    
    
   

}

-(NSMutableArray *)getData{
    infoArr = [[NSMutableArray alloc]init];
    return infoArr;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    HamalDetailViewController *detailViewController = [[HamalDetailViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    [self.navigationController pushViewController:detailViewController animated:YES];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        // the cell is being recycled, remove old embedded controls
        UIView *viewToRemove = nil;
        viewToRemove = [cell.contentView viewWithTag:kMyTag];
        if (viewToRemove)
            [viewToRemove removeFromSuperview];
    }
    UIView *  tmpView = [self drawItemCell];
   //     tmpView = [self.dataArr objectAtIndex:indexPath.row];
    
    
    [cell.contentView addSubview:tmpView];
    return cell;
}

-(UIView *)drawItemCell{
    CGFloat ff=0;
    CGRect frame = CGRectMake(0, 0, 320, 110);
    
    UIView *itemView = [[UIView alloc] initWithFrame:frame];
    itemView.backgroundColor = [UIColor clearColor];
    
    UIImageView *nameBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"nameLab.png"]];
    nameBg.frame= CGRectMake(200, ff, 110, 25);
    [itemView addSubview:nameBg];
    
    UILabel *nameLab = [[ UILabel alloc] initWithFrame:CGRectMake(200, ff, 100, 25)];
    nameLab.backgroundColor = [UIColor clearColor];
    nameLab.textColor = [UIColor whiteColor];
    nameLab.textAlignment = NSTextAlignmentRight;
    nameLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    nameLab.text =@"حملة الإيمان";
    [itemView addSubview:nameLab];
   
    
    ff += 25;
    
    UILabel *townLab = [[ UILabel alloc] initWithFrame:CGRectMake(40, ff, 60, 20)];
    townLab.textColor = [UIColor blackColor];
    townLab.backgroundColor = [UIColor clearColor];
    townLab.textAlignment = NSTextAlignmentRight;
    // timeLab.text = @"9:15  8:30";
    townLab.font = [UIFont fontWithName:@"Arial" size:14];
    [itemView addSubview:townLab];
 
    UILabel *townBg = [[ UILabel alloc] initWithFrame:CGRectMake(100, ff, 50, 20)];
    townBg.textColor = [UIColor colorWithRed:0.122 green:0.565 blue:0.592 alpha:1.0];
    townBg.backgroundColor = [UIColor clearColor];
    townBg.textAlignment = NSTextAlignmentRight;
    townBg.text = @"المدينة:";
    // timeLab.text = @"9:15  8:30";
    townBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:townBg];
 
    
    
    UILabel *stateLab = [[ UILabel alloc] initWithFrame:CGRectMake(160, ff, 90, 20)];
    stateLab.textColor = [UIColor blackColor];
    stateLab.backgroundColor = [UIColor clearColor];
    stateLab.textAlignment = NSTextAlignmentRight;
    // timeLab.text = @"9:15  8:30";
    stateLab.font = [UIFont fontWithName:@"Arial" size:14];
    [itemView addSubview:stateLab];
    UILabel *stateBg = [[ UILabel alloc] initWithFrame:CGRectMake(260, ff, 50, 20)];
    stateBg.textColor = [UIColor colorWithRed:0.122 green:0.565 blue:0.592 alpha:1.0];
    stateBg.backgroundColor = [UIColor clearColor];
    stateBg.textAlignment = NSTextAlignmentRight;
    stateBg.text = @"االدولة:";
    // timeLab.text = @"9:15  8:30";
    stateBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:stateBg];
    
    ff += 25;
    
    
    UILabel *distLab = [[ UILabel alloc] initWithFrame:CGRectMake(40, ff, 50, 20)];
    distLab.textColor = [UIColor blackColor];
    distLab.backgroundColor = [UIColor clearColor];
    distLab.textAlignment =NSTextAlignmentRight;
    // timeLab.text = @"9:15  8:30";
    distLab.font = [UIFont fontWithName:@"Arial" size:14];
    [itemView addSubview:distLab];
   
    UILabel *distBg = [[ UILabel alloc] initWithFrame:CGRectMake(90, ff, 50, 20)];
    distBg.textColor = [UIColor colorWithRed:0.122 green:0.565 blue:0.592 alpha:1.0];
    distBg.backgroundColor = [UIColor clearColor];
    distBg.textAlignment = NSTextAlignmentRight;
    distBg.text = @"المسافة:";
    // timeLab.text = @"9:15  8:30";
    distBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:distBg];
   
    
    
    UILabel *trainLab = [[ UILabel alloc] initWithFrame:CGRectMake(140, ff, 40, 20)];
    trainLab.textColor = [UIColor blackColor];
    trainLab.backgroundColor = [UIColor clearColor];
    trainLab.textAlignment = NSTextAlignmentRight;
    // timeLab.text = @"9:15  8:30";
    trainLab.font = [UIFont fontWithName:@"Arial" size:14];
    [itemView addSubview:trainLab];

    UILabel *trainBg = [[ UILabel alloc] initWithFrame:CGRectMake(180, ff, 50, 20)];
    trainBg.textColor = [UIColor colorWithRed:0.122 green:0.565 blue:0.592 alpha:1.0];
    trainBg.backgroundColor = [UIColor clearColor];
    trainBg.textAlignment = NSTextAlignmentRight;
    trainBg.text = @"القطار:";
    // timeLab.text = @"9:15  8:30";
    trainBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:trainBg];

    
    UILabel *classLab = [[ UILabel alloc] initWithFrame:CGRectMake(230, ff, 40, 20)];
    classLab.textColor = [UIColor blackColor];
    classLab.backgroundColor = [UIColor clearColor];
    classLab.textAlignment =NSTextAlignmentRight;
    // timeLab.text = @"9:15  8:30";
    classLab.font = [UIFont fontWithName:@"Arial" size:14];
    [itemView addSubview:classLab];

    UILabel *classBg = [[ UILabel alloc] initWithFrame:CGRectMake(270, ff, 40, 20)];
    classBg.textColor = [UIColor colorWithRed:0.122 green:0.565 blue:0.592 alpha:1.0];
    classBg.backgroundColor = [UIColor clearColor];
    classBg.textAlignment =NSTextAlignmentRight;
    classBg.text = @"القطار:";
    // timeLab.text = @"9:15  8:30";
    classBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:classBg];

    
    ff += 20;
    
    CGRect lineframe =CGRectMake(0, ff, 320, 2);
    draw2D *dd =[[draw2D alloc]initWithFrame:lineframe];
    [itemView addSubview:dd];
 
    ff += 5;
    
    
    UIButton *favBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    favBtn.frame = CGRectMake(40, ff, 30, 25);
    [favBtn setImage:[UIImage imageNamed:@"unFavBtn.png"] forState:UIControlStateNormal];
    [itemView addSubview:favBtn];
    
    
    UIImageView *priceBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"priceLab.png"]];
    priceBg.frame= CGRectMake(220, ff, 100, 25);
    [itemView addSubview:priceBg];
    
    UILabel *priceLab = [[ UILabel alloc] initWithFrame:CGRectMake(230, ff, 85, 25)];
    priceLab.backgroundColor = [UIColor clearColor];
    priceLab.textColor = [UIColor whiteColor];
    priceLab.textAlignment =NSTextAlignmentRight;
    priceLab.font = [UIFont boldSystemFontOfSize:14];
    priceLab.text = @"ألف ريال";
    [itemView addSubview:priceLab];
    
     ff += 30;
    
    CGRect lineframe2 =CGRectMake(0, ff, 320, 2);
    draw2D *dd2 =[[draw2D alloc]initWithFrame:lineframe2];
    [itemView addSubview:dd2];
  
    UIImageView *arrBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"arrow.png"]];
    arrBg.frame= CGRectMake(20,40, 15, 15);
    [itemView addSubview:arrBg];


    return itemView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
