//
//  OmraTextViewController.h
//  mutnav2
//
//  Created by walid nour on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
//#import <BugSense-iOS/BugSenseController.h>

@interface OmraTextViewController : UIViewController
<UITextViewDelegate >

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;

@property(nonatomic,retain) IBOutlet UITextView *mytext;
//@property(nonatomic,retain) IBOutlet NSMutableArray *arrdoaa;
//@property(nonatomic,retain) IBOutlet NSMutableArray *arrmanasek;
@property(nonatomic,retain) IBOutlet UISegmentedControl *segments;
@property int sizeindex;
@property(nonatomic,retain) NSMutableDictionary *sdic;
@property(nonatomic,retain) NSString *from;

@property (nonatomic, retain)NSNumber *myid;
@property (nonatomic, retain)NSString *plist;

-(void)addfiletotextview:(NSString*)filename;
-(IBAction)changefile:(id) sender;
-(IBAction)changeSize:(id)sender;
@end
