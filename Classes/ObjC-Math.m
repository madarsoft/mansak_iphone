//
//  myMath.m
//  location
//
//  Created by Radwa on ٢٣‏/٢‏/٢٠١١.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ObjC-Math.h"


@implementation myMath

#ifndef __RRR
#define __RRR
const double RTD = 180 / M_PI;
const double DTR = M_PI / 180;
#endif

 +(double) fixAngle:(double) a {
	 a = a - 360.0 * (floor(a / 360.0));
	 a = a < 0 ? a + 360.0 : a;
 return a;
 }
 
+(double) fixHours:(double) a {
	a = a - 24.0 * (floor(a / 24.0));
	a = a < 0 ? a + 24.0 : a;
 return a;
 }
 
 
 //degree Sin
+(double) dSin: (double) DegreeAngle{

	return sin(DegreeAngle * DTR);
 }
 
 //degree Cos
+(double) dCos:(double) DegreeAngle{

	return cos(DegreeAngle * DTR);
 }
 
 // degree Tan
 +(double) dTan:(double) DegreeAngle{
	
	return tan(DegreeAngle * DTR);
 }
 
+(double) dASin:(double) x{
 
 return asin(x) * RTD;
 }
 
 // degree arccos
+(double) dACos:(double) x{
 
	return acos(x) * RTD;
 }
 
 
+(double) dATan:(double) x {
	
	return atan(x) * RTD;
 
 }
 
 // degree arctan2
 +(double) dATan2:(double)y :(double)x{
	
	 return (atan2(y , x) * RTD);
 }
 
 // degree cot
+(double) cotan:(double) i {
	return(1 / tan(i * DTR)); 
 }
 
 // degree arccot
+(double) dACot:(double) x{
 
	return 90-[self dATan:x];
 }
 
 +(double) roundTime:(double) time{
	 int hours = (int)time;
	 double minuts = (time - hours)*60;
	 int min = [self round:minuts];
	 double result = hours + min/60.0;
	 return result;
 }
 
+(int) round:(double) input {
	int integer = (int)input;
	double fraction = input - integer;
 
	if(fraction >= 0.5){
		integer=integer+1;
    }
	return integer;
 
 }
 
 
 //normalize value to be in range 0....1
+(double) normalize:(double) x{
	x = x - (int)x;
	return x < 0 ? x + 1 : x;
 
 }

 +(double)degreeToRadian:(double)d
{
     return d * M_PI / 180;
}

+(double)radiansToDegree:(double)r
{
    return r * 180 / M_PI;
}

@end
