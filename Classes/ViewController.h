//
//  ViewController.h
//  twitternew
//
//  Created by mohamed rashad on 7/8/13.
//  Copyright (c) 2013 mohamed rashad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    NSString *searchtext;
    NSArray *searchlist;
}

- (IBAction)mwaqf:(id)sender;
- (IBAction)memories:(id)sender;
- (IBAction)help:(id)sender;
-(void)getplist;
-(BOOL) checkaccount;
@property (retain, nonatomic) IBOutlet UIButton *help;
@property (retain, nonatomic) IBOutlet UIButton *memories;
@property (retain, nonatomic) IBOutlet UIButton *position;
@property (retain, nonatomic) IBOutlet UITableView *searchtable;
@property (retain, nonatomic) NSArray *searchlist;
@end
