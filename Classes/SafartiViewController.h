//
//  SafartiViewController.h
//  mutnav2
//
//  Created by walid nour on 7/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import <BugSense-iOS/BugSenseController.h>


@class Reachability;

@interface SafartiViewController : UIViewController
<UIWebViewDelegate>

@property (nonatomic, retain) NSString *htmlString;

@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) IBOutlet UIImageView *bgImage;




-(void)getDetails;
-(void)drawView;
@end
