//
//  HamalDetailViewController.m
//  mutnav2
//
//  Created by waleed on 6/5/13.
//
//

#import "HamalDetailViewController.h"

@interface HamalDetailViewController ()

@end

@implementation HamalDetailViewController
@synthesize bgImage;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,420*highf);
   // [self.view addSubview:bgImage];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 40*highf)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor greenColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"نتائج البحث";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLabel];
	// Do any additional setup after loading the view.
    UIImageView *bggImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"detailBg.png"]];
    bggImg.frame=CGRectMake(0, 40, 320, 195*highf);
    [self.view addSubview:bggImg];
    
    UIImageView *lineImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"detailLine.png"]];
    lineImg.frame=CGRectMake(0, 235, 320, 20*highf);
    [self.view addSubview:lineImg];
    
    UIImageView *priImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"price.png"]];
    priImg.frame=CGRectMake(0, 40, 95, 65*highf);
    [self.view addSubview:priImg];
    
    UILabel *state = [[UILabel alloc]initWithFrame:CGRectMake(100, 50, 140, 25)];
    state.backgroundColor = [UIColor clearColor];
    state.text = @"السعودية";
    state.textColor =[UIColor whiteColor];
    state.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:state];
    UILabel *statelab = [[UILabel alloc]initWithFrame:CGRectMake(240, 50, 70, 25)];
    statelab.backgroundColor = [UIColor clearColor];
    statelab.text = @"الدولة:";
    statelab.textAlignment=NSTextAlignmentRight;
    statelab.textColor =[UIColor colorWithRed:0.925 green:0.671 blue:0.004 alpha:1.0];
    [self.view addSubview:statelab];
    
    UILabel *city = [[UILabel alloc]initWithFrame:CGRectMake(100, 75, 140, 25)];
    city.backgroundColor = [UIColor clearColor];
    city.text = @"الرياض";
    city.textColor =[UIColor whiteColor];
    city.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:city];
    UILabel *citylab = [[UILabel alloc]initWithFrame:CGRectMake(240,75, 70, 25)];
    citylab.backgroundColor = [UIColor clearColor];
    citylab.text = @"المدينة:";
    citylab.textAlignment=NSTextAlignmentRight;
    citylab.textColor =[UIColor colorWithRed:0.925 green:0.671 blue:0.004 alpha:1.0];
    [self.view addSubview:citylab];
    
    UILabel *class = [[UILabel alloc]initWithFrame:CGRectMake(100, 100, 140, 25)];
    class.backgroundColor = [UIColor clearColor];
    class.text = @"الرياض";
    class.textColor =[UIColor whiteColor];
     class.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:class];
    UILabel *classlab = [[UILabel alloc]initWithFrame:CGRectMake(240,100, 70, 25)];
    classlab.backgroundColor = [UIColor clearColor];
    classlab.text = @"الفئة:";
    classlab.textAlignment=NSTextAlignmentRight;
    classlab.textColor =[UIColor colorWithRed:0.925 green:0.671 blue:0.004 alpha:1.0];
    [self.view addSubview:classlab];
    
    UILabel *train = [[UILabel alloc]initWithFrame:CGRectMake(100, 125, 140, 25)];
    train.backgroundColor = [UIColor clearColor];
    train.text = @"الرياض";
    train.textColor =[UIColor whiteColor];
    train.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:train];
    UILabel *trainlab = [[UILabel alloc]initWithFrame:CGRectMake(240,125, 70, 25)];
    trainlab.backgroundColor = [UIColor clearColor];
    trainlab.text = @"قطار:";
    trainlab.textAlignment=NSTextAlignmentRight;
    trainlab.textColor =[UIColor colorWithRed:0.925 green:0.671 blue:0.004 alpha:1.0];
    [self.view addSubview:trainlab];
    
    UILabel *dist = [[UILabel alloc]initWithFrame:CGRectMake(100, 150, 140, 25)];
    dist.backgroundColor = [UIColor clearColor];
    dist.text = @"الرياض";
    dist.textColor =[UIColor whiteColor];
    dist.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:dist];
    UILabel *distlab = [[UILabel alloc]initWithFrame:CGRectMake(240,150, 70, 25)];
    distlab.backgroundColor = [UIColor clearColor];
    distlab.text = @"المسافة:";
    distlab.textAlignment=NSTextAlignmentRight;
    distlab.textColor =[UIColor colorWithRed:0.925 green:0.671 blue:0.004 alpha:1.0];
    [self.view addSubview:distlab];
    
    UIButton *mapBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    mapBtn.frame=CGRectMake(25, 150, 25, 25);
    [mapBtn setImage:[UIImage imageNamed:@"maploc.png"] forState:UIControlStateNormal];
    [self.view addSubview:mapBtn];
    
    UIButton *stateBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    stateBtn.frame=CGRectMake(60, 150, 35, 25);
    [stateBtn setImage:[UIImage imageNamed:@"state.png"] forState:UIControlStateNormal];
    [self.view addSubview:stateBtn];
    
    
    
    UIButton *contactBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    contactBtn.frame=CGRectMake(10, 180, 300, 45);
    [contactBtn setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
    [self.view addSubview:contactBtn];
    
    
    


    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    
    UIButton *favButton = [UIButton buttonWithType:UIButtonTypeCustom];
    favButton.frame = CGRectMake(270, 5, 50, 30);
    [favButton setImage:[UIImage imageNamed:@"star.png"] forState:UIControlStateNormal];
    [favButton setShowsTouchWhenHighlighted:TRUE];
//    [favButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:favButton];
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
