#include "date.h"
#include <stdlib.h>
#import "myMath.h"


Date::~Date()
{
    
    [cal release];
    [date release];
    [components release];
    
    
    
}
Date::Date()
{
    
    date = [[NSDate date] retain];
    initializeDate();
}

Date::Date(const Date& date1)
{
    this->date = [date1.date retain];
    initializeDate();
}

void Date::initializeDate()
{
    cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    components = [[cal components:NSEraCalendarUnit |
                   NSYearCalendarUnit |
                   NSMonthCalendarUnit |
                   NSDayCalendarUnit    |
                   NSHourCalendarUnit |
                   NSMinuteCalendarUnit|
                   NSSecondCalendarUnit |
                   NSWeekCalendarUnit |
                   NSWeekdayCalendarUnit |
                   NSWeekdayOrdinalCalendarUnit |
                   NSQuarterCalendarUnit  fromDate:date] retain];
}
Date::Date(int d, int m, int y)
{
    NSDateComponents* tempComp = [[NSDateComponents alloc] init];
    [tempComp setDay:d]; [tempComp setMonth:m]; [tempComp setYear:y];
    [tempComp setHour:0];[tempComp setMinute:0];[tempComp setTimeZone:[NSTimeZone systemTimeZone]];
    NSCalendar* tempcal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    this->date = [[ tempcal dateFromComponents:tempComp] retain];
    [tempcal release];    [tempComp release];
    
    initializeDate();
}

Date::Date(NSDate* date)
{
    this->date = [date retain];
    initializeDate();
    
}


Date& Date::operator=(const Date& d)
{
    [date release];
    [cal release];
    [components release];
    
    date = [d.date retain];
    components = [d.components retain];
    cal = [d.cal retain];
    
    return *this;
    
}

bool Date::operator==(const Date& d1 )
{
    NSDate* inputDate = d1.date;
    if([inputDate isEqual:this->date])
        return true;
    return false;
}

bool Date::isEqualDay(Date d)
{
    if(d.day() == this->day() && d.month() == this->month() && d.year() == this->year())
        return true;
    return false;
}


int Date::timeZone()
{
    NSTimeZone* zone = [NSTimeZone systemTimeZone];
    int hours =(int) [zone secondsFromGMT] / 3600;
    return hours;
    
}

int Date::dayOfWeek()
{
    
    int i=0;
    switch([components weekday])
    {
        case 7:
            i= SATURDAY;
            break;
        case 1:
            i= SUNDAY;
            break;
        case 2:
            i= MONDAY;
            break;
        case 3:
            i= TUESDAY;
            break;
        case 4:
            i= WEDNESDAY;
            break;
        case 5:
            i= THURSDAY;
            break;
        case 6:
            i= FRIDAY;
            break;
    }
    return i;
    
}

NSString* Date::dayOfWeekString()
{
    int dayOfWeek = this->dayOfWeek();
    
    NSString* dayString = nil;
    switch (dayOfWeek) {
        case SATURDAY:
            dayString = NSLocalizedString(@"Saturday", nil);
            break;
        case SUNDAY:
            dayString = NSLocalizedString(@"Sunday", nil);
            break;
        case MONDAY:
            dayString = NSLocalizedString(@"Monday", nil);
            break;
        case TUESDAY:
            dayString = NSLocalizedString(@"Tuesday", nil);
            break;
        case WEDNESDAY:
            dayString = NSLocalizedString(@"Wednesday", nil);
            break;
        case THURSDAY:
            dayString = NSLocalizedString(@"Thursday", nil);
            break;
        case FRIDAY:
            dayString = NSLocalizedString(@"Friday", nil);
            break;
            
        default:
            break;
    }
    
    return dayString;
    
}

// simple getters
int Date::day() { return (int)[components day];}
int Date::month() { return (int)[components month];}
int Date::year() { return (int)[components year];}
int Date::hour()  { return (int)[components hour];}
int Date::minute()  { return (int)[components minute];}
int Date::second() {return (int)[components second];}

//setters
void Date::setDay(int day)
{
    [components setDay:day];
    [date release];
    date = [[cal dateFromComponents:components] retain];
}
void Date::setMonth(int month)
{
    [components setMonth:month];
    [date release];
    date = [[cal dateFromComponents:components] retain];
}

void Date::setYear(int year)
{
    [components setYear:year];
    [date release];
    date = [[cal dateFromComponents:components] retain];
}

void Date::setHour(int hour)
{
    [components setHour:hour];
    [date release];
    date = [[cal dateFromComponents:components] retain];
}

void Date::setMinute(int minute)
{
    [components setMinute:minute];
    [date release];
    date = [[cal dateFromComponents:components] retain];
}

void Date::setSecond(int second)
{
    [components setSecond:second];
    [date release];
    date = [[cal dateFromComponents:components] retain];
}

void Date::setClock(double clock)
{
    int hour = (int) clock;
    int min = MyMath::round( (clock - hour) * 60 );
    int sec = 0;
    
    setHour(hour);
    setMinute(min);
    setSecond(sec);
}

void Date::setWeekDay(int weekDay)
{
    int old = dayOfWeek();
    int diff = weekDay - old ;
    if(diff < 0 ) diff += 7;
    
    [components release];
    components =  [[cal components:NSEraCalendarUnit |
                    NSYearCalendarUnit |
                    NSMonthCalendarUnit |
                    NSDayCalendarUnit    |
                    NSHourCalendarUnit |
                    NSMinuteCalendarUnit|
                    NSSecondCalendarUnit |
                    NSWeekCalendarUnit |
                    NSWeekdayCalendarUnit |
                    NSWeekdayOrdinalCalendarUnit |
                    NSQuarterCalendarUnit  fromDate:[date dateByAddingTimeInterval:diff * 24 * 60 * 60]] retain];
    [date release];
    date = [[cal dateFromComponents:components] retain];
    
}


Date Date::addDays(int days)
{
    
    
    NSDate* newDate = [date dateByAddingTimeInterval:days*24*60*60];
    return Date(newDate);
    
}


// get now clock with double format
double Date::getClock()
{
    Date now;
    double time = now.hour() + now.minute() / 60.0 + now .second() / 3600.0;
    
    return time;
}

double Date::getClockDouble()
{
    
    double time = hour() + minute() / 60.0 + second() / 3600.0;
    return time;
}

// convert an integer interval by seconds to a string representation of hh:mm
NSString* Date::getClockString(int interval)
{
    NSString* hours = @"";
    NSString* minutes = @"";
    interval = abs(interval);
    
    
    if((interval/60/60)<10)
        hours = @"0";
    hours = [hours stringByAppendingFormat:@"%d",interval/60/60];   
    
    
    if( (interval/60%60) <10 )
        minutes = @"0";
    
    minutes = [minutes stringByAppendingFormat:@"%d",interval/60%60 ];
    
    NSString* result = [hours stringByAppendingFormat:@":%@",minutes];
    
    return result;
}
