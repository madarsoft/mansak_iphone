//
//  imageviewcontroller.h
//  mutnav2
//
//  Created by amr on 1/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <BugSense-iOS/BugSenseController.h>

@interface imageviewcontroller : UIViewController<UIScrollViewDelegate> {
	UIImageView *myimg;
	NSMutableDictionary *sdic;
	NSNumber *myid;
	NSString *plist;
	

}
@property (retain, nonatomic) IBOutlet UIView *header_view;
@property (retain, nonatomic) IBOutlet UIView *footer_view;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property(retain,nonatomic)IBOutlet UIImageView *myimg;
@property(nonatomic,retain)NSMutableDictionary *sdic;
@property (nonatomic, retain)NSNumber *myid;
@property (nonatomic, retain)NSString *plist;

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer;

@end
