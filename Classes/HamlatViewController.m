//
//  HamlatViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 5/31/15.
//

#import "GetHijriDate.h"
#import "HamlatViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
@interface HamlatViewController ()

@end

@implementation HamlatViewController
@synthesize type,scr;


- (void)viewDidLoad {
    [super viewDidLoad];
     CGRect screensize=[[UIScreen mainScreen] bounds];
    
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    scr = 0;
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =@"حملات الحج";
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text = @"";
 
     //   [_header_view addSubview:second_image];
     ///   [_header_view addSubview:first_title];
    
    [_header_view addSubview:header_image];
    [ _header_view  addSubview:titleLabel];
  //  [self.view addSubview:_header_view];
    
  UIWebView *myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height)];
    myWeb.delegate=self;
   [self.view addSubview:myWeb];
  /* NSURL *url;
  
    if (type==1) {
        url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"start" ofType:@"html" inDirectory:@"Motawaf"]];
         NSURLRequest *request1 = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:(NSTimeInterval)60.0 ];
    [myWeb loadRequest:request1];
    }
    else if (type==2){
         url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"check_user" ofType:@"html" inDirectory:@"Motawaf"]];
       NSString *deviceUDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        NSString *body = [NSString stringWithFormat: @"device_id=%@&device_type=%d&current_day=%d",deviceUDID,1,[GetHijriDate GetDayIndex]];
        NSLog(@"%@",body);
        
        NSMutableURLRequest *request2 = [[NSMutableURLRequest alloc]initWithURL: url];
        [request2 setHTTPMethod: @"POST"];
        [request2 setHTTPBody: [body dataUsingEncoding: NSUTF8StringEncoding]];
        NSLog(@"%@",request2);
      -//[myWeb loadRequest:request2];
        
        NSURL *contentURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"check_user" ofType:@"html" inDirectory:@"Motawaf"]];
        NSURLComponents *components = [[NSURLComponents alloc] initWithURL:contentURL resolvingAgainstBaseURL:NO];
        NSMutableArray *queryItems = [components.queryItems mutableCopy];
        if (!queryItems) queryItems = [[NSMutableArray alloc] init];
        [queryItems addObject:[NSURLQueryItem queryItemWithName:@"device_id" value:deviceUDID]];
        [queryItems addObject:[NSURLQueryItem queryItemWithName:@"device_type" value:@"1"]];
 
        [queryItems addObject:[NSURLQueryItem queryItemWithName:@"current_day" value:[NSString stringWithFormat:@"%d",[GetHijriDate GetDayIndex]]]];
        
        components.queryItems = queryItems;
        NSURL *newURL = components.URL;
        NSLog(@"%@",newURL);
        
        NSURLRequest *request0 = [NSURLRequest requestWithURL:newURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:(NSTimeInterval)60.0 ];
        [myWeb loadRequest:request0];
   
    }*/
   

    
    
    UILabel *soon_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20,self.view.frame.size.height/2 , second_image.frame.size.width-40, 25)];
    soon_title.backgroundColor=[UIColor clearColor];
   soon_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:24];
    
    soon_title.textColor=[UIColor blackColor];
    soon_title.textAlignment=NSTextAlignmentCenter;
    soon_title.text = @"انتظرونا قريبًا";
    
    // [self.view addSubview:soon_title];
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
  //  [self.view addSubview:_footer_view];

    
    
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    if (type==1) {
        [tracker set:kGAIScreenName value:@"Hamlat Screen/Iphone"];
    }else{
         [tracker set:kGAIScreenName value:@"Hamlty Screen/Iphone"];
    }
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    NSLog(@"%d",scr);
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        if (type==1) {
            UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
            backButton.frame =CGRectMake(10, 20, 30, 30);
            [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
            [backButton setShowsTouchWhenHighlighted:TRUE];
            [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
            [self.view addSubview:backButton];
       }
        
        
        // [backButton release];
        
    }
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
      NSString *tmpStr = [[webView.request mainDocumentURL ]absoluteString];
  
    NSLog(@"Requested url: %@",tmpStr);
    
    }

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSString *tmpStr = [[webView.request mainDocumentURL ]absoluteString];
      NSLog(@"Loaded url: %@", tmpStr);
    
    
    if ([tmpStr containsString:@"Motawaf/password.html"] ) {
        NSLog(@"OKKKKKKKK");
        _returnBtn= [UIButton buttonWithType:UIButtonTypeCustom];
        _returnBtn.frame =CGRectMake(10, 20, 30, 30);
        [_returnBtn setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [_returnBtn setShowsTouchWhenHighlighted:TRUE];
        [_returnBtn addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
       
        [self.view addSubview:_returnBtn];
        
        
    }
    else{
        NSLog(@"NOOOOOOO");
        [_returnBtn removeFromSuperview];
    }
  
    
    //psudocode
   /* if(myRequestedUrl is not the same as myLoadedUrl){
        doSomething
    }*/
}

/// pop the view from navigation to show the previous one.
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
