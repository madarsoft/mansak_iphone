//
//  PrayTimeViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 2/11/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//

#import "PrayTimeViewController.h"
#import "prayertimecalculator.h"
#import "Global_style.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

@implementation PrayTimeViewController

@synthesize prayerTimes , prayerTimesMa,prayerTimesIcons,prayerTimesIcons_ON,prayTimes;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Release any cached data, images, etc that aren't in use.
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"PrayTimes Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    prayerTimesIcons_ON = [NSArray arrayWithObjects:@"fajr-on.png",@"shrouq-on.png",@"zohr-on.png",@"zohr-on.png",@"maghreb-on.png",@"fajr-on.png", nil];
    prayerTimesIcons = [NSArray arrayWithObjects:@"fajr.png",@"shrouq.png",@"zohr.png",@"zohr.png",@"maghreb.png",@"fajr.png", nil];
    
    
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =NSLocalizedString(@"mawqeet",@"test");
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    
    Date melady_today;
    NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hijridate objectAtIndex:i] intValue];
    int	month_number = hegryToday[1];
    int	year_number = hegryToday[2];
    NSString* month_name = NSLocalizedString([HijriTime getArabicMonth:month_number], @"");
    NSString* year=[NSString stringWithFormat:@"%ld",(long)year_number];
    
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"ar"];
    [formatter setLocale:locale];
    [formatter setDateFormat:@"EEEE:"];
    
    NSString* str = [formatter stringFromDate:date];
    first_title.text=[NSString stringWithFormat:@"%@ %d %@ %@",str, hegryToday[0],month_name,year];
    
    [_header_view addSubview:second_image];
    [_header_view addSubview:header_image];
    [_header_view addSubview:first_title];
    
    [ _header_view  addSubview:titleLabel];
    [self.view addSubview:_header_view];
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
    
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    
    NSDate* curDate = [NSDate date];
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* dateOnlyToday = [cal components:( NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit ) fromDate:curDate];
    int d = (int)[dateOnlyToday day];
    int m = (int)[dateOnlyToday month];
    int y = (int)[dateOnlyToday year];
    
    
    PrayersCaculator *calM = [[PrayersCaculator alloc]initWithDay:d Month:m Year:y Lat:21.4266667 Long:39.8261111 Zone:3 Mazhab:1 Way:3 DLS:0];
    
    self.prayerTimes = [calM getPrayersStrings];
    self.prayTimes = [calM getPrayersDoubles];
    [calM release];
    
    PrayersCaculator *calMa = [[PrayersCaculator alloc]initWithDay:d Month:m Year:y Lat:24.460899 Long:39.62019 Zone:3 Mazhab:1 Way:3 DLS:0];
    
    self.prayerTimesMa = [calMa getPrayersStrings];
    [calMa release];
    [self drawPrayTimes];
    
}
-(void)drawPrayTimes{
    int idd = [self getPrayerID];
    UIImageView *label_image=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0,280, 38)];
    [label_image setImage:[UIImage imageNamed:@"praylable.png"]];
    UILabel *first_lab=[[UILabel alloc] initWithFrame:CGRectMake(40, 160, 50, 30)];
    first_lab.backgroundColor=[UIColor clearColor];
    first_lab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_lab.textColor=[UIColor colorWithRed:0.129 green:0.71 blue:0.71 alpha:1] /*#21b5b5*/;
    first_lab.textAlignment=NSTextAlignmentCenter;
    first_lab.text=NSLocalizedString(@"madina", @"Madina");
    [self.view addSubview:first_lab];
    
    UILabel *secnd_lab=[[UILabel alloc] initWithFrame:CGRectMake(110, 160, 50, 30)];
    secnd_lab.backgroundColor=[UIColor clearColor];
    secnd_lab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    secnd_lab.textColor=[UIColor colorWithRed:0.129 green:0.71 blue:0.71 alpha:1] /*#21b5b5*/;
    secnd_lab.textAlignment=NSTextAlignmentCenter;
    secnd_lab.text=NSLocalizedString(@"maka", @"Maka");
    [self.view addSubview:secnd_lab];
    
    UILabel *thrid_lab=[[UILabel alloc] initWithFrame:CGRectMake(180, 160, 70, 30)];
    thrid_lab.backgroundColor=[UIColor clearColor];
    thrid_lab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    thrid_lab.textColor=[UIColor colorWithRed:0.129 green:0.71 blue:0.71 alpha:1] /*#21b5b5*/;
    thrid_lab.textAlignment=NSTextAlignmentCenter;
    thrid_lab.text=NSLocalizedString(@"pray", @"Pray");
    [self.view addSubview:thrid_lab];
    NSArray *prayers = [[NSArray alloc]initWithObjects:@"fajr",@"shrooq",@"dohr",@"3asr",@"mghreb",@"3esha2",nil];
    
    
    for (int i=0; i<6;i++) {
        UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(20, 190+(i*40), 280, 38)];
        tmpView.backgroundColor = [UIColor clearColor];
        UIImageView *prayImg = [[UIImageView alloc]initWithFrame:CGRectMake(240, 4, 35, 30)];
        [prayImg setImage:[UIImage imageNamed:[prayerTimesIcons objectAtIndex:i]]];
        
        if (i==idd) {
            [tmpView addSubview:label_image];
            [prayImg setImage:[UIImage imageNamed:[prayerTimesIcons_ON objectAtIndex:i]]];
            
            
        }
        [tmpView addSubview:prayImg];
        int pmFlag = [self getPrayerPM];
        NSString *pm;
        if (i>=pmFlag) {
            pm = @"PM";
        }
        else{
            pm = @"AM";
        }
        
        
        UILabel *firstlab=[[UILabel alloc] initWithFrame:CGRectMake(30, 4, 60, 30)];
        firstlab.backgroundColor=[UIColor clearColor];
        firstlab.font = [UIFont fontWithName:@"Arial-BoldMT" size:13];
        firstlab.textColor=[UIColor colorWithRed:0.369 green:0.337 blue:0.247 alpha:1] /*#5e563f*/;
        firstlab.textAlignment=NSTextAlignmentCenter;
        firstlab.text=[NSString stringWithFormat:@"%@ %@",[prayerTimesMa objectAtIndex:i],pm];
        [tmpView addSubview:firstlab];
        
        UILabel *secndlab=[[UILabel alloc] initWithFrame:CGRectMake(100, 4, 60, 30)];
        secndlab.backgroundColor=[UIColor clearColor];
        secndlab.font = [UIFont fontWithName:@"Arial-BoldMT" size:13];
        secndlab.textColor=[UIColor colorWithRed:0.369 green:0.337 blue:0.247 alpha:1] /*#5e563f*/;
        secndlab.textAlignment=NSTextAlignmentCenter;
        secndlab.text=[NSString stringWithFormat:@"%@ %@",[prayerTimes objectAtIndex:i],pm];
        
        [tmpView addSubview:secndlab];
        
        UILabel *thridlab=[[UILabel alloc] initWithFrame:CGRectMake(170, 4, 70, 30)];
        thridlab.backgroundColor=[UIColor clearColor];
        thridlab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
        thridlab.textColor=[UIColor colorWithRed:0.369 green:0.337 blue:0.247 alpha:1] /*#5e563f*/;
        thridlab.textAlignment=NSTextAlignmentCenter;
        thridlab.text= NSLocalizedString([prayers objectAtIndex:i],@"test");
        [tmpView addSubview:thridlab];
        
        if (i==idd) {
            
            firstlab.textColor=[UIColor whiteColor];
            secndlab.textColor=[UIColor whiteColor];
            thridlab.textColor=[UIColor whiteColor];
            
        }
        [self.view addSubview:tmpView];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(int)  getPrayerPM{
    int idd=5;
    for (int i=0; i<6; i++) {
        double time = [[prayTimes objectAtIndex:i]doubleValue];
        if ( time > 12.0) {
            return idd=i;
        }
    }
    return idd;
}

-(int)  getPrayerID
{
    Date dt;
    double curTimeDouble = dt.getClockDouble();
    int idd=5;
    for (int i=0; i<6; i++) {
        double time = [[self.prayTimes objectAtIndex:i]doubleValue];
        if ( time > curTimeDouble) {
            return idd=i;
        }
    }
    return idd;
}
- (void)dealloc {
    
    [super dealloc];
}
@end
