//
//  HamltyViewController.h
//  mutnav2
//
//  Created by waleed on 6/9/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "HamltViewController.h"
@interface HamltyViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate>
@property (retain, nonatomic) IBOutlet UIScrollView *scrlView;

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITextField *codeTxt;
@property (retain, nonatomic) IBOutlet UIButton *codeBtn;

@property BOOL _keyboardIsShowing;
@property BOOL keyboardVisible;


- (void)keyboardDidShow: (NSNotification *) notif;
- (void)keyboardDidHide: (NSNotification *) notif;

-(IBAction)btnClicked:(id)sender;

@end
