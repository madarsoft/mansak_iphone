//
//  NMViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 7/7/14.
//
//

#import "NMViewController.h"
#import "RXMLElement.h"
@interface NMViewController ()

@end

@implementation NMViewController

@synthesize prayerTimes;
@synthesize backImage;
@synthesize tawef,saai;
@synthesize prayTimes;
@synthesize closeView,openView,viewId;
@synthesize floorView,floorId,crowdView,crowd,crowdId;
@synthesize leftLab,centLab,rightLab;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.frame=CGRectMake(0, 0, 320, 440*highf);
    self.navigationController.navigationBar.hidden = YES;
    
    
    
    backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NewBG.jpg"]];
    backImage.frame = CGRectMake(0, 0, 320,440*highf);
    [self.view addSubview:backImage];
    tawef = [[NSMutableArray alloc] init];
    saai = [[NSMutableArray alloc] init];
    crowd = [[NSMutableArray alloc] init];
    [self drawHeader];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated{
    [self drawTimePlayers];
    
    
    self.navigationController.navigationBar.hidden = YES;
      [super viewWillAppear:animated];
}




-(NSString *) getMacTemp{
    
    NSString *Mtemps =[NSString stringWithFormat:@""] ;
    NSMutableArray * temps = [[NSMutableArray alloc] init ];
    
    
   
        NSString *tmpUrl = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=1939897&u=c"];
        
        
        NSURL *url = [NSURL URLWithString:tmpUrl];
        NSURLRequest* chRequest = [NSURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval:5];
        NSError* theError;
        NSData* response = [NSURLConnection sendSynchronousRequest:chRequest returningResponse:nil error:&theError];
        RXMLElement *parser = [RXMLElement elementFromXMLData:response];
        
        //     RXMLElement *parser = [RXMLElement elementFromURL:[NSURL URLWithString:tmpUrl]];
        [parser iterateWithRootXPath:@"//item" usingBlock: ^(RXMLElement *item) {
            NSLog(@"Player #5: %@", [item child:@"description"]);
            NSString *txt  = [NSString stringWithFormat:@"%@",[item child:@"description"]];
            NSArray *tmp = [txt componentsSeparatedByString:@" "];
            NSString *currentTemp;
            if ([[tmp objectAtIndex:5]length ]>2) {
                currentTemp = [tmp objectAtIndex:6];
            }
            else {
                currentTemp = [tmp objectAtIndex:5];
            }
            // NSLog(@"====%@",currentTemp);
            if (currentTemp==NULL) {
                currentTemp = @"";
            }
            NSLog(@"%@",currentTemp);
            [temps addObject:currentTemp];
        }];
   
   
    
    
    if ([temps count]==0) {
        for (int i=0; i<2; i++) {
            [temps addObject:@"0"];
        }
    }
    Mtemps = [temps objectAtIndex:0];
    return Mtemps;
    
}

-(void) drawHeader{
    
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NHead1.png"]];
    img.frame = CGRectMake(5, 0*highf, 310, 80*highf);
    [self.view addSubview:img];
    
    [img release];
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"EEEE:MM-dd"];
    //Get the string date
    NSString *str = [NSString stringWithFormat:@"مكة /%@ /%@C",[formatter stringFromDate:date],[self getMacTemp]];
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 0*highf, 160, 16*highf)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.alpha = 1;
    timeLabel.text = str;
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.textAlignment = UITextAlignmentRight;
    timeLabel.font = [UIFont boldSystemFontOfSize:10];
    [self.view addSubview:timeLabel];
    [timeLabel release];
    
}

-(void) drawTimePlayers{
    
    NSDate* curDate = [NSDate date];
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* dateOnlyToday = [cal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)fromDate:curDate];
    int d = [dateOnlyToday day];
    int m = [dateOnlyToday month];
    int y = [dateOnlyToday year];
    PrayersCaculator *calM = [[PrayersCaculator alloc]initWithDay:d Month:m Year:y Lat:21.4266667 Long:39.8261111 Zone:3 Mazhab:1 Way:3 DLS:0];
    self.prayerTimes = [calM getPrayersStrings];
    self.prayTimes = [calM getPrayersDoubles];
    
    // [calM release];
    
    NSMutableArray *praysLab = [[NSMutableArray alloc] initWithObjects:@"sala6.png",@"sala5.png",@"sala4.png",@"sala3.png",@"sala2.png",@"sala1.png", nil];
    
    int idd = [self getPrayerID];
    
    int j=0;
    for (int i=0; i<6; i++) {
        if (i == idd) {
            UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[praysLab objectAtIndex:i]]];
            img.frame = CGRectMake(10+(i*50), 18*highf, 50, 53*highf);
            [self.view addSubview:img];
            
            [img release];
        }
        
        /*  UILabel *prayLabel = [[UILabel alloc] initWithFrame:CGRectMake(((i*51)+12), 22*highf, 50, 20*highf)];
         prayLabel.backgroundColor = [UIColor clearColor];
         prayLabel.alpha = 1;
         prayLabel.text = [praysLab objectAtIndex:i];
         if (i==idd) {
         prayLabel.textColor = [UIColor yellowColor];
         }
         else{
         prayLabel.textColor = [UIColor whiteColor];
         }
         
         prayLabel.font = [UIFont boldSystemFontOfSize:13];
         // [self.view addSubview:prayLabel];
         [prayLabel release];*/
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(((i*50)+10), 48*highf, 50, 20*highf)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.alpha = 1;
        timeLabel.text = [prayerTimes objectAtIndex:5-i];
        timeLabel.textColor = [UIColor whiteColor];
        timeLabel.textAlignment=UITextAlignmentCenter;
        timeLabel.font = [UIFont boldSystemFontOfSize:13];
        [self.view addSubview:timeLabel];
        
        [timeLabel release];
        j++;
    }
    CGSize screensize =[UIScreen mainScreen].bounds.size;
    int g=screensize.height;
    if (g==480) {
        g=60;
    }else{
        g=75;
    }
    UILabel *home=[[UILabel alloc] initWithFrame:CGRectMake(25, g , 50, 20)];
    home.textColor=[UIColor whiteColor];
    home.backgroundColor=[UIColor clearColor];
    [home setFont:[UIFont fontWithName:@"Arial-BoldMT" size:15]];
    home.text=@"الرئاسة العامة لشؤون المسجد الحرام والمسجد النبوي";
    [home sizeToFit];
    // [self.view addSubview:home];
    
    UIImageView *part = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"partLogo.png"]];
    part.backgroundColor = [UIColor colorWithRed:(229/255.0) green:(229/255.0) blue:(229/255.0) alpha:1.0];
    if (highf>1) {
        part.frame = CGRectMake(0, 389*highf, 320, 57);
    }
    else{
        part.frame = CGRectMake(0, 370*highf, 320, 57);
    }
    
    [self.view addSubview:part];
    

    
}

-(void) drawFooter{
    
}


-(void) drawTemp{
    
}

-(void) getCrowd{
    
}

-(void) loadFoot{
    
}

-(int)  getPrayerID{
    Date dt;
    double curTimeDouble = dt.getClockDouble();
    int idd=5;
    for (int i=0; i<6; i++) {
        double time = [[prayTimes objectAtIndex:5-i]doubleValue];
        if (curTimeDouble > time) {
            return idd=i;
        }
    }
    return idd;
}

-(IBAction)moveView:(id)sender{
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
