//
//  HajViewController.h
//  mutnav2
//
//  Created by waleed on 1/13/13.
//
//

#import "oldImg.h"
#import <UIKit/UIKit.h>
//#import <BugSense-iOS/BugSenseController.h>
@interface HajViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;

@property (nonatomic, retain) NSNumber *myid;
@property (nonatomic, retain) NSString *plist;
@property (nonatomic, retain) NSString *titleName;
@property (nonatomic, retain) NSMutableArray *menuearr;
@property (nonatomic, retain) NSMutableDictionary *sdic;
@property (nonatomic, retain) NSMutableArray *mainarr;
@property (nonatomic, retain) NSMutableArray *dataarr;
@property (nonatomic, retain) NSMutableArray *mansekarr;

//@property (nonatomic, retain) IBOutlet AdsViewController *adsView;

//-(void)stopAds;
-(void)loadData;
-(void)fillData;
@property int type;
@property int hajjType;
@property int dayIndex;

@end
