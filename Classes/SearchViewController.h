//
//  SearchViewController.h
//  itour
//
//  Created by walid nour on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.

#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "SearchHomeController.h"

@interface SearchViewController : UIViewController

<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (retain, nonatomic) IBOutlet UIButton *searchBtn;
@property (retain, nonatomic) IBOutlet UITextField *searchTxtField;


@property (nonatomic ,retain) NSMutableArray *mainarr;
@property (nonatomic ,retain) NSMutableArray *filearr;
@property (nonatomic ,retain) NSMutableArray *searcharr;
@property (nonatomic ,retain) NSMutableArray *buildarr;
@property (nonatomic ,retain) NSMutableArray *iddarr;
@property (nonatomic ,retain) NSMutableArray *gradearr;

@property (nonatomic ,retain) NSString *titleName;
@property (nonatomic ,retain) NSString *fileName;

@property (nonatomic ,retain) NSString *searchTxt;

@property NSInteger listType;
@property NSInteger townID;
@property NSInteger buildID;
@property NSInteger searchID;
@property (nonatomic, retain) NSMutableArray *radioButtons;

-(void) fillArr;
-(IBAction)searchPressed:(id)sender;
-(void)stopAds;

-(IBAction) ButtonClicked:(id) sender;
-(IBAction)textFieldDidChange:(id)sender;

@end
