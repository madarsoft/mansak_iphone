//
//  NewEMViewController.m
//  mutnav2
//
//  Created by waleed on 6/17/13.
//
//

#import "NewEMViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface NewEMViewController ()

@end

@implementation NewEMViewController

@synthesize bgImage,timelineView,bgupdate;
@synthesize elecmutawef,imgindex,mytext,arrdoaa,arrmanasek,from;
@synthesize sdic,douBtn,manBtn,pushBtn,mainView,closeBtn,btnindex,type,hajindex,decBtn,tt;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    
 
	
   
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,430*highf);
    [self.view addSubview:bgImage];
    
    mainView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 90*highf, 320, 260*highf)];
    mainView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:mainView];
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 30*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"تطويف";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLabel];
    
    if (tt==1) {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 5, 50, 30*highf);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        /* UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
         self.navigationItem.hidesBackButton = TRUE;
         self.navigationItem.leftBarButtonItem = barBackItem;
         [barBackItem release];*/
        [self.view addSubview:backButton];
        
    }

    
    douBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    douBtn.frame = CGRectMake(25, 40*highf, 125, 40*highf);
    [douBtn setImage:[UIImage imageNamed:@"doua.png"] forState:UIControlStateNormal];
    douBtn.backgroundColor = [UIColor clearColor];
    douBtn.tag=0;
    [douBtn addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    
    manBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    manBtn.frame = CGRectMake(160, 40*highf, 125, 40*highf);
    [manBtn setImage:[UIImage imageNamed:@"mansk.png"] forState:UIControlStateNormal];
    manBtn.backgroundColor = [UIColor clearColor];
    manBtn.tag=1;
    [manBtn addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:douBtn];
    [self.view addSubview:manBtn];
    
    imgindex=0;
    btnindex=1;
    hajindex=4;
    elecmutawef = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"t0.png"]];
    elecmutawef.frame=CGRectMake(60, 5, 200, 200);
    [mainView addSubview:elecmutawef];
    
    pushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    pushBtn.frame = CGRectMake(80, 35, 160, 160);
    pushBtn.backgroundColor = [UIColor clearColor];
    [pushBtn addTarget:self action:@selector(next_tawaf:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:pushBtn];
    
    
    decBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    decBtn.frame = CGRectMake(50,5, 25, 25);
    decBtn.backgroundColor = [UIColor clearColor];
    
    [decBtn setBackgroundImage:[UIImage imageNamed:@"decBtn.png"]	forState:UIControlStateNormal];
    [decBtn addTarget:self action:@selector(previous_tawaf:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:decBtn];
    CGSize screensize =[UIScreen mainScreen].bounds.size;
    timelineView = [[UIView alloc]initWithFrame:CGRectMake(30, screensize.height-120, 260,50)];
    timelineView.backgroundColor = [UIColor clearColor];
    UIImageView *timeImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 260,35)];
    timeImg.backgroundColor = [UIColor clearColor];
    timeImg.image = [UIImage imageNamed:@"time_line.png"];
    // [timelineView addSubview:timeImg];
    NSMutableArray *timeTit = [[NSMutableArray alloc]initWithObjects:@"التحلل",@"السعى",@"ماء زمزم",@"مقام إبراهيم",@"الطواف", nil];
    for (int i=0; i<5; i++) {
        UIButton *timeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        timeBtn.frame = CGRectMake(0+(52*i), 0,55, 35);
        timeBtn.backgroundColor=[UIColor clearColor];
        timeBtn.userInteractionEnabled=YES;
        if (i==4) {
            
            [timeBtn setImage:[UIImage imageNamed:@"step_1.png"] forState:UIControlStateNormal];
        }
        else{
            [timeBtn setImage:[UIImage imageNamed:@"timeIn.png"] forState:UIControlStateNormal];
        }
        timeBtn.tag=i;
        [timeBtn addTarget:self action:@selector(changefile:) forControlEvents:UIControlEventTouchUpInside];
        // [timeBtn setTitle:@"1" forState:UIControlStateNormal];
        [timelineView addSubview:timeBtn];
        
        UILabel *timeLab = [[UILabel alloc]initWithFrame:CGRectMake(5+(52*i),35, 52, 15)];
        timeLab.textColor=[UIColor blackColor];
        timeLab.backgroundColor= [UIColor clearColor];
        timeLab.font = [UIFont boldSystemFontOfSize:10];
        timeLab.textAlignment=NSTextAlignmentCenter;
        timeLab.text=[timeTit objectAtIndex:i];
        [timelineView addSubview:timeLab];
        [timeLab release];
    }
    
    
    [self.view addSubview:timelineView];
    
    
    NSString *mypath=[[NSBundle mainBundle] pathForResource:@"elec" ofType:@"plist"];
	arrmanasek=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
	sdic=[arrmanasek objectAtIndex:0];
	
    mytext = [[UITextView alloc] initWithFrame:CGRectMake(16, elecmutawef.frame.size.height+elecmutawef.frame.origin.y, 288,250*highf)];
    mytext.delegate =self;
    mytext.autoresizesSubviews = YES;
    mytext.backgroundColor = [UIColor clearColor];
    mytext.alpha = 1;
    mytext.font = [UIFont boldSystemFontOfSize:12];
    mytext.editable = NO;
    mytext.textAlignment = NSTextAlignmentRight;
    [mytext.layer setBorderColor: [[UIColor clearColor] CGColor]];
    [mytext.layer setBorderWidth: 0.0];
    [mytext.layer setCornerRadius:0.0f];
    [mytext.layer setMasksToBounds:YES];
    [mainView addSubview:mytext];
    type=0;
   
}


-(IBAction)changeView:(id)sender{
    btnindex =[sender tag];
    if (btnindex!=bIndex) {
        if (type==1) {
            type=0;
        }
        else{
            type=0;
        }
        }
    
    if (type==1) {
        [self closeView:sender];
        type=0;
    }
    else{
       /* NSString *filename=[sdic objectForKey:@"manasek"];
        [self addfiletotextview:filename];*/
        if(btnindex==0){
            NSInteger randomint=arc4random()%14+1;
            NSString *randomfile=[NSString stringWithFormat:@"doaa%ld",(long)randomint];
            [self addfiletotextview:randomfile];
        }
        else {
            //[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
            NSString *filename=[sdic objectForKey:@"manasek"];
            [self addfiletotextview:filename];
        }
    mainView.contentSize=CGSizeMake(320, 500);
    
    closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(250,75, 25, 15);
    [closeBtn setImage:[UIImage imageNamed:@"closeTime.png"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    
        mytext.hidden=NO;
    type=1;
    
    }
    bIndex=btnindex;
    
    
}
-(IBAction)closeView:(id)sender{
  //  [mytext removeFromSuperview];
    mytext.hidden=YES;
      mainView.contentSize=CGSizeMake(320, 200);
    [closeBtn removeFromSuperview];
    type=1;
  /*  if (btnindex==1) {
        btnindex=0;
    }
    else{
        btnindex=0;
    }*/
}


-(IBAction) next_tawaf:(id)sender{
	if(imgindex<[arrmanasek count]-1)
        imgindex++;
	/*if ( imgindex>=7) {
		imgindex=7;
	}*/
    if (hajindex==4) {
        if ( imgindex>=7) {
            imgindex=7;
        }
    }
    else if (hajindex==1)
    {
        if ( imgindex>=17) {
            imgindex=17;
        }
        
    }
	if(( imgindex<=7 ||(imgindex>=11 && imgindex<=17)))
	{
          NSString *newimg=[NSString stringWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
		elecmutawef.image=[UIImage imageNamed:newimg];
		elecmutawef.hidden=NO;
		
	}
    NSLog(@"%d",imgindex);
    NSLog(@"----");
    
	[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
    	
}
-(IBAction) previous_tawaf:(id)sender{
    if ( imgindex>0) {
 imgindex--;
    }
     
	if(imgindex>0){
        if (hajindex==4) {
            if ( imgindex<=0) {
                imgindex=0;
            }
        }
        else if (hajindex==1)
        {
            if ( imgindex<=11) {
                imgindex=11;
            }
            
        }
      
		if(![from isEqualToString:@"0"] &&( imgindex<=7||(imgindex>=11 && imgindex<=17))){
              NSString *newimg=[NSString stringWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
            elecmutawef.image=[UIImage imageNamed:newimg];
			elecmutawef.hidden=NO;
		}
		else {
			elecmutawef.hidden=YES;
		}
        
	}
    [self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];

}

-(IBAction)changefile:(id) sender{
	int i = [sender tag];
    hajindex=i;
    NSLog(@"hajindex%i",hajindex);
    UIButton *tmpBtn = (UIButton *)sender;
    [tmpBtn setImage:[UIImage imageNamed:@"step_1.png"] forState:UIControlStateNormal];
    if (i==4) {
        elecmutawef.image=[UIImage imageNamed:@"t0.png"];
        imgindex =0;
        elecmutawef.hidden=NO;
        pushBtn.hidden=NO;
        mytext.text=@"";
        [mytext setFrame:CGRectMake(16, elecmutawef.frame.size.height+elecmutawef.frame.origin.y, 288, 244)];
      mainView.contentSize=CGSizeMake(320, 500);
          }
    else if (i==3) {
        imgindex =8;
       // [elecmutawef removeFromSuperview];
       // [pushBtn removeFromSuperview];
        elecmutawef.hidden=YES;
        pushBtn.hidden=YES;
        [mytext setFrame:CGRectMake(16, 20, 288, 244)];
          mainView.contentSize=CGSizeMake(320, 250);
    }
    else if (i==2){
        imgindex = 9;
        elecmutawef.hidden=YES;
        pushBtn.hidden=YES;
        [mytext setFrame:CGRectMake(16, 20, 288, 244)];
    }
    else if (i==1){
        imgindex = 10;
        elecmutawef.image=[UIImage imageNamed:@"t0.png"];
        elecmutawef.hidden=NO;
        pushBtn.hidden=NO;
        mytext.hidden=NO;
        [mytext setFrame:CGRectMake(16, 200*highf, 288, 244)];
        mainView.contentSize=CGSizeMake(320, 500);
    }
    else if (i==0){
        imgindex = 18;
        elecmutawef.hidden=YES;
        pushBtn.hidden=YES;
        [mytext setFrame:CGRectMake(16, 20, 288, 244)];
         mainView.contentSize=CGSizeMake(320, 250);
    }

	if(btnindex==0){
		NSInteger randomint=arc4random()%14+1;
       NSString *randomfile=[NSString stringWithFormat:@"doaa%d",randomint];
        [self addfiletotextview:randomfile];
	}
	else {
		[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
	}
}
-(void)addfiletotextview:(NSString*)filename{
	NSFileManager *filemgr;
	NSLog(@"%@",filename);
	filemgr = [NSFileManager defaultManager];
		NSString *path=[[NSBundle mainBundle] pathForResource:filename ofType:@"dat"];
	if ([filemgr fileExistsAtPath: path ] == YES)
	{
	      NSError *error;
	     mytext.text=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
		}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
