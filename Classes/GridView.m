//
//  GridView.m
//  Almosaly
//
//  Created by Sara Ali on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GridView.h"
#import "CalCellView.h"
#import "TextCellView.h"
@implementation GridView
@synthesize gridView,numRows,numcellsPerRow,scrollRows,visibleNumRows,backgroundImage,
viewsArray , leftToRight;




- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
        gridView = [[UIScrollView alloc] initWithFrame:self.bounds];
        gridView.alwaysBounceHorizontal=NO;
        self.leftToRight = YES;
        [self addSubview:gridView];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
    if(backgroundImage != nil)
        [backgroundImage drawInRect:rect];
}

-(void)drawGrid:(NSInteger)Rows col:(NSInteger)Column
{
    
    for(id subview1 in gridView.subviews)
        [subview1 removeFromSuperview];
    
    numRows=Rows;
    numcellsPerRow=Column;
    CGRect frame;
    
    //mode1
    if(self.scrollRows ==NO){
        
        int initialYPos = 0 ;
        int cellCounter = 0;
        
        for (int i=0; i<numRows   && cellCounter < [viewsArray count] ; i++) {
            int  initialXPos = 0;
            if(self.leftToRight == NO)
                initialXPos = gridView.frame.size.width/numcellsPerRow * (numcellsPerRow -1 ) ;
            
            
            for (int j=0; j<numcellsPerRow   && cellCounter < [viewsArray count] ; j++)
            {
                frame = CGRectMake(initialXPos, initialYPos, gridView.frame.size.width/numcellsPerRow, gridView.frame.size.height/numRows);
                // get the UIView* from viewArray that set by controller each iteration increases the cellCounter
                UIView* cell = [viewsArray objectAtIndex:cellCounter];
                cell.frame = frame;
                [gridView addSubview:cell];
                cellCounter++;
                if(self.leftToRight)
                    initialXPos = initialXPos +gridView.frame.size.width/numcellsPerRow;
                else
                    initialXPos = initialXPos  - gridView.frame.size.width/numcellsPerRow;
                
                
            }
            //Change the position ready for the next column
            initialYPos = initialYPos + gridView.frame.size.height/numRows;
        }
        
    }
    //mode2
    
    else {
        {
            
            int initialYPos = 0 ;
            int cellCounter = 0;
            for (int i=0; i<numRows && cellCounter < [viewsArray count] ; i++) {
                
                int initialXPos = 0;
                for (int j=0; j<numcellsPerRow   && cellCounter < [viewsArray count]  ; j++) {
                    frame = CGRectMake(initialXPos, initialYPos, gridView.frame.size.width/numcellsPerRow, gridView.frame.size.height/self.visibleNumRows);
                    // get the UIView* from viewArray that set by controller each iteration increases the cellCounter
                    UIView* cell = [viewsArray objectAtIndex:cellCounter];
                    cell.frame = frame;
                    [gridView addSubview:cell];
                    cellCounter++;
                    
                    //Change the position ready for the next cell
                    initialXPos = initialXPos +gridView.frame.size.width/numcellsPerRow;
                }
                //Change the position ready for the next column
                initialYPos = initialYPos + gridView.frame.size.height/visibleNumRows;
            }
            gridView.contentSize = CGSizeMake(gridView.frame.size.width/numcellsPerRow,(gridView.frame.size.height*numRows)/self.visibleNumRows);
        }
    }
}
@end
