 //
//  NewsListViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 5/12/15.
//
//

#import "NewsListViewController.h"
#import "Global_style.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"



@interface NewsListViewController ()

@end

@implementation NewsListViewController
@synthesize mainTable,menuearr,urlarr,myWeb;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"muatefNews Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =NSLocalizedString(@"muatefNews", @"News");
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    
  
        mainTable = [[UITableView alloc] initWithFrame:CGRectMake(15, 120, 290, 300*highf) style:UITableViewStylePlain];

    
    [_header_view addSubview:header_image];
    [ _header_view  addSubview:titleLabel];
    [self.view addSubview:_header_view];
    
    
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1.0;
    mainTable.rowHeight=50.0;
    mainTable.separatorColor = [UIColor clearColor];
    
    [self.view addSubview:mainTable];
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
        // [backButton release];
    }
    
    menuearr = [[NSMutableArray alloc] init];
    urlarr = [[NSMutableArray alloc] init];
    [menuearr addObject:@"انتظر حتى يتم تحميل الأخبار"];
    [urlarr addObject:@""];
    
    

    
    NSURL *url = [NSURL URLWithString:@"http://api.madarsoft.com/mut2/v1/new.aspx"];
    NSURLRequest* request = [NSURLRequest requestWithURL:url ];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ([data length] >0 && error == nil)
                               {
                                   // DO YOUR WORK HERE
                                   NSString* content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] ;
                                   
                                   NSArray *allLines = [content componentsSeparatedByString: @"::"];
                                   
                                   for (int x=0; x <[allLines count]-1; x++) {
                                       NSString *line = [NSString stringWithFormat:@"%@",[allLines objectAtIndex:x]];
                                       NSArray *Lines = [line componentsSeparatedByString: @"||"];
                                       [menuearr addObject:[Lines objectAtIndex:0]];
                                       [urlarr addObject:[Lines objectAtIndex:1]];
                                       
                                   }
                                   if ([allLines count]>0) {
                                       [menuearr removeObjectAtIndex:0];
                                       [urlarr removeObjectAtIndex:0];
                                       
                                       
                                       [mainTable reloadData];
                                   }
                                   
                               }
                               else if ([data length] == 0 && error == nil)
                               {
                                   NSLog(@"Nothing was downloaded.");
                               }
                               else if (error != nil){
                                   NSLog(@"Error = %@", error);
                               }
                               // ...
                           }];
    
}

-(void)close_view:(id)sender{
    [UIView animateWithDuration:0.4 delay:0  options:0 animations: ^{
        [_news_view setFrame:CGRectMake( -self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
    } completion: ^(BOOL completed) {
        [_news_view removeFromSuperview];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - news
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        // the cell is being recycled, remove old embedded controls
        UIView *viewToRemove = nil;
        viewToRemove = [cell.contentView viewWithTag:kMyTag];
        if (viewToRemove)
            [viewToRemove removeFromSuperview];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    cell.backgroundColor = [UIColor clearColor];
    UIView *cellView = [[UIView alloc]initWithFrame:cell.frame];
    UILabel *cellLab = [[UILabel alloc]initWithFrame:CGRectMake(11, 5, 245, 40)];
    UIView *labBgView = [[UIView alloc]initWithFrame:CGRectMake(11, 5, 255, 40)];

    UIImageView *rightDot = [[UIImageView alloc]initWithFrame:CGRectMake(257, 14, 21, 21)];
    rightDot.image = [UIImage imageNamed:@"dot.png"];
    
    UIImageView *leftDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 14, 21, 21)];
    leftDot.image = [UIImage imageNamed:@"dot.png"];
    
    if (indexPath.row % 2!=0) {
        labBgView.backgroundColor =[UIColor colorWithRed:0.929 green:0.914 blue:0.867 alpha:1] /*#ede9dd*/;
        
        cellLab.backgroundColor = [UIColor colorWithRed:0.929 green:0.914 blue:0.867 alpha:1] /*#ede9dd*/;
    }
    else{
        labBgView.backgroundColor =[UIColor colorWithRed:0.965 green:0.953 blue:0.925 alpha:1] /*#f6f3ec*/ ;
        
        cellLab.backgroundColor = [UIColor colorWithRed:0.965 green:0.953 blue:0.925 alpha:1] /*#f6f3ec*/ ;
    }
    cellLab.text=[menuearr objectAtIndex:indexPath.row];
    
    cellLab.textAlignment = NSTextAlignmentRight;
    cellLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    cellLab.numberOfLines = 0;
    cellLab.textColor = [UIColor colorWithRed:0.361 green:0.318 blue:0.208 alpha:1]; /*#5c5135*/
    cellLab.lineBreakMode = NSLineBreakByWordWrapping;
    
    [cellView addSubview:labBgView];
    [cellView addSubview:cellLab];
    [cellView addSubview:rightDot];
    [cellView addSubview:leftDot];
    [cell.contentView addSubview:cellView ];
 
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
     if ([urlarr count]>1) {
        
        _news_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        _news_view.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
        
          UILabel *header_lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width,30)];
        header_lbl.text=NSLocalizedString(@"muatefNews", @"News");
        [header_lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
        header_lbl.textAlignment=NSTextAlignmentCenter;
        header_lbl.textColor=[UIColor whiteColor];
        [_news_view addSubview:header_lbl];
        UIButton *back_btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
        back_btn.layer.cornerRadius=12.5;
        back_btn.backgroundColor=[UIColor clearColor];
        [back_btn setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
        [back_btn addTarget:self action:@selector(close_view:) forControlEvents:UIControlEventTouchUpInside];
        
        
        myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
        myWeb.delegate=self;
        [_news_view setFrame:CGRectMake( -self.view.frame.size.width,  0,self.view.frame.size.width, self.view.frame.size.height)]; //notice this is OFF screen!
        [UIView beginAnimations:@"animateTableView" context:nil];
        [UIView setAnimationDuration:0.4];
        [self.view addSubview:_news_view];
        [_news_view setFrame:CGRectMake( 0, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
        [UIView commitAnimations];
        [_news_view addSubview:myWeb];
        [_news_view addSubview:back_btn];
        NSString *urlAddress = [urlarr objectAtIndex:indexPath.row];
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"news" ofType:@"html" inDirectory:@"www"]];
        NSString *URLString = [url absoluteString];
        NSString *queryString =[NSString stringWithFormat:@"?id=%@",urlAddress];
        NSString *URLwithQueryString = [URLString stringByAppendingString: queryString];
        NSURL *finalURL = [NSURL URLWithString:URLwithQueryString];
        NSURLRequest *request = [NSURLRequest requestWithURL:finalURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:(NSTimeInterval)10.0 ];
        [myWeb loadRequest:request];
        [self.view bringSubviewToFront:_botimg];
    }
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
