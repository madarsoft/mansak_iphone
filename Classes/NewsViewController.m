//
//  NewsViewController.m
//  mutnav2
//
//  Created by walid nour on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewsViewController.h"
#import "Reachability.h"

@implementation NewsViewController

@synthesize bgImage, mainTable;
@synthesize menuearr,urlarr, myWeb;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
  //  self.title = @"أخبار الحج";
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,420);
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, 320, 390) style:UITableViewStyleGrouped];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 0.6;
    mainTable.rowHeight=60.0;
    mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    
    [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    
    menuearr = [[NSMutableArray alloc] init];
    urlarr = [[NSMutableArray alloc] init];
    
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor greenColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text =  @"أخبار الحج";
    [titleView sizeToFit];
    
    
    Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    //  [curReach set
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {
    
    NSURL *url = [NSURL URLWithString:@"http://38.121.76.242/mut2/new.aspx"];
    NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSArray *allLines = [content componentsSeparatedByString: @"::"];
    for (int x=0; x <[allLines count]-1; x++) {
        NSString *line = [NSString stringWithFormat:@"%@",[allLines objectAtIndex:x]];
         NSArray *Lines = [line componentsSeparatedByString: @"||"];
        [menuearr addObject:[Lines objectAtIndex:0]];
        [urlarr addObject:[Lines objectAtIndex:1]];
    }
    }
    else
    {
        [menuearr addObject:@"لا يوجد اتصال بالإنترنت"];
        
    }
    
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
      //  UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 50, 30); 
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [barBackItem release];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count]-1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
  	cell.textLabel.text=[menuearr objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = UITextAlignmentRight;
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.textLabel.numberOfLines = 0;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    if ([urlarr count]>1) {
      
    
    myWeb = [[UIWebView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:myWeb];
    
    NSString *urlAddress = [urlarr objectAtIndex:indexPath.row];
    NSLog(@"%@", urlAddress);
    //Create a URL object.
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    //URL Requst Object
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    //Load the request in the UIWebView.
    [myWeb loadRequest:requestObj];
    }

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    [self setBgImage:nil];
    [self setMainTable:nil];
    [self setMyWeb:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[menuearr release];
	[urlarr release];
	[super dealloc];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
