//
//  BuildingViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 2/15/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//

#import "BuildingViewController.h"

@implementation BuildingViewController

@synthesize bgImage, mainTable;
@synthesize myId, myName, grade;
@synthesize info, room,build, services, infolabels, buildLabels, gradeLabels;
@synthesize myMap;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
       
    info = [[NSMutableArray alloc] init];
    room = [[NSMutableArray alloc] init];
    build = [[NSMutableArray alloc] init];
    infolabels = [[NSMutableArray alloc] initWithObjects:@"الدرجة",@"الحي",@"الشارع",@"المجموعة",@"الهاتف",@"الطوابق",@"الوحدات",@"الأسرة", nil];
    services = [[NSMutableArray alloc] initWithObjects:@"توصيل الطعام",@"تكييف",@"ثلاجة",@"هاتف",@"تلفاز",@"غسيل ملابس",@"نظافة",@"مطبخ",@"خزانة خاصة",nil ];
        buildLabels = [[NSMutableArray alloc] initWithObjects:@"مطعم",@"مطبخ",@"كافيتريا",@"صناديق أمانات",@"مصعد",@"قاعة اجتماعات",@"إنترنت",@"سوق تجاري",@"مواقف",@"توصيل للمطار",@"توصيل للحرم", nil];
    
      gradeLabels = [[NSMutableArray alloc] initWithObjects:@"غير محددة",@"أولى/أ",@"أولى/ب",@"الأولى",@"الثالثة",@"الثالثة/أ",@"الثالثة/ب",@"الثانية",@"الثانية/أ",@"الثانية/ب",@"الممتاز",@"الرابعة", nil];
    _status=0;
    
    
    
   [self getbuildingDetails];
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,432*highf);
    
    
    mainTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 5.0*highf, 320, 370*highf) style:UITableViewStylePlain]autorelease];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=45.0*highf;
    mainTable.hidden = NO;
   // mainTable.sectionHeaderHeight = 20.0;
    mainTable.allowsSelection = NO;
     mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
  //  [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    
   // self.title= myName;    
    
    
    UIImageView *titleImg =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titleImg.png"]];
    titleImg.frame = CGRectMake(0, 0, 320,40);
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(80, 0, 240,40)];
    titleLab.backgroundColor = [UIColor clearColor];
    titleLab.font = [UIFont boldSystemFontOfSize:20.0];
   // titleLab.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    
    titleLab.textColor = [UIColor whiteColor]; // Change to desired color
    titleLab.textAlignment = NSTextAlignmentRight;
     titleLab.text =  myName;
    [titleImg addSubview:titleLab];
    
    [self.view addSubview:titleImg];
    
    UIButton *contactBtn=[[UIButton alloc] initWithFrame:CGRectMake(10,4, 30,32)];
    
    contactBtn.alpha = 1;
    [contactBtn setBackgroundImage:[UIImage imageNamed:@"contactBtn.png"]	forState:UIControlStateNormal];
   // [contactBtn setContentMode:UIViewContentModeCenter];
    
    [contactBtn addTarget:self action:@selector(contactPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:contactBtn];
    
    
    UIButton *mapBtn=[[UIButton alloc] initWithFrame:CGRectMake(60, 4, 30, 32)];
    mapBtn.alpha = 1;
    [mapBtn setBackgroundImage:[UIImage imageNamed:@"mapBtn.png"]	forState:UIControlStateNormal];
   // [mapBtn setContentMode:UIViewContentModeCenter];
    
    [mapBtn addTarget:self action:@selector(mapBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mapBtn];
    

    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
      //  titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text =  @"السكن";
    [titleView sizeToFit];
    

    // Do any additional setup after loading the view from its nib.
}

-(void)popViewControllerWithAnimation {
    if (_status==1) {
        [myMap removeFromSuperview];
        _status=0;
    }
    else{
    [self.navigationController popViewControllerAnimated:YES];
    }
}



- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [backButton release];
        [barBackItem release];
    }
    
}

- (void)viewDidUnload
{
    [self setBgImage:nil];
    [self setMainTable:nil];
    [self setMyMap:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count =0;
    if (section==0) {
        count =[info count];
    }
    else if(section==1)
    {
        count =[room count];
        
    }
    else if(section==2)
    {
    count =[build count];
    }
   /* else if(section==3)
    {
        count =1;
    }*/
    return count;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    if(indexPath.section==0)
    {
      // cell.textLabel.textAlignment   = NSTextAlignmentRight; 
        cell.textLabel.text = [info objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [infolabels objectAtIndex:indexPath.row];
        cell.imageView.image = nil;
    }
    else if (indexPath.section == 1)
    {
              // cell.textLabel.textAlignment = NSTextAlignmentLeft;
        
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = [services objectAtIndex:indexPath.row];
    
        int x = [[room objectAtIndex:indexPath.row] intValue];
        if (x==1) {
            cell.imageView.image = [UIImage imageNamed:@"check.png"];
        }
        else {
            cell.imageView.image = [UIImage imageNamed:@"unCheck.png"];
        }

        
    }
    else if (indexPath.section ==2)
    {
       
     //   cell.textLabel.textAlignment = NSTextAlignmentLeft;
        
        cell.textLabel.text = @"";
        cell.detailTextLabel.text = [buildLabels objectAtIndex:indexPath.row];
        int x = [[build objectAtIndex:indexPath.row] intValue];
        if (x==1) {
            cell.imageView.image = [UIImage imageNamed:@"check.png"];
        }
        else {
            cell.imageView.image = [UIImage imageNamed:@"unCheck.png"];
        }
        
    }
   /* else if (indexPath.section ==3)
    {
        
        static NSString *Buttondentifier = @"CellBUT";
        
        UITableViewCell *cellButton = [tableView dequeueReusableCellWithIdentifier:Buttondentifier];
        if (cellButton == nil) {
            cellButton = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:Buttondentifier] autorelease];
        }
        cellButton.imageView.image = nil;
        cellButton.detailTextLabel.text =@"";
        cellButton.textLabel.text  = @"";
        
      
        CGRect rect = CGRectMake(20, 0, 80, 45);
        
        UIButton *button=[[UIButton alloc] initWithFrame:rect];
        [button setFrame:rect];
        button.alpha = 1;
        UIImage *buttonImageNormal=[UIImage imageNamed:@"contactBtn.png"];
        [button setBackgroundImage:buttonImageNormal	forState:UIControlStateNormal];
        [button setContentMode:UIViewContentModeCenter];
        
        [button addTarget:self action:@selector(contactPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cellButton.contentView addSubview:button];
        [button release];
        
        
        CGRect rect2 = CGRectMake(200, 0, 80, 45);
        UIButton *button2=[[UIButton alloc] initWithFrame:rect2];
        [button2 setFrame:rect2];
        button2.alpha = 1;
        UIImage *buttonNormal=[UIImage imageNamed:@"mapBtn.png"];
        [button2 setBackgroundImage:buttonNormal	forState:UIControlStateNormal];
        [button2 setContentMode:UIViewContentModeCenter];
        
        [button2 addTarget:self action:@selector(mapBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cellButton.contentView addSubview:button2];
        [button2 release];
    
        return cellButton;

    cell.imageView.image = nil;
        cell.detailTextLabel.text =@"";
        cell.textLabel.text  = @"";
        
        
        CGRect rect = CGRectMake(20, 0, 80, 45);
      
        UIButton *button=[[UIButton alloc] initWithFrame:rect];
        [button setFrame:rect];
        button.alpha = 1;
        UIImage *buttonImageNormal=[UIImage imageNamed:@"contactBtn.png"];
        [button setBackgroundImage:buttonImageNormal	forState:UIControlStateNormal];
       [button setContentMode:UIViewContentModeCenter];
        
       [button addTarget:self action:@selector(contactPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        [button release];
       
        
        CGRect rect2 = CGRectMake(200, 0, 80, 45);
        UIButton *button2=[[UIButton alloc] initWithFrame:rect2];
        [button2 setFrame:rect2];
        button2.alpha = 1;
        UIImage *buttonNormal=[UIImage imageNamed:@"contactBtn.png"];
        [button2 setBackgroundImage:buttonNormal	forState:UIControlStateNormal];
        [button2 setContentMode:UIViewContentModeCenter];
        
        [button2 addTarget:self action:@selector(mapBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button2];
        [button2 release];

    }*/
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(void) getbuildingDetails{
    
    NSString *path=[[NSBundle mainBundle] pathForResource:@"buildingsdetails" ofType:@"dat"];
    NSError *error;
    
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    
    NSArray *allLines = [content componentsSeparatedByString: @"\n"];
    int count = [allLines count];
    
    [info addObject:[gradeLabels objectAtIndex:[self.grade intValue]]];
       
    for (int i=0; i<count; i++) {
        NSString *line = [allLines objectAtIndex:i];
        
        NSArray *items = [line componentsSeparatedByString: @"|"];
        NSString *typ = [items objectAtIndex:0];
        if ([typ isEqualToString:myId]) {
          //  int itemCount = [items count];
            
            [info addObject:[items objectAtIndex:1]];
            [info addObject:[items objectAtIndex:2]];
            [info addObject:[items objectAtIndex:3]];
            [info addObject:[items objectAtIndex:5]];
            [info addObject:[items objectAtIndex:11]];
            [info addObject:[items objectAtIndex:12]];
            [info addObject:[items objectAtIndex:13]];
            
          //  myPhone = [NSString stringWithFormat:@"%@",[items objectAtIndex:11]];
            
                  
            for(int i=0;i<9;i++)
            {
                [room addObject:[items objectAtIndex:i+14]];
            }
            for(int i=0;i<11;i++)
            {
                [build addObject:[items objectAtIndex:i+23]];
            }
            break;
        }
    }
    
}

-(IBAction)mapBtnPressed:(id)sender{
    
    myMap = [[MKMapView alloc] initWithFrame:self.view.bounds];
    myMap.delegate =self;
    myMap.mapType = MKMapTypeStandard;
    [self.view addSubview:myMap];
    [NSThread detachNewThreadSelector:@selector(displayMap) toTarget:self withObject:nil];
    _status=1;
    
    
}

-(IBAction)contactPressed:(id)sender{
    NSString *path=[[NSBundle mainBundle] pathForResource:@"buildingsdetails" ofType:@"dat"];
    NSError *error;
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    NSArray *allLines = [content componentsSeparatedByString: @"\n"];
    int count = [allLines count];
    for (int i=0; i<count; i++) {
        NSString *line = [allLines objectAtIndex:i];
        NSArray *items = [line componentsSeparatedByString: @"|"];
        NSString *typ = [items objectAtIndex:0];
        if ([typ isEqualToString:myId]) {
            NSString *myPhone = [NSString stringWithFormat:@"tel://0096612%@",[items objectAtIndex:5]];
              [[UIApplication sharedApplication] openURL:[NSURL URLWithString:myPhone]];
            NSLog(@"%@",myPhone);
            break;
        }
    }
}

-(void) displayMap{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    MKCoordinateRegion region; 
    MKCoordinateSpan span; 
    CLLocationCoordinate2D location; 
    span.latitudeDelta=0.001; 
    span.longitudeDelta=0.001; 
    NSError *error;
    NSString *path=[[NSBundle mainBundle] pathForResource:@"makkabuldings" ofType:@"dat"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    NSArray *allLines = [content componentsSeparatedByString: @"\n"];
    int count = [allLines count];
    for (int i=0; i<count-1; i++) {
        NSArray *items = [[allLines objectAtIndex:i] componentsSeparatedByString: @"|"];
        NSString *idd = [[NSString alloc] initWithFormat:@"%@",[items objectAtIndex:0]];
        if ([self.myId isEqualToString:idd]){
            NSString *title = [[NSString alloc] initWithFormat:@"%@",[items objectAtIndex:1]];
            //  NSString *title = [NSString stringWithFormat:@"%@",[items objectAtIndex:1]];
            location.latitude =  [[items objectAtIndex:4]doubleValue];
            NSLog(@"%f", location.latitude);
            location.longitude = [[items objectAtIndex:5]doubleValue];
            NSLog(@"%f", location.longitude);
        // Add the annotation to our map view
        MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:title andCoordinate:location];
        [self.myMap addAnnotation:newAnnotation];
        [newAnnotation release];
         //   [title release];
            
            break;
          }
        [idd release];
    }
    region.span=span; 
    region.center=location; 
    
    [myMap setRegion:region animated:TRUE]; 
    [myMap regionThatFits:region];
    
    
    [pool release];
    
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
   UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 30*highf)];
  //   UIView* customView = [[UIView   initWithFrame:CGRectMake(10.0, 0.0, 300.0, 30.0)];
	UIImageView *cusImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cusImg.png"]];
    cusImg.frame = CGRectMake(0.0, 0.0, 320.0, 30);
    [customView addSubview:cusImg];
    UIImageView *pLeft =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"point.png"]];
    pLeft.frame = CGRectMake(92, 11, 8, 8);
    [customView addSubview:pLeft];
    UIImageView *pRight =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"point.png"]];
    pRight.frame = CGRectMake(200, 11, 8, 8);
    [customView addSubview:pRight];
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.alpha=1;
	headerLabel.textColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:16];
    headerLabel.textAlignment = NSTextAlignmentCenter;
	// If you want to align the header text as centered
	headerLabel.frame = CGRectMake(100, 0.0, 100.0, 30*highf);
    
    
	if (section==1) {
        headerLabel.text =@"خدمات الغرف";
    }
    else if(section==2)
    {
          headerLabel.text = @"خدمات المبني";
    }
    else if(section==0)
    {
        headerLabel.text = @"بيانات عامة";
    }
   /* else if(section==3)
    {
        headerLabel.text = @"";
    }*/
        
	[customView addSubview:headerLabel];
    [headerLabel release];
	return customView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
   
	return 30.0*highf;
    
}


- (void)dealloc
{
    [info release];
    [room release];
    [build release];
    [services release];
    [buildLabels release];
    [infolabels release];
  /*  [myPhone release];
    [myName release];
    [myId release];
    [myMap release];*/
    [super dealloc];
     
}
@end
