//
//  SearchViewController.m
//  itour
//
//  Created by walid nour on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchViewController.h"
#import "BuildingViewController.h"
#import <QuartzCore/QuartzCore.h>
@implementation SearchViewController

@synthesize bgImage, mainTable;
@synthesize mainarr, filearr ,iddarr;
@synthesize townID, listType, buildID;
@synthesize titleName ,fileName;
@synthesize searchID;
@synthesize searchTxt;
@synthesize searchBtn;
@synthesize searcharr,buildarr; 
@synthesize searchTxtField,gradearr,radioButtons;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    mainarr = [[NSMutableArray alloc] init];
    iddarr  = [[NSMutableArray alloc] init];
    filearr = [[NSMutableArray alloc] init];
    gradearr= [[NSMutableArray alloc] init];
    
    self.radioButtons =[[NSMutableArray alloc]init];
    [self fillArr];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.leftBarButtonItem = barBackItem;
    [barBackItem release];
  
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,420*highf);
    [self.view addSubview:bgImage];
    
    buildName = @"الفنادق";
    NSLog(@"%i",[mainarr count]);
    
   // self.titleName=@"بحث";
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0*highf];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text =  @"بحث";
    [titleView sizeToFit];
    
    
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.view.backgroundColor = [UIColor clearColor];
    CGRect rect;
    if (listType==-1) {
        rect = CGRectMake(0, 20, 320, 200);
        CGRect rect2 = CGRectMake(15, 215, 290, 50);
        searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [searchBtn setImage:[UIImage imageNamed:@"searchBtn.png"] forState:UIControlStateNormal];
        
        searchBtn.frame = rect2;
        searchBtn.alpha = 1;
        searchBtn.titleLabel.text = @"ابحث";
        [searchBtn setContentMode:UIViewContentModeCenter];
        [searchBtn addTarget:self action:@selector(searchPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBtn];
        
    }
    else
    {
        rect = CGRectMake(10, 0.0, 320, 400);
    }

    mainTable = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=45.0;
    mainTable.separatorColor = [UIColor clearColor];
    mainTable.hidden = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    // [mainTable setBackgroundView:bgImage];
    if (listType==-1) {
        
    }
    
    [self.view addSubview:mainTable];
    self.title = titleName;
    
    
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    
}
/*- (void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}*/
- (void)viewDidUnload
{
     [self setMainarr:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mainarr count];
}

-(IBAction)textFieldDidChange:(id)sender{
    // [searchTxtField setPlaceholder:@""];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    cell.backgroundColor = [UIColor clearColor];
    if (listType==-1) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(indexPath.row==0)
        {
            searchTxtField = [[UITextField alloc] initWithFrame:CGRectMake(20, 5, 260, 35)];
            searchTxtField.delegate = self;
            searchTxtField.textAlignment =NSTextAlignmentRight;
            searchTxtField.borderStyle = UITextBorderStyleRoundedRect;
            // searchTxtField.text= @"";
            
            searchTxtField.textColor = [UIColor blackColor];
            searchTxtField.font = [UIFont systemFontOfSize:17.0];
            searchTxtField.backgroundColor = [UIColor clearColor];
            searchTxtField.autocorrectionType = UITextAutocorrectionTypeNo;
            searchTxtField.keyboardType = UIKeyboardTypeDefault;
            searchTxtField.returnKeyType = UIReturnKeyDone;
            searchTxtField.layer.borderWidth=5.0f;
            searchTxtField.layer.borderColor=[[UIColor greenColor] CGColor];
            searchTxtField.layer.cornerRadius=5.0f;
            //    [searchTxtField addTarget:self action:@selector(textFieldDidChange:)                   forControlEvents:UIControlEventEditingDidEnd];
            
            if (scr!=5) {
                
                [searchTxtField setPlaceholder:@"كلمة البحث:"];
                // scr=1;
            }
            else{
                [searchTxtField setPlaceholder:@""];
                //scr=1;
                
            }     [cell.contentView addSubview:searchTxtField];
          //  cell.backgroundColor = [UIColor clearColor];
            //[searchTxtField release];
        }
             else if(indexPath.row==1)
        {
             UIView *tmpView= [[UIView alloc]initWithFrame:CGRectMake(20, 5, 260, 150)];
            tmpView.backgroundColor = [UIColor clearColor];
            
            UILabel *tmplab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 260,30)];
            tmplab.text = @"اختر نوع السكن:";
            tmplab.textColor = [UIColor blackColor];
            tmplab.backgroundColor = [UIColor clearColor];
            tmplab.font = [UIFont boldSystemFontOfSize:18];
            tmplab.textAlignment = NSTextAlignmentRight;
            
            [tmpView addSubview:tmplab];
            
            NSString *btntitle = @"فنادق";
            for (int i=0; i<2; i++) {
              //  UIButton *bttTemp = [[UIButton alloc] initWithFrame:CGRectMake(200,35+(i*40),30,30)];
                  UIButton *bttTemp = [UIButton buttonWithType:UIButtonTypeCustom];
                  bttTemp.frame = CGRectMake(200,35+(i*40),30,30);
                [bttTemp addTarget:self action:@selector(ButtonClicked:)forControlEvents:UIControlEventTouchUpInside];
                bttTemp.tag = i;
                //[self.radioButtonst addObject:bttTemp];
                UIImage *img0=[UIImage imageNamed:@"radio-off.png"];
                UIImage *img2=[UIImage imageNamed:@"radio-on.png"];
                if (i==0) {
                   [bttTemp setImage:img2 forState:UIControlStateNormal];
                   
                }
                else{
                    [bttTemp setImage:img0 forState:UIControlStateNormal];
                     
                }
                 [self.radioButtons addObject:bttTemp];
                [tmpView addSubview:bttTemp];
                
                
                UILabel *bttLab =[[UILabel alloc]initWithFrame:CGRectMake(100, 30+(i*40), 100,40)];
                bttLab.text = btntitle;
                bttLab.textColor = [UIColor blackColor];
                bttLab.font = [UIFont boldSystemFontOfSize:15];
                 bttLab.backgroundColor = [UIColor clearColor];
                bttLab.textAlignment = NSTextAlignmentCenter;
              /*  [bttTemp setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                UIImageView *img1=[[UIImageView alloc]initWithFrame:CGRectMake(100, 5, 30, 30)];
                
                [bttTemp setTitle:btntitle forState:UIControlStateNormal];
                bttTemp.titleLabel.font =[UIFont boldSystemFontOfSize:15.0];
                [bttTemp setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, -2,0)];*/
                
                 [tmpView addSubview:bttLab];
                //[bttTemp release];
                btntitle=@"شقق";
            }
            
            
            
            
             [cell.contentView addSubview:tmpView];
            /*cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.textAlignment = NSTextAlignmentRight;
            cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
            cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
            cell.textLabel.numberOfLines = 0;
            cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
            cell.detailTextLabel.text = buildName;
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.lineBreakMode=NSLineBreakByCharWrapping;
            cell.detailTextLabel.numberOfLines=0;
            // cell.backgroundColor = [UIColor clearColor];*/
        }
        
    }
    else 
    {
    // Configure the cell...
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.textLabel.numberOfLines = 0;
        cell.textLabel.textColor = [UIColor blackColor];
       // cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat customHeight;
if (listType==-1) {
        if(indexPath.row==1)
        {      customHeight =115;
        
        
    }
    else {
        
        customHeight =45;
    }
    }
else {
     customHeight =45;
}
    
    return customHeight;
}
-(IBAction) ButtonClicked:(id) sender{
    
    buildID = [sender tag];
    NSLog(@"%i",buildID);
    
    for(int i=0;i<[self.radioButtons count];i++){
        // MODIFIED:
        UIButton *btTemp = [self.radioButtons objectAtIndex:i];
        [btTemp setImage:[UIImage imageNamed:@"radio-off.png"] forState:UIControlStateNormal];
        
    }
    [sender setImage:[UIImage imageNamed:@"radio-on.png"] forState:UIControlStateNormal];
    

    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (listType==-1) {
        /*if (indexPath.row!=0) {
            SearchHomeController *detailViewController = [[SearchHomeController alloc] initWithNibName:@"ListViewController" bundle:nil];
            // ...
            // Pass the selected object to the new view controller.
            
            detailViewController.listType=indexPath.row;
            if (indexPath.row==1) {
               detailViewController.checkID =buildID ;
            }
           
            detailViewController.titleName = [mainarr objectAtIndex:indexPath.row];
            detailViewController.sv = self;
            [self.navigationController pushViewController:detailViewController animated:YES];
            [detailViewController release];
        }*/
    }
    else if(listType==4)
    {
        if (iddarr.count>0) {
           
    BuildingViewController *detailViewController = [[BuildingViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    NSLog(@"%@",[iddarr objectAtIndex:indexPath.row] );
    detailViewController.myId = [iddarr objectAtIndex:indexPath.row];
    detailViewController.myName = [mainarr objectAtIndex:indexPath.row];
    detailViewController.grade = [gradearr objectAtIndex:indexPath.row];
    
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    }
   
    }
}

- (void) fillArr
{
    if (listType==-1) {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"",@"اختر نوع المنشأة", nil];
        
        
        buildarr = [[NSMutableArray alloc] initWithObjects:@"الفنادق",@"الشقق", nil];
        scr=1;
        

    }
    else if (listType==4)
    {
        scr=5;
        filearr = [[NSMutableArray alloc] initWithObjects: @"hotel",@"house", nil];
        NSString *type;
        NSLog(@"%i",self.buildID);
        NSLog(@"%@",self.searchTxt);
        if (self.buildID==1) {
            type = @"2";
        }
        else if (self.buildID==0) {
            type = @"1";
        }
        NSString *path=[[NSBundle mainBundle] pathForResource:@"buildings" ofType:@"dat"];
        NSError *error;
        
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        
        
        NSArray *allLines = [content componentsSeparatedByString: @"\n"];
        int count = [allLines count];
        NSLog(@"%d", count);
        
        for (int i=0; i<count; i++) {
            NSString *line = [allLines objectAtIndex:i];
            
            NSArray *items = [line componentsSeparatedByString: @"|"];
            NSString *typ = [items objectAtIndex:4];
            NSString *name =  [items objectAtIndex:3];
            NSRange tmp =[name rangeOfString:searchTxt options:NSCaseInsensitivePredicateOption|NSOrderedSame ];
            
            NSLog(@"%d",tmp.length);
    
      
            
         /*  if ([type isEqualToString:typ] &&[searchTxt isEqualToString:name] ) {
          //   if ([type isEqualToString:typ] && tmp.location==0 ) {
           
                [self.mainarr addObject:[items objectAtIndex:3]];
                [self.iddarr addObject:[items objectAtIndex:0]];
                [self.gradearr addObject:[items objectAtIndex:5]];
                
                
            }*/
            if ([type isEqualToString:typ] &&[name rangeOfString:searchTxt].location != NSNotFound ) {
                //   if ([type isEqualToString:typ] && tmp.location==0 ) {
                
                [self.mainarr addObject:[items objectAtIndex:3]];
                [self.iddarr addObject:[items objectAtIndex:0]];
                [self.gradearr addObject:[items objectAtIndex:5]];
                
                
            }
        }
    if ([mainarr count]==0) {
        [mainarr addObject:@"لا توجد نتائج"];
    }
        
   

    }
}

-(IBAction)searchPressed:(id)sender{
  
    
    SearchViewController *detailViewController = [[SearchViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    
    detailViewController.listType=4;
    detailViewController.titleName = @"نتيجة البحث";
    detailViewController.buildID = self.buildID;
      detailViewController.searchTxt = self.searchTxtField.text;
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    searchTxt = textField.text;
   
	[textField resignFirstResponder];
    return YES;
}



-(void) dealloc{
     [searchTxtField release];
    [super dealloc];
}
    

@end
