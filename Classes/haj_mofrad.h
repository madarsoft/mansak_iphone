//
//  haj_mofrad.h
//  mutnav2
//
//  Created by rashad on 8/14/14.
//
//

#import <UIKit/UIKit.h>
#import "custom_pagecontrol.h"

@interface haj_mofrad : UIViewController<UIScrollViewDelegate>{
    NSMutableArray *array;
    NSMutableArray *array2;
    NSMutableArray *array3;
    int check;
    BOOL naimation;
}
@property (nonatomic, retain) IBOutlet UIScrollView* scrollView;
@property (retain, nonatomic) custom_pagecontrol *pageControl;
@property (retain, nonatomic) NSMutableDictionary *mansak_data;
@property (retain, nonatomic) NSString *hajj_plist;
@property (retain, nonatomic) UILabel *title_lable;
@property (retain, nonatomic) UILabel *second_title_lable;
@property (retain, nonatomic) UIButton *back_btn;

@property (retain, nonatomic) NSString *title_lable_text;

@property int mansak_day;
@property (retain, nonatomic) IBOutlet UIView * content_view_4in;
@property (retain, nonatomic) IBOutlet UIView *header_view;
@property (retain, nonatomic) IBOutlet UIView *footer_view;
@property (retain, nonatomic) IBOutlet UIButton *back;
@property (retain, nonatomic) IBOutlet UIView *content_view;

@end
