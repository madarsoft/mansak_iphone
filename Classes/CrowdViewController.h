//
//  CrowdViewController.h
//  mutnav2
//
//  Created by walid nour on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BugSense-iOS/BugSenseController.h>
@class Reachability;
@interface CrowdViewController : UIViewController

<UITableViewDataSource,UITableViewDelegate>


@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (nonatomic, retain) NSMutableArray *info;
@property (nonatomic, retain) NSMutableArray *infolabels;
@property (retain, nonatomic) NSMutableArray *tawefLabels;
@property (retain, nonatomic) NSMutableArray *tawef;
@property (retain, nonatomic) NSMutableArray *saaea;

-(void) getbuildingDetails;
@end
