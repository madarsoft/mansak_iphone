//
//  menu_mainViewController.h
//  mutnav2
//
//  Created by rashad on 7/9/14.
//
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "NewMainViewController.h"
#import "manasik.h"
#import "select_haj_type.h"
#import "PrayTimeViewController.h"
#import "GridViewController.h"
#import "webviewcontroller.h"
#import "GetHijriDate.h"
#import "TatwefViewController.h"
#import "HaramViewController.h"
#import "HamlatViewController.h"
#import "NewsListViewController.h"
#import "MyListViewController.h"
#import "GAIDictionaryBuilder.h"



@interface menu_mainViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>{
    CGRect hide_position;
    CGRect show_position;
    CGRect menu_button_original_position;
    CGRect menu_button_hide_position;
    UIImage *select_img;
    UIImageView *select_img_v;
    UIImageView *select_img_back;
    NSMutableArray *menu_arr;

    BOOL check_anim;
    MyListViewController *listController;
    HaramViewController *channl_view;
    NewsListViewController *news_View;
}
@property (nonatomic, strong)MyListViewController *detailViewController;
@property (nonatomic, strong)UINavigationController *navController;
@property (nonatomic, strong)UINavigationController *channlController;
@property (nonatomic, strong)UINavigationController *newsController;
@property (nonatomic, strong)IBOutlet UITableView *main_table;
@property (nonatomic, strong)UIView *main_view;
@property (nonatomic, strong)UIView *SUBVIEW;
@property (nonatomic, strong)UIButton *show_menu_button;
@property (nonatomic, strong)UIButton *hide_view_button;
@property (nonatomic, strong)UIButton *city_btn;
@property (nonatomic, strong)UIButton *arrow_btn;
@property (nonatomic, strong)UIImageView *background_image;

@end
