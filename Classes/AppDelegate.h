//
//  AppDelegate.h
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "SKMenu.h"
#import "menu_mainViewController.h"

@interface AppDelegate : NSObject <UIApplicationDelegate,NSXMLParserDelegate,UIAlertViewDelegate,NSXMLParserDelegate> {
    
   UIWindow *window;
   UINavigationController *navigationController;
    NSString *currentKey;
    NSString *currenturl;
    NSMutableString *currentStringValue;
    NSMutableString *urlvalue;
    menu_mainViewController *main_menu;

    int isAPN;
    NSString* APN_message;

}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *navigationController;
@property (nonatomic,retain) UIView *news_view;

@property (nonatomic, retain)menu_mainViewController *main_menu;
@property (nonatomic, retain) SKMenu *myData;
@property (nonatomic) BOOL videoIsInFullscreen;
@property int netState;
@property(nonatomic,strong) NSMutableArray *versions;
@property(nonatomic,strong) NSMutableDictionary *version;
@property(nonatomic,strong) NSString *currentElement;
@property(nonatomic,strong) NSMutableString *ElementValue;
@property BOOL errorParsing;


-(void)registerApp;
-(void)getVersion;


@end

