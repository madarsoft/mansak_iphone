//
//  WeatherViewController.m
//  mutnav2
//
//  Created by walid nour on 2/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WeatherViewController.h"
#import "RXMLElement.h"
#import "Reachability.h"

@implementation WeatherViewController
@synthesize bgImage, mainTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,420);
    
    
    mainTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 30, 320, 390) style:UITableViewStyleGrouped]autorelease];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 0.7;
    mainTable.rowHeight=40.0;
    mainTable.hidden = NO;
    // mainTable.sectionHeaderHeight = 20.0;
    mainTable.allowsSelection = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    
 
    
   // self.title = @"الطقس";
    
    
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor greenColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = @"الطقس";
    [titleView sizeToFit];
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
       // UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,50,30)];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 50, 30);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [barBackItem release];
    }
    
}

- (void)viewDidUnload
{
    [self setBgImage:nil];
    [self setMainTable:nil];
    [super viewDidUnload];


    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 30.0)];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor blackColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:20];
    //	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
	// If you want to align the header text as centered
	headerLabel.frame = CGRectMake(150.0, 0.0, 300.0, 30.0);
    
    
	if (section==0) {
        headerLabel.text =@"مكة";
    }
    else if(section==1)
    {
        headerLabel.text = @"المدينة";
    }
        
	[customView addSubview:headerLabel];
    [headerLabel release];
	return customView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
	return 30.0;
    
}

-(NSMutableArray *) getTemp{
    NSMutableArray * temps = [[NSMutableArray alloc] init ];
    NSMutableArray * cities = [[NSMutableArray alloc] initWithObjects:@"1939897",@"1937801", nil];
    
    Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {
        for (int i=0; i<2; i++) {
            NSString *tmpUrl = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=%@&u=c", [cities objectAtIndex:i]];
            
            RXMLElement *parser = [RXMLElement elementFromURL:[NSURL URLWithString:tmpUrl]];
            
            
            [parser iterateWithRootXPath:@"//item" usingBlock: ^(RXMLElement *item) {
                NSLog(@"Player #5: %@", [item child:@"description"]);
                NSString *txt  = [NSString stringWithFormat:@"%@",[item child:@"description"]];
                NSArray *tmp = [txt componentsSeparatedByString:@" "];
                NSString *currentTemp;
                if ([[tmp objectAtIndex:5]length ]>2) {
                    currentTemp = [tmp objectAtIndex:6];
                }
                else {
                    currentTemp = [tmp objectAtIndex:5];
                }
                // NSLog(@"====%@",currentTemp);
                if (currentTemp==NULL) {
                    currentTemp = @"";
                }
                NSLog(@"%@",currentTemp);
                [temps addObject:currentTemp];
                
            }];
            
            
            
        }
    }
    else
    {
        for (int i=0; i<2; i++) {
            [temps addObject:@"0"];
        }
    }
    
    //[parser release];
    
    
    
    return temps;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    if(indexPath.section==0)
    {
        cell.textLabel.textAlignment   =  UITextAlignmentLeft;
        cell.textLabel.text =[[self getTemp] objectAtIndex:0];
        cell.detailTextLabel.text =  @"مكة المكرمة:";
    }
    else if (indexPath.section == 1)
    {
        cell.textLabel.textAlignment   =  UITextAlignmentLeft;
        cell.textLabel.text = [[self getTemp] objectAtIndex:1];
        cell.detailTextLabel.text = @"المدينة النبوية:";
    }

    
    return cell;
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count =0;
    if (section==0 || section == 1) {
        count =1;
    }
  
    return count;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
