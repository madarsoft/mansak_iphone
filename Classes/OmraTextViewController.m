//
//  OmraTextViewController.m
//  mutnav2
//
//  Created by walid nour on 3/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OmraTextViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Global.h"
@implementation OmraTextViewController


@synthesize bgImage;
@synthesize mytext,segments,from;
//@synthesize arrdoaa,arrmanasek;
@synthesize sdic,myid,plist;;
@synthesize sizeindex;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
     sizeindex = text_size;
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0*highf, 320,430*highf);
    
    
    myid=[sdic objectForKey:@"id"];
    mytext = [[UITextView alloc] initWithFrame:CGRectMake(20, 90*highf, 280,295*highf)];
    mytext.delegate =self;
    mytext.autoresizesSubviews = YES;
    mytext.backgroundColor = [UIColor clearColor];
    mytext.alpha = 1;
    mytext.font = [UIFont boldSystemFontOfSize:14*highf];    
    mytext.editable = NO;
    mytext.textAlignment = NSTextAlignmentRight;
    [mytext.layer setBorderColor: [[UIColor colorWithRed:0.314 green:0.243 blue:0.227 alpha:1.0] CGColor]];
    [mytext.layer setBorderWidth: 3.0];
    [mytext.layer setCornerRadius:15.0f];
    [mytext.layer setMasksToBounds:YES];
    mytext.layer.shouldRasterize = YES;
    mytext.font = [UIFont boldSystemFontOfSize:sizeindex];

    // [txtdetails.layer setShadowRadius:2.0f];
    mytext.layer.shadowColor = [[UIColor blackColor] CGColor];
    // txtdetails.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    //txtdetails.layer.shadowOpacity = 1.0f;
    mytext.layer.shadowRadius = 1.0f;

    
    segments = [[UISegmentedControl alloc] initWithFrame:CGRectMake(8, 45*highf, 296, 40*highf)];
       segments.backgroundColor = [UIColor clearColor];
    segments.hidden = NO;
    [segments setBackgroundImage:[UIImage imageNamed:@"blacksea.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segments setBackgroundImage:[UIImage imageNamed:@"greensea.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [segments setBackgroundImage:[UIImage imageNamed:@"greensea.png"] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    [segments setDividerImage:[UIImage imageNamed:@"hHead2.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segments setDividerImage:[UIImage imageNamed:@"hHead.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segments setDividerImage:[UIImage imageNamed:@"hHead2.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [segments setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                      [UIFont boldSystemFontOfSize:16.0],UITextAttributeFont,
                                      [UIColor whiteColor], UITextAttributeTextColor,
                                      [UIColor clearColor], UITextAttributeTextShadowColor,
                                      [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                      nil] forState:UIControlStateNormal];

    [segments setSelectedSegmentIndex:2];
    
    [segments addTarget:self action:@selector(changefile:) forControlEvents:UIControlEventValueChanged];
	
    int x = [myid intValue];
    //self.title=[sdic objectForKey:@"name"];
    
        


    
    /*
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor greenColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = [sdic objectForKey:@"name"];
    [titleView sizeToFit];
  	*/
	if(x==1||x==2)
	{
        [segments insertSegmentWithTitle:@"الدعاء" atIndex:1 animated:YES];
        [segments insertSegmentWithTitle:@"أخطاء شائعة" atIndex:0 animated:YES];
        [segments insertSegmentWithTitle:@"المناسك" atIndex:2 animated:YES];
        [segments setSelectedSegmentIndex:2];
    }
    else if(x==0||x==3) {
        [segments insertSegmentWithTitle:@"المناسك" atIndex:1 animated:YES];
        [segments insertSegmentWithTitle:@"أخطاء شائعة" atIndex:0 animated:YES];
        [segments setSelectedSegmentIndex:1];
	}
//	[self addfiletotextview:[[NSString alloc] initWithFormat:@"%@%@",plist,myid]];	
    	[self addfiletotextview:[NSString stringWithFormat:@"%@%@",plist,myid]];	
    [self.view addSubview:bgImage];
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor]; 
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text = [sdic objectForKey:@"name"];
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    titleLabel.numberOfLines=0;
    //[titleLabel sizeToFit];
    [self.view addSubview:titleLabel];

    [self.view addSubview:mytext];
    [self.view addSubview:segments];
        
    CGRect increct = CGRectMake(130, 370*highf, 30,30*highf);
    UIButton *incbutton=[[UIButton alloc] initWithFrame:increct];
    [incbutton setFrame:increct];
    incbutton.alpha = 1;
    incbutton.tag = 1;
    UIImage *buttonImageNormal=[UIImage imageNamed:@"incBtn.png"];
    [incbutton setBackgroundImage:buttonImageNormal	forState:UIControlStateNormal];
    [incbutton setContentMode:UIViewContentModeCenter];
    [incbutton addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect decrect = CGRectMake(160, 370*highf,30,30*highf);
    UIButton *decbutton=[[UIButton alloc] initWithFrame:decrect];
    [decbutton setFrame:decrect];
    decbutton.alpha = 1;
    decbutton.tag =2;
    UIImage *decImageNormal=[UIImage imageNamed:@"decBtn.png"];
    [decbutton setBackgroundImage:decImageNormal	forState:UIControlStateNormal];
    [decbutton setContentMode:UIViewContentModeCenter];
    [decbutton addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:incbutton];
    [self.view addSubview:decbutton];
    [decbutton release];
    [incbutton release];
    
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [self.mytext resignFirstResponder];
    return NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    /*
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
     //   UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,50,30)];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 50, 30);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [barBackItem release];
    }
    */
}

-(IBAction)changeSize:(id)sender{
    
    int i = [sender tag];
    if (i==1) {
        sizeindex++;
    }
    else if(i==2)
    {
        sizeindex--;
    }
    if (sizeindex<14) {
        sizeindex=14;
    }
   mytext.font = [UIFont boldSystemFontOfSize:sizeindex];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"size"
                                                     ofType:@"txt"];
    NSString *size=[[NSString alloc] init];
    size=[NSString stringWithFormat:@"%d",sizeindex];
    [size writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    text_size=sizeindex;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    [self setFrom:nil];
   [self setBgImage:nil];
      [self setMytext:nil];
    [self setSdic:nil];
    [self setSegments:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)changefile:(id) sender{
	
    int x = [myid intValue];
  	if(x==1||x==2)
	{
        if([sender selectedSegmentIndex]==1){
            NSInteger randomint=arc4random()%14+1;
         //   NSString *randomfile=[[NSString alloc] initWithFormat:@"doaa%d",randomint];
               NSString *randomfile=[NSString stringWithFormat:@"doaa%d",randomint];
            [self addfiletotextview:randomfile];
            NSLog(@"%d",[sender selectedSegmentIndex]);
        }
        else if([sender selectedSegmentIndex]==0){
            
          //  NSString *mfile=[[NSString alloc] initWithFormat:@"omraerr%i",x];
            
  NSString *mfile=[NSString stringWithFormat:@"omraerr%i",x];
            [self addfiletotextview:mfile];
            NSLog(@"%@",mfile);
        }
        else if([sender selectedSegmentIndex]==2){
            
        //    [self addfiletotextview:[[NSString alloc] initWithFormat:@"%@%@",plist,myid]];
                [self addfiletotextview:[NSString stringWithFormat:@"%@%@",plist,myid]];
            NSLog(@"%d",[sender selectedSegmentIndex]);
        }
      }
	else if(x==0||x==3) {
      if([sender selectedSegmentIndex]==0){
            
           // NSString *mfile=[[NSString alloc] initWithFormat:@"omraerr%i",x];
           NSString *mfile=[NSString stringWithFormat:@"omraerr%i",x];
            [self addfiletotextview:mfile];
               NSLog(@"%@",mfile);
        }
        else if([sender selectedSegmentIndex]==1){
            
         //   [self addfiletotextview:[[NSString alloc] initWithFormat:@"%@%@",plist,myid]];	
               [self addfiletotextview:[NSString stringWithFormat:@"%@%@",plist,myid]];
        	}

	}

}

-(void)addfiletotextview:(NSString*)filename{
	NSFileManager *filemgr;
	
	filemgr = [NSFileManager defaultManager];
	//plist=[[NSString alloc] initWithFormat:filepath,plist];
	
	NSString *path=[[NSBundle mainBundle] pathForResource:filename ofType:@"dat"];
	if ([filemgr fileExistsAtPath: path ] == YES)
	{
		
		//menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
		
		//NSData *databuffer;
	//	databuffer = [filemgr contentsAtPath: path ];

        NSError *error;
        
	//	mytext.text=[[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        mytext.text=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
		//NSString* aStr = [[NSString alloc] initWithData:databuffer encoding:NSASCIIStringEncoding];
		
	}
	
}
@end
