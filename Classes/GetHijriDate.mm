//
//  GetHijriDate.m
//  mutnav2
//
//  Created by Waleed Nour on 8/28/14.
//
//

#import "GetHijriDate.h"
#import "HijriTime.h"

@implementation GetHijriDate

+(NSString *)GetHiriDate{
    Date melady_today;
    NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hijridate objectAtIndex:i] intValue];
    int	month_number = hegryToday[1];
    NSLog(@"%d ", month_number);
    NSLog(@"%d ", hegryToday[0]);
    int	year_number = hegryToday[2];
    NSString* month_name =NSLocalizedString([HijriTime getArabicMonth:month_number], @"") ;
    NSString* year=[NSString stringWithFormat:@"%ld",(long)year_number];
    
    return [NSString stringWithFormat:@"%d %@ %@",hegryToday[0],month_name,year];
}

+(int )GetDayIndex{
    int di = 0;
    
    Date melady_today;
    NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hijridate objectAtIndex:i] intValue];
    int day_nubmer =  hegryToday[0];
    int	month_number = hegryToday[1];
    if (month_number == 12) {
        if (day_nubmer >= 7 || day_nubmer <= 13) {
            di = day_nubmer-6;
        }
    }
    return di;
}

@end
