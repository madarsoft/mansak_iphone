//
//  HamlatViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 5/31/15.
//
//
#import "Global.h"
#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface HamlatViewController : GAITrackedViewController<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UIButton *returnBtn;


@property int type;
@property int scr;

@end
