//
//  webviewcontroller.m
//  gadaweltest
//
//  Created by amr on 9/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "webviewcontroller.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@implementation webviewcontroller
@synthesize mypage,myview,mywebview,viewwidth,mypreloadingview,imgpreloading,lblpreloading,webDisplayiPad;
@synthesize type,urlStr;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	
    [super viewDidLoad];
       [self updateToolbar];
}


-(void)webViewDidStartLoad:(UIWebView *)webView {
//    NSLog(@"web did started loading");
    [self updateToolbar];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
//	NSLog(@"web did finished loading");
    // Remove loading image from view
    [self updateToolbar];
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [mypreloadingview removeFromSuperview];
	
}
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.view.frame=CGRectMake(0, 0, 320, 480*highf);
 webDisplayiPad.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    webDisplayiPad.scalesPageToFit = YES;
    webDisplayiPad.delegate = self;
   
   /* if([webDisplayiPad respondsToSelector:@selector(scrollView)]){
        
        [webDisplayiPad.scrollView setScrollEnabled:NO];
        webDisplayiPad.scrollView.bounces=NO;
    }*/
    
 /*   if (type==1) {
        self.navigationController.navigationBarHidden=YES;
    }
    else{
        self.navigationController.navigationBarHidden=YES;
        UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
        headImg.frame=CGRectMake(0, 0, 320, 50*highf);
        [self.view addSubview:headImg];
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment=NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor greenColor];
        titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        if (type==2){
            titleLabel.text =@"دليل الحملات";}
        else {
            titleLabel.text =@"حملتي";}
        
        titleLabel.font = [UIFont boldSystemFontOfSize:18];
        titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
        titleLabel.numberOfLines=0;
        [self.view addSubview:titleLabel];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 5, 50, 30);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
    }*/
    
    
  /*  UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30*highf);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.leftBarButtonItem = barBackItem;
    [barBackItem release];*/
    
    if (type==2) {
        UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]];
        headImg.frame=CGRectMake(0, 0, 320, 50*highf);
        [self.view addSubview:headImg];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment=NSTextAlignmentCenter;
        titleLabel.textColor = [UIColor whiteColor];
        titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        titleLabel.text =@"برامجنا";
        
        titleLabel.font = [UIFont boldSystemFontOfSize:18];
        titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
        titleLabel.numberOfLines=0;
          [self.view addSubview:titleLabel];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 5*highf, 50, 30*highf);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
            [self.view addSubview:backButton];
        [webDisplayiPad setFrame:CGRectMake(0, 40*highf, 320, 420*highf)];
        
    }
    else{
        	[webDisplayiPad setFrame:CGRectMake(0, 0, 320, 480*highf)];
    }
   

    
    int x=lblpreloading.frame.origin.x*(viewwidth/320);
    int y=lblpreloading.frame.origin.y;
    int width=lblpreloading.frame.size.width*(viewwidth/320);
    int height=lblpreloading.frame.size.height*(420.0/460.0);
//	[lblpreloading setFrame:CGRectMake(x, y, width, height)];
	x=imgpreloading.frame.origin.x*(viewwidth/320);
    y=imgpreloading.frame.origin.y;
	width=imgpreloading.frame.size.width;
	height=lblpreloading.frame.size.height;
//	[imgpreloading setFrame:CGRectMake(x, y, width, height)];
	[myview setFrame:CGRectMake(0, 0, 320, 478*highf)];
	//[mypreloadingview setFrame:CGRectMake(0, 0, viewwidth, 375)];
//
    /*
    Reachability *curReach = [[Reachability reachabilityForInternetConnection] retain];
    [curReach startNotifier];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    //NSLog(@"netstatus is %@",netStatus);
    if (netStatus !=NotReachable) {
      lblpreloading.hidden=NO;
        imgpreloading.hidden=NO;
        mypreloadingview.hidden=NO;
    
     }
    else{
        lblpreloading.hidden=YES;
        imgpreloading.hidden=YES;
         mypreloadingview.hidden=YES;
        [lblpreloading removeFromSuperview];
        [imgpreloading removeFromSuperview];
        [mypreloadingview removeFromSuperview];
    }*/
    
    
    NSString *urlAddress;
    if (type==1) {
        self.navigationController.navigationBarHidden=YES;
     //   self.webToolbar.hidden =NO;
       urlAddress =[NSString stringWithFormat:@"http://api.madarsoft.com/mobileads_NFcqDqu/v3/pages/page.aspx?programid=28&name=wemansak"] ;
        //   urlAddress =[NSString stringWithFormat:@"http://www.alharamain.gov.sa/index.cfm?do=cms.AudioDetails&audioid=4535&audiotype=lectures&browseby=speaker"] ;
    }
    else if (type==2) {
        urlAddress =[NSString stringWithFormat:@"http://api.madarsoft.com/mobileads_NFcqDqu/v1/pages/page.aspx?programid=6&name=programsmutawef"] ;
    }
    else if (type==3){
        self.navigationController.navigationBarHidden=NO;
        //self.webToolbar.hidden =NO;
        urlAddress =[NSString stringWithFormat:@"http://www.gph.gov.sa/section/2/%D8%A7%D9%84%D8%A7%D8%AE%D8%A8%D8%A7%D8%B1"] ;
    }
    else if (type==4){
        [self.webToolbar removeFromSuperview];
       //  self.webToolbar.hidden =NO;
        urlAddress =[NSString stringWithFormat:@"http://www.alharamain.gov.sa/index.cfm?do=cms.ScholarsList&audiotype=lectures&browseby=speaker"] ;
    }

        
	
	//Create a URL object.
	NSURL *url = [NSURL URLWithString:urlAddress];
  
	//URL Requst Object
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url  ];
    [self.webDisplayiPad loadRequest:requestObj ];
      
  //  if (netStatus !=NotReachable) {
      //  [self.webDisplayiPad loadRequest:requestObj ];
  //* }
    /*else {
		[self afterconnectionfail];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"تنبيه"
                                                          message:@"لا يوجد اتصال بالإنترنت."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
        
    }*/
    
}

-(void)updateToolbar {
    
    UIBarButtonItem *backButton =	[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backIcon.png"] style:UIBarButtonItemStylePlain target:webDisplayiPad action:@selector(goBack)];
    UIBarButtonItem *forwardButton =	[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"forwardIcon.png"] style:UIBarButtonItemStylePlain target:webDisplayiPad action:@selector(goForward)];
    
    [backButton setEnabled:webDisplayiPad.canGoBack];
    [forwardButton setEnabled:webDisplayiPad.canGoForward];
    
    UIBarButtonItem *refreshButton = nil;
    if (webDisplayiPad.loading) {
        refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:webDisplayiPad action:@selector(stopLoading)];
    } else {
        refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:webDisplayiPad action:@selector(reload)];
    }
    
    UIBarButtonItem *openButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareAction)];
    UIBarButtonItem *spacing       = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *contents = [[NSArray alloc] initWithObjects:backButton, spacing, forwardButton, spacing, spacing, spacing, openButton, nil];
    //NSArray *contents = [[NSArray alloc] initWithObjects:backButton, spacing, forwardButton, spacing, refreshButton, spacing, openButton, nil];
    
    [self setToolbarItems:contents animated:NO];
    
    
    
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    //   [self removeLoadingView];
   /* NSLog(@"Error for WEBVIEW: %@", [error description]);
	[self afterconnectionfail];
    if([error code]!=NSURLErrorDomain){
	UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"تنبيه"
													  message:@"فشل في الاتصال"
													 delegate:nil
											cancelButtonTitle:@"OK"
											otherButtonTitles:nil];
	[message show];
    }*/
    [self updateToolbar];
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    lblpreloading.hidden=YES;
   imgpreloading.hidden=YES;
    mypreloadingview.hidden=YES;
	
}
-(void) afterconnectionfail{
	lblpreloading.text=@"فشل في الاتصال";
	imgpreloading.hidden=YES;
	
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    webDisplayiPad.delegate=nil;
	//[viewwidth release];
}


- (IBAction)goBack:(id)sender {
    if ([webDisplayiPad canGoBack]) {
        [webDisplayiPad goBack];
    }
}
@end
