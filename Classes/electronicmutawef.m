//
//  electronicmutawef.m
//  mutnav2
//
//  Created by amr on 1/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "electronicmutawef.h"


@implementation electronicmutawef
@synthesize bgImage;
@synthesize elecmutawef,imgindex,mytext,arrdoaa,arrmanasek,segments,lbltitle,from;


-(IBAction) next_tawaf:(id)sender{
	if(imgindex<[arrmanasek count]-1)
	imgindex++;
	if ([from isEqualToString:@"1"] && imgindex>=7) {
		imgindex=7;
	}
	NSLog(@"%d",imgindex);
	
	if(![from isEqualToString:@"0"] &&( imgindex<=7 ||(imgindex>=11 && imgindex<=17)))
	{
	
		NSString *newimg=[[NSString alloc] initWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
		elecmutawef.image=[UIImage imageNamed:newimg];
		elecmutawef.hidden=NO;
		
	}
	else {
		elecmutawef.hidden=YES;
	}

	lbltitle.text=[[arrmanasek objectAtIndex:imgindex] objectForKey:@"title"];
	//NSString *filename=[sdic objectForKey:@"manasek"];
	[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
	segments.selectedSegmentIndex=0;
	
	
}
-(IBAction) previous_tawaf:(id)sender{
	if(imgindex>0){
	imgindex--;
		
		if(![from isEqualToString:@"0"] &&( imgindex<=7||(imgindex>=11 && imgindex<=17))){
		NSString *newimg=[[NSString alloc] initWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
		elecmutawef.image=[UIImage imageNamed:newimg];
			elecmutawef.hidden=NO;
		}
		else {
			elecmutawef.hidden=YES;
		}

	}
	lbltitle.text=[[arrmanasek objectAtIndex:imgindex] objectForKey:@"title"];
	[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
	segments.selectedSegmentIndex=0;
	
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/
-(IBAction)changefile:(id) sender{
	
	//NSLog(@"%d",randomint);
	if([sender selectedSegmentIndex]==1){
		NSInteger randomint=arc4random()%14+1;
		NSString *randomfile=[[NSString alloc] initWithFormat:@"doaa%d",randomint];
	[self addfiletotextview:randomfile];
	NSLog(@"%d",[sender selectedSegmentIndex]);
	}
	else {
		[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
	}

	//plist=;
	//[self addfiletotextview:[[NSString alloc] initWithFormat:@"%@_%d",[sdic objectForKey:@"targetname"],[sender selectedSegmentIndex]]];
}
-(void)addfiletotextview:(NSString*)filename{
	NSFileManager *filemgr;
	
	filemgr = [NSFileManager defaultManager];
	//plist=[[NSString alloc] initWithFormat:filepath,plist];
	
	NSString *path=[[NSBundle mainBundle] pathForResource:filename ofType:@"dat"];
	if ([filemgr fileExistsAtPath: path ] == YES)
	{
		
		//menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
		
		NSData *databuffer;
		databuffer = [filemgr contentsAtPath: path ];
		//NSLog(@"vvvvv%s",databuffer);
		mytext.text=[[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
		//NSString* aStr = [[NSString alloc] initWithData:databuffer encoding:NSASCIIStringEncoding];
		
	}
	
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	NSLog(@"%@",from);
	imgindex=0;
	elecmutawef.image=[UIImage imageNamed:@"t0.png"];
	
	NSString *mypath=[[NSBundle mainBundle] pathForResource:@"elec" ofType:@"plist"];
	arrmanasek=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
	sdic=[arrmanasek objectAtIndex:0];
	lbltitle.text=[sdic objectForKey:@"title"];
	NSString *filename=[sdic objectForKey:@"manasek"];
	[self addfiletotextview:filename];
	//arrmanasek=[NSMutableArray];
	if([from isEqualToString:@"0"])
	{
		elecmutawef.hidden=YES;
		mytext.hidden=NO;
		segments.hidden=NO;
		mytext.frame=CGRectMake(20, 76, 280, 270);
		segments.frame=CGRectMake(20, 10, 280, 60);
	}
	else if([from isEqualToString:@"1"])
	{
		elecmutawef.frame=CGRectMake(0, 10, 324, 342);
		mytext.hidden=YES;
		segments.hidden=YES;
		elecmutawef.hidden=NO;
		
	}
	else {
		elecmutawef.frame=CGRectMake(73, 5, 162, 171);
		elecmutawef.hidden=NO;
		mytext.hidden=NO;
		segments.hidden=NO;
	}

	
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [self setBgImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [bgImage release];
    [super dealloc];
	[elecmutawef release];
	[mytext release];
	[arrmanasek release];
	[arrdoaa release];
	[segments release];
	[lbltitle release];
	[sdic release];
	[from release];
}


@end
