//
//  SKDatabase.m
//
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SKDatabase.h"

@implementation SKDatabase

- (id)initWithFile:(NSString *)dbFile {
    
    self = [super init];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex: 0];
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:dbFile];
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if(!success) {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbFile];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
    if(sqlite3_open([dbPath UTF8String], &dbh) != SQLITE_OK)
    {
        sqlite3_open([dbPath UTF8String], &dbh);
    }
    return self;
}

- (void) copyDatabaseIfNeeded:(NSString *)dbFile{
    //Using NSFileManager we can perform many file system operations.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex: 0];
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:dbFile];
    
    BOOL success = [fileManager fileExistsAtPath:dbPath];
    
    if(!success) {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbFile];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

- (void)close {
    if (dbh) {
        sqlite3_close(dbh);
    }
}

- (sqlite3 *)dbh {
    
    return dbh;
}

- (sqlite3_stmt *)prepare:(NSString *)sql {
    
    const char *utfsql = [sql UTF8String];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare([self dbh],utfsql,-1,&statement,NULL) == SQLITE_OK) {
        return statement;
    } else {
        return 0;
    }
}

- (id)lookupSingularSQL:(NSString *)sql forType:(NSString *)rettype {
    
    sqlite3_stmt *statement;
    id result;
    
    if ((statement=[self prepare:sql])) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if ([rettype compare:@"text"] == NSOrderedSame) {
                result = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,0)];
            } else if ([rettype compare:@"integer"] == NSOrderedSame) {
                result = [NSNumber numberWithInt:sqlite3_column_int(statement,0)];
            }
        }
    }
    sqlite3_finalize(statement);
    return result;
    
}

- (int)lookupSingularINT:(NSString *)sql{
    @autoreleasepool {
        sqlite3_stmt *statement;
        int result;
        
        if ((statement=[self prepare:sql])) {
            if (sqlite3_step(statement) == SQLITE_ROW) {
                result = (int)sqlite3_column_int(statement,0);
            }
        }
        sqlite3_finalize(statement);
        return result;
    }
}

- (id)lookupSingularSQL:(NSString *)sql {
    
    sqlite3_stmt *statement;
    id result;
    
    if ((statement=[self prepare:sql])) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            
            char *txtChars = (char *)sqlite3_column_text(statement,0);
            if (txtChars==NULL) {
                result = @"";
            }
            else
            {
                
                result =[NSString stringWithUTF8String: txtChars];
            }
            
        }
    }
    sqlite3_finalize(statement);
    return result;
    
}
- (NSMutableArray *)lookupMultiSQL:(NSString *)sql forType:(NSString *)rettype{
    sqlite3_stmt *statement;
    NSMutableArray *result = [[NSMutableArray alloc]init];
    
    if ((statement=[self prepare:sql])) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            if ([rettype compare:@"text"] == NSOrderedSame) {
                [result addObject:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement,0)]];
            } else if ([rettype compare:@"integer"] == NSOrderedSame) {
                
                [result addObject:[NSNumber numberWithInt:sqlite3_column_int(statement,0)]];
            }
        }
    }
    sqlite3_finalize(statement);
    return result;
}

- (void) insertRecordIntoeTableNamed:(NSString *)sql{
    
    char *err;
    if (sqlite3_exec(self.dbh, [sql UTF8String], NULL, NULL, &err)!= SQLITE_OK) {
        sqlite3_close(self.dbh);
        NSAssert(0, @"Error updating table.");
    }
}

@end
