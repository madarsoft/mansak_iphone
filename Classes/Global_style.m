//
//  Global_style.m
//  mutnav2
//
//  Created by rashad on 7/6/14.
//
//

#import "Global_style.h"
#import "AppDelegate.h"
@implementation Global_style


#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE

+(UIView *)header_view:(UIView *)your_header view_frame:(CGRect )super_view_frame{
    your_header.backgroundColor=[UIColor clearColor];
    
    if (your_header.frame.size.height!=95)
    {
        your_header.frame =CGRectMake(0, 0, super_view_frame.size.width, 130);
        UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, your_header.frame.size.width, your_header.frame.size.height)];
        header_image.image=[UIImage imageNamed:@"inner screen header.png"];
        UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, your_header.frame.size.height-40, super_view_frame.size.width-80, 45)];
        [second_image setImage:[UIImage imageNamed:@"title.png"]];
        UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, your_header.frame.size.height-25, second_image.frame.size.width-40, 25)];
        first_title.backgroundColor=[UIColor clearColor];
        first_title.tag=5;
        first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
        
        first_title.textColor=[UIColor whiteColor];
        first_title.textAlignment=NSTextAlignmentCenter;
        
        UILabel *second_title=[[UILabel alloc] initWithFrame:CGRectMake((your_header.frame.size.width-120)/2,50,120, 25)];
        second_title.backgroundColor=[UIColor clearColor];
        second_title.tag=4;
      //  NSString *selectedLang= [[NSLocale preferredLanguages]objectAtIndex:0];
        second_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
       /* if ([selectedLang isEqualToString:@"ar"]) {
            second_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
        }else{
            second_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
        }*/
        
        second_title.textColor=[UIColor blackColor];
        second_title.textAlignment=NSTextAlignmentCenter;
        
        
        [your_header addSubview:second_image];
        [your_header addSubview:header_image];
        [your_header addSubview:first_title];
        [your_header addSubview:second_title];
        
    }
    else
    {
        
        your_header.frame =CGRectMake(0, 0, super_view_frame.size.width, 95);
        
        UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, your_header.frame.size.width, your_header.frame.size.height)];
        header_image.image=[UIImage imageNamed:@"inner screen header.png"];
        
        UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(50, your_header.frame.size.height-30, super_view_frame.size.width-100,30)];
        [second_image setImage:[UIImage imageNamed:@"title.png"]];
        [your_header addSubview:second_image];
        [your_header addSubview:header_image];
    }
    return your_header;
}
+(UIView *)footer_view:(UIView *)your_footer page_no:(int )page_no{
    your_footer.frame=CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-59, [[UIScreen mainScreen] bounds].size.width, 60);
    your_footer.backgroundColor=[UIColor clearColor];
    UIImageView *footer_back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, your_footer.frame.size.width, your_footer.frame.size.height)];
    [footer_back_img setImage:[UIImage imageNamed:@"footer_44.png"]];
    [your_footer addSubview:footer_back_img];
    if (page_no>1) {
        UIImageView *footer_left_arrow=[[UIImageView alloc] initWithFrame:CGRectMake(5, 24, 32,32)];
        footer_left_arrow.image=[UIImage imageNamed:@"footer_arrow_left.png"];
        UIImageView *footer_right_arrow=[[UIImageView alloc] initWithFrame:CGRectMake(your_footer.frame.size.width-37, 24, 32, 32)];
        footer_right_arrow.image=[UIImage imageNamed:@"footer_arrow_right.png"];
        
        [your_footer addSubview:footer_left_arrow];
        [your_footer addSubview:footer_right_arrow];
        
    }
    return your_footer;
}
-(void)startSampleProcess{
    
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self.delegate
                                   selector:@selector(processCompleted) userInfo:nil repeats:NO];
}
-(void)setArray:(NSMutableArray *)array array2:(NSMutableArray*)array_2 array3:(NSMutableArray*)array_3{
    array1=array;
    array2=array_2;
    array3=array_3;
}
-(void)global_animation:(int)index{
    index1=index;
    if(index1<[array1 count]){
        UIView *btn=[array1 objectAtIndex:index1];
        if ([[array2 objectAtIndex:index1] isEqual:@"scale"]) {
            btn.layer.affineTransform = CGAffineTransformMakeScale(0.0f, 0.0f);
            [self scale_animation:btn];
            
        }else if([[array2 objectAtIndex:index1] isEqual:@"move"]){
            NSString *check=[array3 objectAtIndex:index1];
            [self move_animation:btn direction:check];
        }
    }
}
-(void)scale_animation:(UIView *)view_scaled{
    view_scaled.hidden=NO;
    int delay=0;
    if (index1==0) {
        delay=0.99;
    }else{
        delay=0;
    }
    [UIView animateWithDuration:0.2 delay:delay  options:0 animations: ^{
        view_scaled.layer.affineTransform = CGAffineTransformMakeScale(1.2f, 1.2f);
        
        
    } completion: ^(BOOL completed) {
        [UIView animateWithDuration:0.2 delay:0  options:0 animations: ^{
            view_scaled.layer.affineTransform = CGAffineTransformMakeScale(1.0f,1.0f);
        } completion: ^(BOOL completed) {
            index1++;
            [self global_animation:index1];
        }];
        
    }];
}
-(void)move_animation:(UIView *)view_moved direction:(NSString *)dir{
    CGRect oldframe;
    view_moved.hidden=NO;
    if ([dir isEqual:@"right"]) {
        oldframe=view_moved.frame;
        view_moved.frame=CGRectMake(view_moved.frame.origin.x, view_moved.frame.origin.y, 0,view_moved.frame.size.height);
    }else if ([dir isEqual:@"down"]){
        oldframe=view_moved.frame;
        view_moved.frame=CGRectMake(view_moved.frame.origin.x, view_moved.frame.origin.y, view_moved.frame.size.width,0);
    }else if([dir isEqual:@"left"]){
        oldframe=view_moved.frame;
        view_moved.layer.anchorPoint=CGPointMake(1,0);
        view_moved.frame=CGRectMake( oldframe.origin.x,oldframe.origin.y, view_moved.frame.size.width, view_moved.frame.size.height);
        view_moved.bounds=CGRectMake(view_moved.frame.size.width, 0,0, view_moved.frame.size.height);
    }
    if ([dir isEqualToString:@"left"]) {
        [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
            view_moved.bounds=CGRectMake(0, 0, oldframe.size.width, oldframe.size.height);
            
            
        } completion: ^(BOOL completed) {
            index1++;
            [self global_animation:index1];
        }];
    }else{
        [UIView animateWithDuration:1 delay:0  options:0 animations: ^{
            view_moved.frame=oldframe;
            
            
        } completion: ^(BOOL completed) {
            index1++;
            [self global_animation:index1];
        }];
    }
    
}
+(void)custom_page:(UIPageControl *)pagecontrol{
    for (int i = 0; i < [pagecontrol.subviews count]; i++)
    {
        UIView* dot = [pagecontrol.subviews objectAtIndex:i];
        UIImageView *dot_image=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, dot.frame.size.width, dot.frame.size.height)];
        dot_image.image=[UIImage imageNamed:@"inactive_page_image.png"];
        dot.backgroundColor=[UIColor redColor];
        [dot addSubview:dot_image];
    }
}
+(UILabel *)global_text:(UILabel *)lbl_style{
    lbl_style.textColor= [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1];
    lbl_style.font = [UIFont fontWithName:@"Arial" size:text_size];
    NSString *labelText = @"some text";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    lbl_style.attributedText = attributedString ;
    
    return lbl_style;
    
}
@end
