//
//  HijriTime.h
//  AlMosaly
//
//  Created by Sara Ali on 2/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#ifdef __cplusplus

#import <UIKit/UIKit.h>
#import "date.h"


@interface HijriTime : UIView {
    
}

+ (NSMutableArray*) toHegry:(Date) melady1;
+ (NSMutableArray*) todayInHegry;
+ (NSMutableArray*) getMelady:(NSInteger) D month:(NSInteger) M year:(NSInteger) Y;
+ (NSString*) getArabicMonth:(NSInteger) index;
+ (NSString*) getGeogorianMonth:(NSInteger) index;
@end

#endif