//
//  TwitterViewController.h
//  mutnav2
//
//  Created by waleed on 5/26/13.
//
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>



@interface TwitterViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) NSArray *twitterAccounts;
@property (strong, nonatomic) NSMutableArray *tweets;
@property int cliKID;
- (void)fetchTweets;
- (void)retrieveAccounts;

- (IBAction)tweetTapped:(id)sender;
- (IBAction)btnClicked:(id)sender;
@end
