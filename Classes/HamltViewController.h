//
//  HamltViewController.h
//  mutnav2
//
//  Created by waleed on 6/9/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapViewAnnotation.h"
#define kMyTag 0

@interface HamltViewController : UIViewController<MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource>
@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UIImageView *btnBg;
@property (retain, nonatomic) IBOutlet UIView *leftView;
@property (retain, nonatomic) IBOutlet MKMapView *myMapView;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (retain, nonatomic) IBOutlet UILabel *titleLab;
@property (nonatomic, retain) NSMutableArray *dataArr ;
@property (nonatomic, retain) NSMutableArray *infoArr ;

-(UIView *)drawItemCell ;
-(NSMutableArray *)getData;

- (void) displayMap;
-(IBAction)btnClicked:(id)sender;
@end
