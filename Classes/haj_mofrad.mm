//
//  haj_mofrad.m
//  mutnav2
//
//  Created by rashad on 8/14/14.
//
//

#import "haj_mofrad.h"
#import "Global_style.h"
#import "CMSCoinView.h"
#import "Global_style.h"
//#import "Global.h"
#import "HijriTime.h"

#import "ArabicConverter.h"

@interface haj_mofrad ()<UINavigationControllerDelegate>

@end

@implementation haj_mofrad
@synthesize scrollView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
// This bool used to know if the iphone is "iphone4" or not to used it in display the view.
#define isiPhone4s  ([[UIScreen mainScreen] bounds].size.height == 480)?TRUE:FALSE

- (void)viewDidLoad
{
    [super viewDidLoad];
    /**
     *  get the views (headerView , title labels, footerView ) from xib and pass it to  "Global_style".
     */
    
    //get header view from xib and pass it to "Global_style" to draw & return it.
    _header_view=[Global_style header_view:_header_view view_frame:self.view.frame];
    //get main & second title
    _title_lable =(UILabel *)[_header_view viewWithTag:4];
    _second_title_lable=(UILabel *)[_header_view viewWithTag:5];
    _title_lable.text=NSLocalizedString(_title_lable_text,@"test");
     //get footer view from xib and pass it to "Global_style" to draw & return it.
    _footer_view.autoresizingMask= UIViewAutoresizingFlexibleTopMargin;
    _footer_view=[Global_style footer_view:_footer_view page_no:7];
    
    // get date of day and covert it to HIJRI date .
    Date melady_today;
    NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
    int hij_month=[[hijridate objectAtIndex:1] intValue];
    int hij_day=[[hijridate objectAtIndex:0] intValue];
    // check the day if the day in haj days period "_mansak_day"= currentDay else "_mansak_day" =0 (start from first day of haj).
    if (hij_month==12) {
        if (hij_day>=1&&hij_day<=7) {
            _mansak_day=0;
        }else if(hij_day==8){
            _mansak_day=1;
        }else if(hij_day==9){
            _mansak_day=2;
        }else if(hij_day==10){
            _mansak_day=3;
        }else if(hij_day>=11&&hij_day<=13){
            _mansak_day=4;
        }else if(hij_day>13){
            _mansak_day=5;
        }
    }else{
        _mansak_day=0;
    }
    // call initPageControl to add number of pages of this mansk and draw it on footerView.
    [self initPageControl];
}

/**
 *  draw custom PageControl with numberOfManasik and add it to footer.
 */
- (void)initPageControl
{
    // init custom_pagecontrol that different from normal in dot image
    _pageControl = [[custom_pagecontrol alloc] initWithFrame:CGRectMake(0,30, _footer_view.frame.size.width, 25)];
    _pageControl.userInteractionEnabled = NO;
    _pageControl.backgroundColor = [UIColor clearColor];
    //set number of points and set the current by selected mansk.
    _pageControl.numberOfPages = 6;
    [self.footer_view addSubview:_pageControl];
     // add _pageControl to footerView
    [_pageControl setCurrentPage:_mansak_day];
}

/**
 *  This method used to draw mainView "contentView" of selected hajj type (images ,labels ,title) , buttons and its action.
 * it work according current day.
 */
-(void)init_mansak_view{
    // get selected plist and put it into array of dictionaries "manasik_array"
    NSString *mypath=[[NSBundle mainBundle] pathForResource:_hajj_plist ofType:@"plist"];
    NSMutableArray *manasik_array=[[NSMutableArray alloc]initWithContentsOfFile:mypath];
    // "_mansak_data" dic hold all needed data about current day of manasik
    _mansak_data=[manasik_array objectAtIndex:_mansak_day];
    // get xib view of this manasik day and set its title.
    NSString *nib_file=[_mansak_data objectForKey:@"manasik_numbers"];
    _second_title_lable.text=NSLocalizedString([_mansak_data objectForKey:@"hajj_type"],@"test");
    
    // change size of "scrollView.contentSize" according number of masanik of this type of haj
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width *_pageControl.numberOfPages, self.scrollView.frame.size.height);
    
    // make "_content_view_4in" that hold all of views in xib and add it to the view with their valu of frame.
    _content_view_4in=[[UIView alloc]init];
    NSArray * allTheViewsInMyNIB = [[NSBundle mainBundle] loadNibNamed:nib_file owner:self options:nil];
    if (isiPhone4s) {
        _content_view_4in= allTheViewsInMyNIB[1];
    }else{
        _content_view_4in= allTheViewsInMyNIB[0];
    }
    
    // get images and array of array "arrOfArrlabelsText" of the view
    int mansak_num=0;
    NSMutableArray *imgs=[_mansak_data objectForKey:@"array_imgs"];
    NSMutableArray *arrOfArrlabelsText=[_mansak_data objectForKey:@"array_labels"];
    NSMutableArray *labelsTextArr;
    
    // change contentOffset of "scrollView" according number of days.
    float x=self.scrollView.frame.size.width * (int)_mansak_day;
    [scrollView setContentOffset:CGPointMake(x, 0)];
    // change frame of "content view" according number of days.
    _content_view_4in.frame=CGRectMake(x, 0, self.view.frame.size.width, self.view.frame.size.height);
    [scrollView addSubview:_content_view_4in];
    [self.view sendSubviewToBack:_content_view_4in];
    
    array=[[NSMutableArray alloc] init];
    array2=[[NSMutableArray alloc] init];
    array3=[[NSMutableArray alloc] init];
    
    // This iteration add views to arrays and add action on this views (according type arrow or coinView)  & add localization labels to views and pass those arrays to "Global_style" to do animation.
    for (UIView *subView in _content_view_4in.subviews) {
        subView.hidden=YES;
        
        // add allViews  to array1
        [array addObject:subView];
        if ([subView isKindOfClass:[CMSCoinView class]]) {
            // add type of view to array2 array3 (scale if the view is coinView).
            [array2 addObject:@"scale"];
            [array3 addObject:@"scale"];
            // get array of labels of manasik number from the biggestArray .
            labelsTextArr=arrOfArrlabelsText[mansak_num];
            // get current device language
            NSString *selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
            // this block of code added at localization period .. it loop on coinView and get labels from view & pass the view and selectedLanguage and text to "addLabelsToPics" method.
            for (UIView *coinView in subView.subviews ) {
                if (coinView.tag==110) {
                    [self addLabelsToPics:coinView :labelsTextArr[0] :selectedLanguage];
                }else if(coinView.tag==111){
                    [self addLabelsToPics:coinView :labelsTextArr[1] :selectedLanguage];
                }else if (coinView.tag==112){
                    [self addLabelsToPics:coinView :labelsTextArr[2] :selectedLanguage];
                }else if (coinView.tag==113){
                    [self addLabelsToPics:coinView :labelsTextArr[3] :selectedLanguage];
                }else if (coinView.tag==18){
                    // get the button from coinView and set its image and action.
                    UIButton *mansak_btn=(UIButton *)coinView;
                    [mansak_btn setImage:[UIImage imageNamed:[imgs objectAtIndex:mansak_num]] forState:UIControlStateNormal];
                    mansak_btn.imageView.contentMode = UIViewContentModeScaleAspectFit;
                    [mansak_btn addTarget:self action:@selector(first_mansak_click:) forControlEvents:UIControlEventTouchUpInside];
                }
            }
            mansak_num++;
        }else{
              // add type of view to array2  (move if the view is arrow).
            [array2 addObject:@"move"];
            if (subView.tag==1) {
                // add direction to arraw View.
                [array3 addObject:@"right"];
                
            }else if(subView.tag==2){
                [array3 addObject:@"down"];
                
            }else if(subView.tag==3){
                [array3 addObject:@"left"];
                
            }else if(subView.tag==4){
                [array3 addObject:@"up"];
                
            }
            
        }
    }
    
    //init "Global_style" class to used it in animation
    Global_style *global=[[Global_style alloc] init];
     // pass arrays to global class & call "global_animation" with index 0 to start animation from fisrt coinView
    [global setArray:array array2:array2 array3:array3];
    [global global_animation:0];
}

/**
 *  This method added at localization period .. it used to get label from view and add the gaven text to it.
 *
 *  @param targetView coinView that hold button and textLabel.
 *  @param text NSString that will appear in textLabel.
 *  @param selLang NSString var hold cuurent device language.
 */
-(void)addLabelsToPics :(UIView*)targetView :(NSString*)text :(NSString*)selLang{
    UILabel *label=(UILabel*)targetView;
    label.font=[label.font fontWithSize:12]; //arabic
    // check the current device language if it is english just add text to label .. BUT if it is arabic if you want to use custom font you must pass the text to "ArabicConverter" then use custom font .. or you can use the default font direct.
    ArabicConverter* c = [[ArabicConverter alloc] init];
    NSString* s = [c convertArabic:NSLocalizedString(text,@"test")];
    label.text = s;
    [label setTextAlignment:NSTextAlignmentCenter];
    [label sizeToFit];

    /*if ([selLang isEqualToString:@"ar"]) {
        label.font=[label.font fontWithSize:12]; //arabic
        // check the current device language if it is english just add text to label .. BUT if it is arabic if you want to use custom font you must pass the text to "ArabicConverter" then use custom font .. or you can use the default font direct.
        ArabicConverter* c = [[ArabicConverter alloc] init];
        NSString* s = [c convertArabic:NSLocalizedString(text,@"test")];
        label.text = s;
        [label setTextAlignment:NSTextAlignmentCenter];
        [label sizeToFit];
    }else{
      //  label.font=[UIFont fontWithName:@"Ventilla Stone" size:13];//english
          label.font=[UIFont systemFontOfSize:10];//english
        label.text=NSLocalizedString(text,@"test");
        [label setTextAlignment:NSTextAlignmentCenter];
        [label sizeToFit];
    }*/
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // detected the page user selected.
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if ((int)_mansak_day!=page && page<_pageControl.numberOfPages) {
        // assign page to day that user selected
        _mansak_day=page;
        // change current page of pageControl to selected page.
        self.pageControl.currentPage = page;
        // remove current view(_content_view_4in) from the view and call "init_mansak_view" to draw the view again according new values.
        [_content_view_4in removeFromSuperview];
        [self init_mansak_view];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    //call "init_mansak_view" for first time.
    [self init_mansak_view];
    // draw back btn
    UIButton *back_btn1=[[UIButton alloc] initWithFrame:CGRectMake(5, 25, 25, 25)];
    back_btn1.layer.cornerRadius=12.5;
    back_btn1.backgroundColor=[UIColor clearColor];
    [back_btn1 setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [back_btn1 addTarget:self action:@selector(backbtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.header_view addSubview:back_btn1];
}

/**
 *  This method get superView of button (coinView) & pass this view to "setUpView" method from "CMSCoinView" class to make coin animation to button
 *
 *  @param sender manasikButton
 */
- (IBAction)first_mansak_click:(id)sender {
    UIView *mansak_btn=sender;
    // get superView of button
    CMSCoinView *mansak_view=(CMSCoinView *)mansak_btn.superview;
      // call setUpView and pass the plist & index of mansk and index of day .
    [mansak_view setUpView:self plist_name:_hajj_plist mansak_id:mansak_view.tag mansak_day:_mansak_day];
    //check the current view if button flip to webPage view and change the flag.
    if (check==0) {
        [UIView animateWithDuration:0.5
                              delay:0.25
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             for (UIView *View_4inch in _content_view_4in.subviews) {
                                 if (View_4inch!=mansak_btn.superview) {
                                     View_4inch.alpha=0;
                                 }
                             }
                             self.header_view.alpha=0;
                             _footer_view.alpha=0;
                             scrollView.scrollEnabled=NO;
                         }
                         completion:NULL];
        // call "doTransitionWithType" to flip view
        [mansak_view doTransitionWithType:UIViewAnimationOptionTransitionFlipFromLeft header:self.header_view sender_frame:mansak_btn.superview.frame];
        // change flag
        check=1;
    }
}

/**
 *  This method get superView of back button (coinView) & pass this view to "close" method from "CMSCoinView" class to close view animated &remove webview.
 *
 *  @param sender backButton
 */
-(void)close_view:(id)sender{
    scrollView.scrollEnabled=YES;
    UIView *get_class=sender;
    // get superView of button
    CMSCoinView *mansak_view=(CMSCoinView *)get_class.superview.superview.superview;
    
    //call close method to this view from "CMSCoinView" class
    [mansak_view close];
    
     // hide webPage view animated
    [UIView animateWithDuration:0.5
                          delay:0.25
                        options:UIViewAnimationCurveEaseOut
                     animations:^(void) {
                         for (UIView *View_4inch in _content_view_4in.subviews) {
                             View_4inch.alpha=1;
                         }
                         _header_view.alpha=1;
                         _footer_view.alpha=1;
                         [[UIApplication sharedApplication] setStatusBarHidden:NO];
                         [mansak_view.web_content removeFromSuperview];
                         
                     }
                     completion:NULL];
    // change flag
    check=0;
}

/**
 *  This method made to handel back button pressed .. it remove (animated) the current view from super view and return to select hajjType view.
 *
 *  @param sender backBtn.
 */
- (IBAction)backbtn:(id)sender {
    [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
        self.view.frame=CGRectMake(-320,self.view.frame.origin.y,self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion: ^(BOOL completed) {
        [self.view removeFromSuperview];
    }];
}
@end
