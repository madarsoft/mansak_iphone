//
//  TwitterViewController.m
//  mutnav2
//
//  Created by waleed on 5/26/13.
//
//

#import "TwitterViewController.h"
//#import "TweetCell.h"
//#import "TwitterFeedCell.h"
#import <Social/Social.h>

@interface TwitterViewController ()

@end

@implementation TwitterViewController
@synthesize twitterAccounts,mainTable,tweets,cliKID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
 UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
 headImg.frame=CGRectMake(0, 0, 320, 50);
 [self.view addSubview:headImg];
    cliKID =0;
    UIImageView *mojBg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MojBg.png"]]autorelease];
    mojBg.frame=CGRectMake(0, 40, 320, 50);
    [self.view addSubview:mojBg];
    
  
      NSMutableArray *tit = [[NSMutableArray alloc]initWithObjects:@"ساعدوني",@"ذكرياتي",@"مواقف مؤثرة", nil];
    for (int i=0; i<3; i++) {
        UIButton *mojBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        mojBtn.tag=i;
        mojBtn.enabled=YES;
        mojBtn.backgroundColor = [UIColor clearColor];
      // [mojBtn setImage:[UIImage imageNamed:@"MojBg.png"] forState:UIControlStateNormal];
        mojBtn.frame = CGRectMake(10+(100*i), 40, 100,50);
     [mojBtn setTitle:[tit objectAtIndex:i] forState:UIControlStateNormal];
   //     [mojBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:mojBtn];
    }
    
 mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 90, 320, 390) style:UITableViewStylePlain];
 mainTable.delegate = self;
 mainTable.dataSource = self;
 mainTable.autoresizesSubviews = YES;
 mainTable.backgroundColor=[UIColor clearColor];
 mainTable.alpha = 1;
mainTable.rowHeight=80.0;
mainTable.separatorColor = [UIColor blackColor];
  mainTable.hidden = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
  
    
[self.view addSubview:mainTable];
    
    self.accountStore = [[ACAccountStore alloc] init];
    self.tweets = [[NSMutableArray alloc] initWithCapacity:50];
 //   [self retrieveAccounts];
    [self fetchTweets];
    
    
	// Do any additional setup after loading the view.
}
- (IBAction)btnClicked:(id)sender{
    UIButton *tmpBtn= (UIButton *)sender;
    int i = tmpBtn.tag;
    NSMutableArray *path = [[NSMutableArray alloc]initWithObjects:@"Moj01.png",@"Moj02.png",@"Moj03.png", nil];
    [tmpBtn setImage:[UIImage imageNamed:[path objectAtIndex:i]] forState:UIControlStateNormal ];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)retrieveAccounts{
    ACAccountType *accountType =
    [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [self.accountStore requestAccessToAccountsWithType:accountType options:nil
                                            completion:^(BOOL granted, NSError *error)
    {
        if (granted)
        {
            self.twitterAccounts = [self.accountStore accountsWithAccountType:accountType];
            dispatch_async(dispatch_get_main_queue(), ^(void)
            {
                [self.mainTable reloadData];
            });
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
  //  self.navigationItem.title = @"مجتمع الحج";
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addButton.frame = CGRectMake(270, 5, 50, 30);
    [addButton setImage:[UIImage imageNamed:@"addTweet.png"] forState:UIControlStateNormal];
    [addButton setShowsTouchWhenHighlighted:TRUE];
    [addButton addTarget:self action:@selector(tweetTapped:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:addButton];
 
}

- (IBAction)tweetTapped:(id)sender {
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Tweeting from my own app! :)"];
      /*  if (self.imageString)
        {
            [tweetSheet addImage:[UIImage imageNamed:self.imageString]];
        }
        
        if (self.urlString)
        {
            [tweetSheet addURL:[NSURL URLWithString:self.urlString]];
        }*/
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry"
                                  message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }
}



-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)fetchTweets
{
    
    [self.tweets removeAllObjects];
    SLRequest *request;
    
        // Get the public timeline of Twitter
        NSURL *requestURL =
        [NSURL URLWithString:@"http://api.twitter.com/1/statuses/public_timeline.json"];
        request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                     requestMethod:SLRequestMethodGET URL:requestURL parameters:nil];
    
    [request performRequestWithHandler:
     ^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
    {
        if ([urlResponse statusCode] == 200)
        {
            NSError *jsonParsingError;
            self.tweets = [NSJSONSerialization JSONObjectWithData:responseData
                                                          options:0 error:&jsonParsingError];
        }
        
        else
        {
            NSLog(@"HTTP response status: %i\n", [urlResponse statusCode]);
        }
        dispatch_async(dispatch_get_main_queue(), ^(void)
        {
            [self.mainTable reloadData];
        });
    }];
   
   
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
  //  return 1 + self.twitterAccounts.count;
      return [tweets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  /*  TwitterFeedCell *cell =
    [tableView dequeueReusableCellWithIdentifier:TwitterFeedCellId forIndexPath:indexPath];
    // Configure the cell...
    if (indexPath.row == 0)
    {
        // Public Feed
        cell.twitterAccount = nil;
    }
    else
    {
        cell.twitterAccount = [self.twitterAccounts objectAtIndex:indexPath.row - 1];
    }
    return cell;*/
    
  /* TweetCell *cell = [tableView dequeueReusableCellWithIdentifier:TweetCellId
                                                      forIndexPath:indexPath];
    // Configure the cell...
    cell.tweetata = [self.tweets objectAtIndex:indexPath.row];
    return cell;
    */
    static NSString *CellIdentifier = @"TweetCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *tweet = [tweets objectAtIndex:indexPath.row];
    NSString *text = [tweet objectForKey:@"text"];
    NSString *name = [[tweet objectForKey:@"user"] objectForKey:@"name"];
    
    cell.textLabel.text = text;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"by %@", name];
    
    return cell;

}


@end
