//
//  manasik.m
//  mutnav2
//
//  Created by rashad on 7/21/14.
//
//

#import "manasik.h"
#import "Global_style.h"
#import <QuartzCore/QuartzCore.h>
#import "ArabicConverter.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
@interface manasik ()

@end

@implementation manasik

@synthesize title_lable=_title_lable,header_view=_header_view,footer_view=_footer_view,adab_view=_adab_view,e7ram_view=_e7ram_view,tawaf_view=_tawaf_view,maqam_view=_maqam_view,zmzm_view=_zmzm_view,sa3i_view=_sa3i_view,left_arrow=_left_arrow,ta7ll_view=_ta7ll_view,down_arrow=_down_arrow,down_arrow_2=_down_arrow_2,up_arrow=_up_arrow,up_arrow_2=_up_arrow_2,content_view_3in=_content_view_3in,content_view_4in=_content_view_4in,down_arrow_3=_down_arrow_3;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#define isiPhone5  ([[UIScreen mainScreen] bounds].size.height == 568)?TRUE:FALSE
#pragma mark -dasdasdad

- (void)viewDidLoad
{
    [super viewDidLoad];
    /**
     *  get the views (headerView , title labels ) from "Global_style" & get footer view and manasikButtons from xib.
     */
    
    //  pass header view to "Global_style" to draw header view and return it.
    _header_view=[Global_style header_view:_header_view view_frame:self.view.frame];
    _title_lable =(UILabel *)[_header_view viewWithTag:4];
    _title_lable.text=  NSLocalizedString(@"manasikElOmara",@"test");
    _title_lable =(UILabel *)[_header_view viewWithTag:5];
    _title_lable.text=  NSLocalizedString(@"3omraMoswra",@"test");
    
    
    
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ Tatwef Screen/Iphone ", NSLocalizedString(@"manasikElOmara",@"test")]];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
  
    
    //call "init_content_view" to modify view.
    [self init_content_view];
    self.content_view_4in.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    [self first_animation];
    
    _footer_view.autoresizingMask= UIViewAutoresizingFlexibleTopMargin;
    // flag used to know what view showen now button or webPageView.
    check=0;
}

/**
 *  this method make coinView cornerRadius & loop on firstView and add content to secondView with new frame if is NOT IPHONE5
 */
-(void)init_content_view{
    for (UIView *subView in _content_view_4in.subviews) {
        subView.hidden=YES;
        if ([subView isKindOfClass:[CMSCoinView class]]) {
            subView.layer.cornerRadius=20;
        }
        
    }
    if (!isiPhone5)
    {
        for (UIView *View_4inch in _content_view_4in.subviews) {
            View_4inch.frame=[_content_view_3in viewWithTag:View_4inch.tag].frame;
            for (UIView *subView1 in View_4inch.subviews) {
                subView1.frame=CGRectMake(0, 0, View_4inch.frame.size.width, View_4inch.frame.size.height);
            }
        }
    }
}

/**
 *  This method added at localization period .. it used to get label from view and add the gaven text to it.
 *
 *  @param targetView coinView that hold button and textLabel.
 *  @param text NSString that will appear in textLabel.
 */
-(void)addLabelsToPics :(UIView*)targetView :(NSString*)text{
    // loop of coinnView to get the label that its tag = 110 in xib.
    for (UIView *myview in [targetView subviews]) {
        if (myview.tag==110) {
            UILabel *picLabel=(UILabel*)myview;
            picLabel.textColor=[UIColor blackColor];
            
            // check the current device language if it is english just add text to label .. BUT if it is arabic if you want to use custom font you must pass the text to "ArabicConverter" then use custom font .. or you can use the default font direct.
         //   NSString *selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
            picLabel.font=[picLabel.font fontWithSize:12]; //defult
            ArabicConverter* c = [[ArabicConverter alloc] init];
            NSString* s = [c convertArabic:text];
            picLabel.text = s;

        /*    if ([selectedLanguage isEqualToString:@"ar"]) {
                picLabel.font=[picLabel.font fontWithSize:12]; //defult
                ArabicConverter* c = [[ArabicConverter alloc] init];
                NSString* s = [c convertArabic:text];
                picLabel.text = s;
            }else{
                picLabel.font=[UIFont fontWithName:@"Ventilla Stone" size:12];//english
                picLabel.text=text;
            }*/
            [picLabel setTextAlignment:NSTextAlignmentCenter];
            [picLabel sizeToFit];
            
        }
    }
    
}

/**
 *  This method add views to arrays and add action on this views (according type arrow or coinView)  & add localization labels to views and pass those arrays to "Global_style" to do animation.
 */
-(void) first_animation{
    
    NSMutableArray *array=[[NSMutableArray alloc] init];
    
    // get coinView from xib and add it to array1
    [array addObject:_adab_view];
    
    // add localization label to coinview
    [self addLabelsToPics:_adab_view:NSLocalizedString(@"adabWTawgehatSlash",@"test")];
    
    // add arrow view to array1
    [array addObject:_left_arrow];
    
    // the same steps to the rest of views of xib.
    [array addObject:_e7ram_view];
    [self addLabelsToPics:_e7ram_view:NSLocalizedString(@"e7ramMnMikatSlash",@"test")];
    [array addObject:_down_arrow];
    [array addObject:_tawaf_view];
    [self addLabelsToPics:_tawaf_view:NSLocalizedString(@"tawaf",@"test")];
    [array addObject:_down_arrow_2];
    [array addObject:_maqam_view];
    [self addLabelsToPics:_maqam_view:NSLocalizedString(@"makamIbrahimSlash",@"test")];
    [array addObject:_up_arrow];
    [array addObject:_zmzm_view];
    [self addLabelsToPics:_zmzm_view:NSLocalizedString(@"zamzam",@"test")];
    [array addObject:_down_arrow_3];
    [array addObject:_sa3i_view];
    [self addLabelsToPics:_sa3i_view:NSLocalizedString(@"sa3i",@"test")];
    [array addObject:_up_arrow_2];
    [array addObject:_ta7ll_view];
    [self addLabelsToPics:_ta7ll_view:NSLocalizedString(@"el7alkAwElTakserSlash",@"test")];
    
    // add type of view to array2 (scale if the view is coinView) (move if the view is arrow)
    NSMutableArray *array2=[[NSMutableArray alloc] initWithObjects:@"scale",@"move",@"scale",@"move",@"scale",@"move",@"scale",@"move",@"scale",@"move",@"scale",@"move",@"scale", nil];
    
    // add direction to arraw View and scale to coinView
    NSMutableArray *array3=[[NSMutableArray alloc] initWithObjects:@"0.5",@"right",@"0.5",@"down",@"0.5",@"down",@"0.5",@"left",@"0.5",@"down",@"0.5",@"left", nil];
    
        //init "Global_style" class to used it in animation
    Global_style *global=[[Global_style alloc] init];
    // pass arrays to global class & call "global_animation" with index 0 to start animation from fisrt coinView
    [global setArray:array array2:array2 array3:array3];
    [global global_animation:0];
}

/**
 *  This method get superView of button (coinView) & pass this view to "setUpView" method from "CMSCoinView" class to make coin animation to button
 *
 *  @param sender manasikButton
 */
- (IBAction)first_mansak_click:(id)sender {
    
    UIView *mansak_btn=sender;
    
    // get superView of button
    CMSCoinView *mansak_view=(CMSCoinView *)mansak_btn.superview;
    
    // call setUpView and pass specific plist "omra_manasik" & index of mansk and index of day always 0 as omara is only one view.
    [mansak_view setUpView:self plist_name:@"omra_manasik" mansak_id:mansak_view.tag mansak_day:0];
    
    //check the current view if button flip to webPage view and change the flag.
    if (check==0) {
        [UIView animateWithDuration:0.5
                              delay:0.25
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             for (UIView *View_4inch in _content_view_4in.subviews) {
                                 if (View_4inch!=mansak_btn.superview) {
                                     View_4inch.alpha=0;
                                 }
                             }
                             self.header_view.alpha=0;
                             _footer_view.alpha=0;
                         }
                         completion:NULL];
        // call "doTransitionWithType" to flip view
        [mansak_view doTransitionWithType:UIViewAnimationOptionTransitionFlipFromLeft header:self.header_view sender_frame:mansak_btn.superview.frame];
        
        // change flag
        check=1;
    }else{
        // if the current view is webPage call "hide_header_view" to display the orignal button and view.
        [self hide_header_view];
    }
}

/**
 *  this method check the current state & hide or unhide according state.
 */
-(void)hide_header_view{
    if (_header_view.alpha==1 && check==1) {
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             _header_view.alpha=0;
                             _footer_view.alpha=0;
                         }
                         completion:NULL];
    }else{
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options:UIViewAnimationCurveEaseOut
                         animations:^(void) {
                             _header_view.alpha=1;
                             _footer_view.alpha=1;
                             
                         }
                         completion:NULL];
    }
}

/**
 *  This method get superView of back button (coinView) & pass this view to "close" method from "CMSCoinView" class to close view animated &remove webview.
 *
 *  @param sender backButton
 */
-(void)close_view:(id)sender{
   
    UIView *get_class=sender;
    
     // get superView of button
    CMSCoinView *mansak_view=(CMSCoinView *)get_class.superview.superview.superview;
    //call close method to this view from "CMSCoinView" class
    [mansak_view close];
    
    // hide webPage view animated
    [UIView animateWithDuration:0.5
                          delay:0.25
                        options:UIViewAnimationCurveEaseOut
                     animations:^(void) {
                         for (UIView *View_4inch in _content_view_4in.subviews) {
                             View_4inch.alpha=1;
                         }
                         _header_view.alpha=1;
                         _footer_view.alpha=1;
                         
                         [[UIApplication sharedApplication] setStatusBarHidden:NO];
                         [mansak_view.web_content removeFromSuperview];
                         
                     }
                     completion:NULL];
    // change flag
    check=0;
}


@end
