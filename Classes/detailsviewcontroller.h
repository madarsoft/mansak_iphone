//
//  detailsviewcontroller.h
//  mutnav2
//
//  Created by amr on 1/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface detailsviewcontroller : UIViewController {
	UITextView *txtdetails;
	NSMutableDictionary *sdic;
	NSNumber *myid;
	NSString *plist;
	UISegmentedControl *segments;
	
}
@property(nonatomic,retain)IBOutlet UITextView *txtdetails;
@property(nonatomic,retain)NSMutableDictionary *sdic;
@property (nonatomic, retain)NSNumber *myid;
@property (nonatomic, retain)NSString *plist;
@property(nonatomic,retain)IBOutlet UISegmentedControl *segments;

-(void)addfiletotextview:(NSString*)filename;
-(IBAction)changefile:(id) sender;

@end
