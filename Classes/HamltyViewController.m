//
//  HamltyViewController.m
//  mutnav2
//
//  Created by waleed on 6/9/13.
//
//

#import "HamltyViewController.h"

@interface HamltyViewController ()

@end

@implementation HamltyViewController
@synthesize bgImage,codeBtn,codeTxt,scrlView,keyboardVisible,_keyboardIsShowing;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    scrlView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 50,320, 330)];
    scrlView.delegate=self;
    scrlView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:scrlView];
    
    UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 40*highf)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=UITextAlignmentCenter;
    titleLabel.textColor = [UIColor greenColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"حملتي";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLabel];


  //  scrlView.contentSize = self.view.frame.size;
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hamlty.png"]];
    bgImage.frame = CGRectMake(80,0, 160,210*highf);
    [self.scrlView addSubview:bgImage];
    
    
    codeBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    codeBtn.frame = CGRectMake(20, 220, 70, 40);
    
    [codeBtn setImage:[UIImage imageNamed:@"codeBtn.png"] forState:UIControlStateNormal];
    [codeBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrlView addSubview:codeBtn];
    
    UIImageView *hamlImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"codeTxt.png"]];
    hamlImg.frame=CGRectMake(90, 220, 210, 40);
    [self.scrlView addSubview:hamlImg];
    
   
    codeTxt = [[UITextField alloc]initWithFrame:CGRectMake(95, 222, 200, 36)];
    codeTxt.backgroundColor = [UIColor clearColor];
    codeTxt.keyboardType=UIKeyboardTypeDefault;
    codeTxt.borderStyle=UITextBorderStyleRoundedRect;
    codeTxt.delegate = self;
    [self.scrlView addSubview:codeTxt];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    keyboardVisible = NO;
	// Do any additional setup after loading the view.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
 	[textField resignFirstResponder];
    return YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardDidShow:(NSNotification *)notif {
    if (keyboardVisible) {
        NSLog(@"%@",@"Keyboard is already visible. Ignoring notification.");
        return;
    }
    NSLog(@"Resizing smaller for keyboard");
    NSDictionary *info = [notif userInfo];
    NSValue *aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect viewFrame = self.view.bounds;
    viewFrame.size.height = keyboardTop - (self.view.bounds.origin.y);
    self.scrlView.frame = viewFrame;
    keyboardVisible = YES;
    
}
/// Custom for keyboard  ////
- (void)keyboardDidHide:(NSNotification *)notif {
    if (!keyboardVisible) {
        NSLog(@"%@",@"Keyboard already hidden. Ignoring notification.");
        return;
    }
    NSLog(@"%@",@"Resizing bigger with no keyboard");
    
    self.scrlView.frame = CGRectMake(0, 50,320, 330);
    keyboardVisible = NO;
}

-(IBAction)btnClicked:(id)sender{
    HamltViewController *detailViewController = [[HamltViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    [self.navigationController pushViewController:detailViewController animated:YES];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
