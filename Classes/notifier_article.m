//
//  notifier_article.m
//  queen
//
//  Created by hossam fathy on 9/1/14.
//
//

#import "notifier_article.h"

@implementation notifier_article

- (id)init;
{
    self = [super initWithFrame:CGRectMake(0, -68, 320, 68)];
    if (self) {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        self.backgroundColor=[UIColor greenColor];
        btntitle=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        btntitle.backgroundColor=[UIColor blackColor];
        [btntitle setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btntitle.titleLabel. numberOfLines = 0; // Dynamic number of lines
        [btntitle.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:btntitle];
        
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGSize screenSize = screenBound.size;
        CGFloat screenHeight = screenSize.height;
        mywebview=[[UIWebView alloc] init];
        mywebview.frame=CGRectMake(0, 0-screenHeight, 320, screenHeight-(140));
        mywebview.layer.zPosition = 99;
        [appDelegate.window addSubview:mywebview];
        
        
        webHeader=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
        webHeader.backgroundColor=[UIColor colorWithRed:(63/255.0) green:(63/255.0) blue:(63/255.0) alpha:1];
        webHeader.layer.zPosition = 100;
        [appDelegate.window addSubview:webHeader];
        UILabel*  webHeaderTitle =[[UILabel alloc] initWithFrame:CGRectMake(70, 4, 180, 27)];
        webHeaderTitle.textColor=[UIColor whiteColor];
        webHeaderTitle.backgroundColor=[UIColor clearColor];
        [webHeaderTitle setTextAlignment:NSTextAlignmentCenter];
        webHeaderTitle.text=@"أخبار المنسك";
        [webHeader addSubview:webHeaderTitle];
        webHeader.alpha=0;
        
        btnWebCloser = [UIButton buttonWithType:UIButtonTypeCustom];
        btnWebCloser.frame = CGRectMake(20, 0, 35, 35);
        [btnWebCloser setBackgroundImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [btnWebCloser addTarget:self  action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        btnWebCloser.layer.zPosition = 100;
        btnWebCloser.alpha=0;
        
        [appDelegate.window addSubview:btnWebCloser];
    }
    return self;
}
-(void)show :(NSString*)title id:(int)id{
    _title=title;
    _id=id;
    [btntitle setTitle:_title forState:UIControlStateNormal];
    [UIView beginAnimations:@"animateTableView" context:nil];
    [UIView setAnimationDuration:1];
    self.frame=CGRectMake(0, 0, 320, 48); //notice this is ON screen!
    [UIView commitAnimations];
    [self performSelector:@selector(hide) withObject:nil afterDelay:10];
}

-(void)hide{
    [UIView beginAnimations:@"animateTableView" context:nil];
    [UIView setAnimationDuration:1];
    self.frame=CGRectMake(0, -68, 320, 48); //notice this is ON screen!
    [UIView commitAnimations];
}
-(void)openview{
    [self hide];
    mywebview.scrollView.scrollEnabled = TRUE;
    mywebview.scalesPageToFit = TRUE;
    [UIView beginAnimations:@"animateTableView" context:nil];
    [UIView setAnimationDelay:1];
    [UIView setAnimationDuration:1];
    btnWebCloser.alpha=1;
    webHeader.alpha=1;
    [UIView commitAnimations];
    
    [UIView beginAnimations:@"animateTableView" context:nil];
    [UIView setAnimationDuration:1];
    mywebview.frame=CGRectMake(0, 95, mywebview.frame.size.width, mywebview.frame.size.height);//notice this is ON screen!
    [UIView commitAnimations];
}
-(void) drawWebClosingButton{
    
}
-(void)closeWebView{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenHeight = screenSize.height;
    [UIView beginAnimations:@"animateTableView" context:nil];
    [UIView setAnimationDuration:0.5];
    mywebview.frame=CGRectMake(0, 0-screenHeight, 320, screenHeight);//notice this is ON screen!
    
    [UIView commitAnimations];
    btnWebCloser.alpha=0;
    webHeader.alpha=0;
}


@end
