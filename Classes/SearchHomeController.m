//
//  SearchHomeController.m
//  itour
//
//  Created by walid nour on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchHomeController.h"


@implementation SearchHomeController

@synthesize bgImage, mainTable;
@synthesize mainarr, filearr ,iddarr;
@synthesize townID, listType, buildID;
@synthesize titleName ,fileName;
@synthesize searchID;
@synthesize sv;
@synthesize checkID;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    mainarr = [[NSMutableArray alloc] init];
    iddarr  = [[NSMutableArray alloc] init];
    filearr = [[NSMutableArray alloc] init];
    
    [self fillArr];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.hidesBackButton = TRUE;
    self.navigationItem.leftBarButtonItem = barBackItem;
    [barBackItem release];
 
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,370);
 
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0.0, 320,370) style:UITableViewStyleGrouped];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=45.0;
    mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
 //   [mainTable setBackgroundView:bgImage];
  [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    self.title = titleName;
    
}
-(void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidUnload
{
    [self setMainarr:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mainarr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    //cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    // Configure the cell...
	   
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
        [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    
    [cell.contentView addSubview:bgView];
    [cell setBackgroundView:bgView];
    cell.backgroundColor = [UIColor clearColor];
    [bgView release];
    
    

    if (indexPath.row==checkID) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
        // Configure the cell...
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
        cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        cell.textLabel.numberOfLines = 0;

    return cell;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for(int iterator=0;iterator<mainarr.count;iterator++){
        UITableViewCell *eachCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:iterator inSection:0]];
        [eachCell setSelected:NO animated:YES];
        [eachCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    [newCell setSelected:YES animated:YES];
    [newCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    if (listType==1) {
        sv.buildID = indexPath.row;
        buildName =[mainarr objectAtIndex:indexPath.row];
    }
    else if (listType==2) {
         sv.searchID = indexPath.row;
        typeName  =[mainarr objectAtIndex:indexPath.row];
    }
   
    
}


- (void) fillArr
{
    if(listType==1)
    {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"الفنادق",@"شقق", nil];
        filearr = [[NSMutableArray alloc] initWithObjects: @"hotel",@"house", nil];
    }
    else if(listType==2)
    {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"ابحث باسم المنشأة",@"بحث بشارع المنشأة", nil];
        
    }
  /*  else
    {
        NSString *pathRoot = [[NSString alloc] initWithFormat:@"lists"];
        fileName = pathRoot;
        NSString *path=[[NSBundle mainBundle] pathForResource:pathRoot ofType:@"txt"];
        NSError *error;
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        NSArray *allLines = [content componentsSeparatedByString: @"\n"];
        int count = [allLines count];
        for (int i=0; i<count; i++) {
            NSString *line = [allLines objectAtIndex:i];
            NSArray *items = [line componentsSeparatedByString: @"|"];
            if ([[items objectAtIndex:0] intValue] ==townID ) {
                [self.mainarr addObject:[items objectAtIndex:1]];
                [self.iddarr addObject:[items objectAtIndex:2]];
                [self.filearr addObject:[items objectAtIndex:3]];
            }
        }
    }*/
}

@end
