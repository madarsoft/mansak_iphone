#ifndef DATE_H
#define DATE_H

#ifdef __cplusplus

class Date
{
public:
    //constucts a Date object for now
    Date();
    Date(const Date& date1);
    ~Date();
    //constucts a Date object from a NSDate object
    Date(NSDate* date);
    Date ( int d, int m, int y );
    Date& operator=(const Date& d);
    bool operator==(const Date& d1 );
    bool isEqualDay(Date d);
    
    int dayOfWeek();
    int day();
    int month();
    int year();
    int hour();
    int minute();
    int second();
    NSString* dayOfWeekString();
    
    double getClockDouble();
    
    void setDay(int day);
    void setMonth(int month);
    void setYear(int year);
    void setHour(int hour);
    void setMinute(int mnt);
    void setSecond(int second);
    void setClock(double clock);
    void setWeekDay(int weekDay);
    Date addDays(int days);
    
    static double getClock(); // Now clock
    static NSString* getClockString(int interval);
    static int timeZone();
    
    NSDate* date;
    NSDateComponents* components;
    NSCalendar* cal;
    
    static const int SATURDAY = 0;
    static const int SUNDAY = 1;
    static const int MONDAY = 2;
    static const int TUESDAY = 3;
    static const int WEDNESDAY = 4;
    static const int THURSDAY = 5;
    static const int FRIDAY = 6;
    
private:
    void initializeDate();
    
};


#endif

#endif // DATE_H
