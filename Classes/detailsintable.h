//
//  detailsintable.h
//  mutnav2
//
//  Created by amr on 1/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "arabic.m"

@interface detailsintable : UITableViewController {
	NSMutableDictionary *sdic;
	NSMutableDictionary *arabicdic;
	NSNumber *myid;
	NSString *plist;
}
@property(nonatomic,retain)NSMutableDictionary *sdic;
@property(nonatomic,retain)NSMutableDictionary *arabicdic;
@property (nonatomic, retain)NSNumber *myid;
@property (nonatomic, retain)NSString *plist;

-(IBAction)buttonContact:(id)sender;

@end
