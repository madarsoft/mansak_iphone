//
//  GridView.h
//  Almosaly
//
//  Created by Sara Ali on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalCellView.h"
#import "TextCellView.h"

@interface GridView : UIView {
    NSInteger numRows;
    NSInteger numcellsPerRow;
    
    UIScrollView* gridView;
    UIImage* backgroundImage;
    NSArray* viewsArray;
}
-(void)drawGrid:(NSInteger)Rows col:(NSInteger)Column;
@property (nonatomic) NSInteger numRows;
@property (nonatomic) NSInteger numcellsPerRow;

@property (nonatomic) NSInteger visibleNumRows;
@property (nonatomic,strong) UIScrollView* gridView;

@property (nonatomic,strong) UIImage* backgroundImage;
@property (nonatomic,strong) NSArray* viewsArray;
@property BOOL leftToRight;
@property BOOL scrollRows;
@end
