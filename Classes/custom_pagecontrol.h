//
//  custom_pagecontrol.h
//  mutnav2
//
//  Created by mohamed on 8/16/14.
//
//

#import <UIKit/UIKit.h>

@interface custom_pagecontrol : UIPageControl
@property (strong, nonatomic) UIImage *normalImage;
@property (strong, nonatomic) UIImage *highlightedImage;

@end
