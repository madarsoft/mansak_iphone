//
//  HamalSerachViewController.m
//  mutnav2
//
//  Created by waleed on 6/2/13.
//
//

#import "HamalSerachViewController.h"
#import "NIDropDown.h"
#import "QuartzCore/QuartzCore.h"
@interface HamalSerachViewController ()

@end

@implementation HamalSerachViewController
@synthesize bgImage,stateBtn,searchBtn,townBtn,offerBtn,dropDown,type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,420*highf);
    [self.view addSubview:bgImage];
    
    UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 40*highf)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor greenColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"دليل الحملات";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLabel];
    
    NSMutableArray *imgArr = [[NSMutableArray alloc]initWithObjects:@"haml_01.png",@"haml_02.png",@"haml_03.png", nil];
    
    for (int i=0; i<3; i++) {
        UIImageView *hamlImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"haml.png"]];
        hamlImg.frame=CGRectMake(20, 50+(i*60), 28, 38*highf);
        [self.view addSubview:hamlImg];
        
        UIImageView *hamlbgImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"hamlBg.png"]];
        hamlbgImg.frame=CGRectMake(48, 50+(i*60), 252, 38*highf);
        [self.view addSubview:hamlbgImg];
        
        UIImageView *haml =[[UIImageView alloc] initWithImage:[UIImage imageNamed:[imgArr objectAtIndex:i]]];
        haml.frame=CGRectMake(260, 56+(i*60), 36, 28*highf);
       [self.view addSubview:haml];
    }
    
    stateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    stateBtn.backgroundColor = [UIColor clearColor];
    [stateBtn setTitle:@"اختر الدولة" forState:UIControlStateNormal];
    [stateBtn setTitleColor:[UIColor colorWithRed:0.733 green:0.859 blue:0.349 alpha:1.0] forState:UIControlStateNormal];
    stateBtn.frame = CGRectMake(55, 56, 180, 28*highf);
    stateBtn.layer.borderWidth = 1;
    stateBtn.layer.borderColor = [[UIColor blackColor] CGColor];
    stateBtn.layer.cornerRadius = 5;

    [stateBtn addTarget:self action:@selector(selectClicked:) forControlEvents:UIControlEventTouchUpInside];
    stateBtn.tag=1;
    [self.view addSubview:stateBtn];
    
    townBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    townBtn.backgroundColor = [UIColor clearColor];
   [townBtn setTitle:@"اختر المدينة" forState:UIControlStateNormal];
    [townBtn setTitleColor:[UIColor colorWithRed:0.733 green:0.859 blue:0.349 alpha:1.0] forState:UIControlStateNormal];
    townBtn.frame = CGRectMake(55, 116, 180, 28*highf);
    townBtn.layer.borderWidth = 1;
    townBtn.layer.borderColor = [[UIColor blackColor] CGColor];
    townBtn.layer.cornerRadius = 5;
    [townBtn addTarget:self action:@selector(selectClicked:) forControlEvents:UIControlEventTouchUpInside];
    townBtn.tag=2;
    [self.view addSubview:townBtn];
    
    offerBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    offerBtn.backgroundColor = [UIColor clearColor];
    [offerBtn setTitle:@"عرض خاص بـ"forState:UIControlStateNormal];
    [offerBtn setTitleColor:[UIColor colorWithRed:0.733 green:0.859 blue:0.349 alpha:1.0] forState:UIControlStateNormal];
    offerBtn.layer.borderWidth = 1;
    offerBtn.layer.borderColor = [[UIColor blackColor] CGColor];
    offerBtn.layer.cornerRadius = 5;
    [offerBtn addTarget:self action:@selector(selectClicked:) forControlEvents:UIControlEventTouchUpInside];
    offerBtn.tag=3;
    offerBtn.frame = CGRectMake(55, 176, 180, 28*highf);
     [self.view addSubview:offerBtn];
    
    
    searchBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.backgroundColor = [UIColor clearColor];
    [searchBtn setImage:[UIImage imageNamed:@"hamlSear"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(goToPage:) forControlEvents:UIControlEventTouchUpInside];
    searchBtn.frame = CGRectMake(20, 230, 280, 35*highf);
    [self.view addSubview:searchBtn];

    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)selectClicked:(id)sender {
       NSArray * arr = [[NSArray alloc] init];
    CGFloat f;
    if ([sender tag]==1) {
        type =1;
        f=260;
      arr = [NSArray arrayWithObjects:@"السعودية", @"الكويت", @"الإمارات", @"البحرين", @"قطر", @"همان", @"الأردن", @"مصر", nil];
    }
    else if([sender tag]==2){
        type =2;
        arr = [NSArray arrayWithObjects:@"الرياض", @"الخبر", @"جدة", @"الأحساء", @"الطائف", @"حفر الباطن", nil];
        f=220;
    }
    else if([sender tag]==3){
        type=3;
        arr = [NSArray arrayWithObjects:@"السعر", @"الفئى", @"المسافة",  nil];
        f=120;
    }
        
   
  
    if(dropDown == nil) {
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    
    [self rel];
}

- (IBAction)goToPage:(id)sender{
    HamalListViewController *detailViewController = [[HamalListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    
}

-(void)rel{
    NSLog(@"%@",dropDown.selectText);
    if (type==1) {
        state = dropDown.selectText;
    }
    else if (type==2){
        city=dropDown.selectText;
     }
    else if (type==3){
        offer=dropDown.selectText;
      }
        
    //    [dropDown release];
    dropDown = nil;
}
@end
