//
//  EMutawefViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 2/19/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
//#import <BugSense-iOS/BugSenseController.h>
@interface EMutawefViewController : UIViewController
<UITextViewDelegate ,UIGestureRecognizerDelegate >
@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property(nonatomic,retain) IBOutlet UIImageView *elecmutawef;
@property(nonatomic,retain) IBOutlet UITextView *mytext;
@property(nonatomic,retain) IBOutlet NSMutableArray *arrdoaa;
@property(nonatomic,retain) IBOutlet NSMutableArray *arrmanasek;
@property(nonatomic,retain) IBOutlet UISegmentedControl *segments;
@property(nonatomic,retain) IBOutlet UILabel *lbltitle;
@property(nonatomic,retain) NSMutableDictionary *sdic;
@property(nonatomic,retain) NSString *from;
@property(nonatomic,retain) IBOutlet UIButton *leftButton;
@property(nonatomic,retain) IBOutlet UIButton *rightButton;


@property(nonatomic,retain) IBOutlet UIButton *douBtn;
@property(nonatomic,retain) IBOutlet UIButton *manBtn;
@property(nonatomic,retain) IBOutlet UIButton *pushBtn;
@property int imgindex;
-(IBAction) next_tawaf:(id)sender;
-(IBAction) previous_tawaf:(id)sender;
-(void)addfiletotextview:(NSString*)filename;
-(IBAction)changefile:(id) sender;


-(void)handleGesture:(UIPanGestureRecognizer *)gestureRecognizer;

@end
