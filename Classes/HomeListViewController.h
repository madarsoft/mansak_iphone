//
//  HomeListViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 2/15/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "BuildingViewController.h"
//#import <BugSense-iOS/BugSenseController.h>

@interface HomeListViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;


@property (nonatomic, retain) NSString *grade;
@property (nonatomic, retain) NSString *type;

@property (nonatomic, retain) NSMutableArray *mainarr;
@property (retain, nonatomic) NSMutableArray *idarr;

-(void) getHomeList;

@end
