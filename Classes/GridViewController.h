//
//  GridViewController.h
//  Almosaly
//
//  Created by Sara Ali on 2/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GridView.h"
#import <QuartzCore/QuartzCore.h>

@interface GridViewController : UIViewController <CalCellViewDelegate , UITableViewDataSource , UITableViewDelegate,UIAlertViewDelegate> {
    
    UILabel* meladyDateLabel;
    UILabel* hegryDateLabel;
    UILabel* meladyMonthLabel;
    UILabel* hegryMonthLabel;
    UILabel* meladyYearLabel;
    UILabel* hegryYearLabel;
    GridView* calGrid;
    GridView* daysGrid;
    UIBarButtonItem *correctButtonItem;
    NSArray* dayNames;
    NSMutableArray* difBtns;
    NSMutableArray* difLabels;
    
    
    NSInteger offset;
    NSInteger month_number;
    NSInteger year_number;
    NSInteger meladymonth_number;
    NSInteger meladyyear_number;
}

@property (nonatomic, retain)  IBOutlet UIPickerView* correctionPicker;
@property (retain) IBOutlet UIView* containerView;
@property BOOL mode;

@property (retain) NSArray* events;
@property (retain) NSArray* currentEvents;
@property (retain) UITableView* tableView;

-(IBAction) okButtonPressed:(id)sender;
-(IBAction) difButtonPressed:(id)sender;
-(IBAction) resetButtonPressed:(id)sender;
-(void) correctButtonPressed:(id) sender;
-(void)forwardButtonPressed ;
-(void)backwardButtonPressed ;
-(void) updateView;
@property int timeDif;
@property (retain, nonatomic) IBOutlet UIView *EntryModalView;
@property (retain, nonatomic) IBOutlet UIDatePicker *timePicker;
@property (retain, nonatomic) IBOutlet UILabel *meladyEntryViewLabel;
@property (retain, nonatomic) IBOutlet UILabel *HijriEntryViewLable;
@property (retain, nonatomic) IBOutlet UITextField *entryNameTextField;
@property (retain, nonatomic) IBOutlet UIViewController *entryModalViewController;
@property (retain, nonatomic) IBOutlet UIView *header_view;
@property (retain, nonatomic) IBOutlet UIView *footer_view;
@property (retain) CalCellView* selectedCell;

@property (retain, nonatomic) IBOutlet UIView *correctView;
@property (retain, nonatomic) IBOutlet UISlider *correctSlider;

- (IBAction)correctBackPressed:(id)sender;
- (IBAction)correctSavePressed:(id)sender;
- (IBAction)correctSliderValueChanged:(id)sender;

- (IBAction)checkMarkAction:(id)sender;
- (IBAction)deleteAction:(id)sender;
@property int firstDayflag;
@end

