//
//  HamaltViewController.h
//  mutnav2
//
//  Created by waleed on 3/17/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "SearchListController.h"
@class RXMLElement;
@class Reachability;

@interface HamaltViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (retain, nonatomic) IBOutlet UIButton *searchBtn;

@property (nonatomic ,retain) NSMutableArray *mainarr;
@property (nonatomic ,retain) NSMutableArray *filearr;
@property (nonatomic ,retain) NSMutableArray *searcharr;
@property (nonatomic ,retain) NSMutableArray *buildarr;
@property (nonatomic ,retain) NSMutableArray *iddarr;

@property (nonatomic ,retain) NSString *titleName;
@property (nonatomic ,retain) NSString *fileName;

@property (nonatomic ,retain) NSString *searchTxt;

@property NSInteger listType;
@property NSInteger townID;
@property NSInteger buildID;
@property NSInteger searchID;


-(void) fillArr;
-(IBAction)searchPressed:(id)sender;


@end
