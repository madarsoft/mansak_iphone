#ifndef MYMATH_H  // first time include
#define MYMATH_H
#ifdef __cplusplus

#include <math.h>

class MyMath
{
public:
    MyMath();
    
    static double const RTD ;
    static double const DTR ;
    /*
     it was static double const RTD = 180 / M_PI
     but i c++  it warining to make it variable in .h then init it in .m
     */
    
    static double fixAngle(double a)
    {
        a = a - 360.0 * (floor(a / 360.0));
        a = a < 0 ? a + 360.0 : a;
        return a;
    }
    
    static double fixHours(double a)
    {
        a = a - 24.0 * (floor(a / 24.0));
        a = a < 0 ? a + 24.0 : a;
        return a;
    }
    
    
    //degree Sin
    static double dSin(double DegreeAngle)
    {
        return sin(DegreeAngle * DTR);
    }
    
    //degree Cos
    static double dCos(double DegreeAngle)
    {
        return cos(DegreeAngle * DTR);
    }
    
    // degree Tan
    static double dTan(double DegreeAngle)
    {
        return tan(DegreeAngle * DTR);
    }
    
    static double dASin(double x){
        
        return asin(x) * RTD;
    }
    
    // degree arccos
    static double dACos(double x)
    {
        
        return acos(x) * RTD;
    }
    
    
    static double dATan(double x)
    {
        return atan(x) * RTD;
        
    }
    
    // degree arctan2
    static double dATan2(double y, double x)
    {
        return atan2(y , x) * RTD;
    }
    
    // degree cot
    static double cotan(double i) { return(1 / tan(i * DTR)); }
    
    // degree arccot
    static double dACot(double x) { return 90-dATan(x);}
    
    static double roundTime(double time)
    {
        int hours = (int)time;
        double minuts = (time - hours)*60;
        int min = round(minuts);
        
        double result = hours + min/60.0;
        
        return result;
    }
    
    static int round(double input)
    {
        int integer = (int)input;
        double fraction = input - integer;
        
        if(fraction >= 0.5)
            integer++;
        
        return integer;
        
    }
    
    
    //normalize value to be in range 0....1
    static double normalize(double x)
    {
        x = x - (int)x;
        return x < 0 ? x + 1 : x;
    }
    
    static CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;}
};
#endif
#endif // MYMATH_H
