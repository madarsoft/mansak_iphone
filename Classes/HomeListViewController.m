//
//  HomeListViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 2/15/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//

#import "HomeListViewController.h"

@implementation HomeListViewController

@synthesize bgImage, mainTable;
@synthesize type,grade;
@synthesize mainarr, idarr;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    mainarr = [[NSMutableArray alloc] init];
    idarr = [[NSMutableArray alloc] init];
    

    
    
    [self getHomeList];
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,432*highf);
    
    
    mainTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 5.0*highf, 320, 375*highf) style:UITableViewStylePlain]autorelease];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1.0;
    mainTable.rowHeight=50.0*highf;
    //mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
 //   mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
   [self.view addSubview:bgImage];
   [self.view addSubview:mainTable];
    
    //self.title = @"الدليل";
    
  
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text =  @"الدليل";
    [titleView sizeToFit];
    

    // Do any additional setup after loading the view from its nib.
}


- (void)viewDidUnload
{
    [self setBgImage:nil];
    [self setMainTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        self.navigationController.navigationBar.tintColor = [UIColor greenColor];
        [backButton release];
        [barBackItem release];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [mainarr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
     
cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
       [bgView setImage:[UIImage imageNamed:@"i2.png"]]; 
    }
    
    [cell.contentView addSubview:bgView];
    [cell setBackgroundView:bgView];
    cell.backgroundColor = [UIColor clearColor];
    [bgView release];
    
    
	//cell.textLabel.text=[dic objectForKey:@"name"];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
   cell.textLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
    // cell.selectionStyle = UITableViewCellSelectionStyleGray;
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    lbl.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
    lbl.text = [NSString stringWithFormat:@"%i",indexPath.row +1];
    lbl.backgroundColor = [UIColor clearColor];
    cell.accessoryView = lbl;
    [lbl release];
  /*  UIImageView *bggView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    [bggView setImage:[UIImage imageNamed:@"cellbgg.png"]];
    [cell setSelectedBackgroundView:bggView];
    [bggView release];*/
       return cell;
    
    
}


-(void) getHomeList{
    if ([self.type isEqualToString:@"house"]) {
        self.type = @"2";
    }
    else if ([self.type isEqualToString:@"hotel"]) {
        self.type = @"1";
    }
    NSString *path=[[NSBundle mainBundle] pathForResource:@"buildings" ofType:@"dat"];
    NSError *error;
    
    NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    
    
    NSArray *allLines = [content componentsSeparatedByString: @"\n"];
    int count = [allLines count];
    NSLog(@"%d", count);
    
    for (int i=0; i<count; i++) {
        NSString *line = [allLines objectAtIndex:i];
        
        NSArray *items = [line componentsSeparatedByString: @"|"];
        NSString *typ = [items objectAtIndex:4];
        NSString *grad =  [items objectAtIndex:5];
        
        if ([type isEqualToString:typ] &&[grade isEqualToString:grad] ) {
            NSLog(@"%@",typ);
            NSLog(@"%@", grad);
            NSLog(@"%@", [items objectAtIndex:3]);
            [self.mainarr addObject:[items objectAtIndex:3]];
            [self.idarr addObject:[items objectAtIndex:0]];
        
            
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    //[bgView setImage:[UIImage imageNamed:@"i3.png"]];
    //[[tableView cellForRowAtIndexPath:indexPath] setBackgroundView:bgView];
    
    BuildingViewController *detailViewController = [[BuildingViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    NSLog(@"%@",[idarr objectAtIndex:indexPath.row] );
    detailViewController.myId = [idarr objectAtIndex:indexPath.row];
    detailViewController.myName = [mainarr objectAtIndex:indexPath.row];
    detailViewController.grade = self.grade;
    
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *currentSelectedIndexPath = [tableView indexPathForSelectedRow];
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
        [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    if (currentSelectedIndexPath != nil)
    {
        [[tableView cellForRowAtIndexPath:currentSelectedIndexPath] setBackgroundView:bgView];
    }
    
    return indexPath;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    if (cell.isSelected == YES)
    {
        //[bgView setImage:[UIImage imageNamed:@"i3.png"]];
        
        //[cell setBackgroundView:bgView];
        
    }
    else
    {
        
        if (indexPath.row%2==0) {
            [bgView setImage:[UIImage imageNamed:@"i1.png"]];
        }
        else{
            [bgView setImage:[UIImage imageNamed:@"i2.png"]];
        }
        [cell setBackgroundView:bgView];
    }
}



-(void) dealloc{
    [mainarr release];
    [idarr release];
    
    [super dealloc];
}

@end
