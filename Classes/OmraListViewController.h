//
//  OmraListViewController.h
//  mutnav2
//
//  Created by walid nour on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
//#import <BugSense-iOS/BugSenseController.h>
@interface OmraListViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;

@property (nonatomic, retain) NSNumber *myid;
@property (nonatomic, retain) NSString *plist;
@property (nonatomic, retain) NSMutableArray *menuearr;
@property (nonatomic, retain) NSMutableDictionary *sdic;
@property (nonatomic, retain) NSMutableArray *mainarr;

//@property (nonatomic, retain) IBOutlet AdsViewController *adsView;

//-(void)stopAds;

@end
