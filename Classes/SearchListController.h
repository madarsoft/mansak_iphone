//
//  SearchListController.h
//  itour
//
//  Created by walid nour on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "HamaltViewController.h"
@class HamaltViewController;

@interface SearchListController : UIViewController

<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;


@property (nonatomic ,retain) NSMutableArray *mainarr;
@property (nonatomic ,retain) NSMutableArray *filearr;
@property (nonatomic ,retain) NSMutableArray *iddarr;

@property (nonatomic ,retain) NSString *titleName;
@property (nonatomic ,retain) NSString *fileName;

@property (nonatomic, retain) HamaltViewController *sv;

@property NSInteger listType;
@property NSInteger townID;
@property NSInteger buildID;
@property NSInteger searchID;
@property NSInteger checkID;


-(void) fillArr;


@end



