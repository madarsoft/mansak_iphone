//
//  NewMainViewController.h
//  mutnav2
//
//  Created by waleed on 3/31/13.
//
//


#import <UIKit/UIKit.h>
#import "prayertimecalculator.h"
#import "date.h"
#import <sqlite3.h>
#import "SKMenu.h"
#import "HijriTime.h"
#import "GetHijriDate.h"
//#import <CoreLocation/CoreLocation.h>

#define kMyTag 0
@class RXMLElement;
@class Reachability;

@interface NewMainViewController : UIViewController<UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,UINavigationControllerDelegate/*,CLLocationManagerDelegate*/>{
    NSThread *save_data_thread;
    NSString *last_time;
    NSTimer *refresh_btn_timer;
    int btn_refresh_clicked;
}
@property (nonatomic,retain) UIView *news_view;
@property (retain, nonatomic) IBOutlet UIWebView *myWeb;
@property (nonatomic,retain) UIImageView *botimg;
@property (nonatomic,retain)  NSArray *prayerTimes;
@property (nonatomic,retain)  NSArray *prayTimes;
@property (retain, nonatomic) IBOutlet UIImageView *backImage;
@property (retain, nonatomic) NSMutableArray *tawef;
@property (retain, nonatomic) NSMutableArray *saai;
@property (retain, nonatomic) NSMutableArray *crowd;
@property (retain, nonatomic) UILabel *refresh_lbl;
@property (retain, nonatomic) UIImageView *right_image;
@property (retain, nonatomic) UIImageView *first_image;
@property (nonatomic,retain) IBOutlet UIView *floorView;
@property (retain, nonatomic) IBOutlet UILabel *citylabel;
@property (retain, nonatomic) IBOutlet UILabel *timeLabel;


//@property (nonatomic, retain) CLLocationManager *locationManager;

@property int floorId;
@property int crowdId;

@property (retain,nonatomic) UIView *refresh_view;
@property (nonatomic,retain) IBOutlet UIView *mobView;
@property (retain,nonatomic) UIButton *refresh_btn;
@property (retain,nonatomic) UIView *refresh_lbl_view;
@property (retain,nonatomic) IBOutlet UIImageView *prayImage;
@property (retain,nonatomic) IBOutlet UIButton *rightBtn;
@property (retain,nonatomic) IBOutlet UIButton *leftBtn;

@property (nonatomic,retain) IBOutlet UILabel *leftLab;
@property (nonatomic,retain) IBOutlet UILabel *centLab;
@property (nonatomic,retain) IBOutlet UILabel *rightLab;
//@property (nonatomic,retain) IBOutlet UILabel *timeLabel;
@property (nonatomic,retain) NSMutableArray *praysLab;
@property (retain, nonatomic) NSMutableArray *praysname;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;

@property(nonatomic,retain)NSMutableArray *menuearr;
@property(nonatomic,retain)NSMutableArray *urlarr;
@property (nonatomic,retain) IBOutlet UIButton *leftbut;
@property (nonatomic,retain) IBOutlet UIButton *centbut;
@property (nonatomic,retain) IBOutlet UIButton *rightbut;
@property (nonatomic,retain) IBOutlet UIView *sa3yView;
@property (nonatomic,retain) IBOutlet UIView *headerView;
@property (nonatomic,retain) IBOutlet UIView *ka3baView;
@property (nonatomic,retain) IBOutlet UIView *jamaratView;
@property (retain, nonatomic) NSMutableArray *moblabs;
@property (retain, nonatomic) NSMutableArray *mobViews;

@property (retain, nonatomic) NSMutableArray *crowdImgs;
@property (retain, nonatomic) NSArray *crowdColors;
@property (retain, nonatomic) NSMutableArray *crowdNames;
@property (retain,nonatomic) IBOutlet UIImageView *liveImage;
@property (retain,nonatomic) IBOutlet  UIImageView *ka3baimg;
@property int prayIndex;
@property int labId;

@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *contactDB;

@property int curView;
@property int prevTag;
@property int viewId;
@property int labText;


//-(NSMutableArray *) getTemp;
-(NSString *) getMacTemp;
-(void) drawHeader;
-(void) drawTimePlayers;

-(void) drawMob;
-(void) drawMedArea;

-(void) getCrowd;
-(int)  getPrayerID;
-(void) drawTawefAnimat;
-(void) drawSa3ayAnimat;
-(void) drawJameratAnimat;
-(void) drawAnimatwithInt:(int)curTag;
-(int)  getPrayerPM;
@property (nonatomic, retain) SKMenu *myData;

@property (nonatomic, retain) UILabel *first_title;
@property (nonatomic, strong) NSTimer *timer;

-(IBAction)moveView:(id)sender;
-(IBAction)moveMansk:(id)sender;
-(IBAction)removeNews:(id)sender;

-(IBAction)btnClicked:(id)sender;

-(void)updateAndsave;
-(void)getCrowdData;
-(void) startTimer;
-(void)updateDisplay;




@end
