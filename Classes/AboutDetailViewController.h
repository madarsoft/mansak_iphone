//
//  AboutDetailViewController.h
//  mutnav2
//
//  Created by walid nour on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BugSense-iOS/BugSenseController.h>
@interface AboutDetailViewController : UIViewController<UIWebViewDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *bgImage;
@property (nonatomic, retain) NSMutableArray *pathArr;
@property (nonatomic, retain) NSMutableArray *titleArr;

@property int pid; 
           

-(NSString *)getDetails;


@end
