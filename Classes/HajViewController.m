//
//  HajViewController.m
//  mutnav2
//
//  Created by waleed on 1/13/13.
//
//

#import "HajViewController.h"
#import "HajjTextViewController.h"

@interface HajViewController ()

@end

@implementation HajViewController
@synthesize bgImage, mainTable;
@synthesize myid,menuearr,plist,sdic;
@synthesize mainarr;
@synthesize type;
@synthesize hajjType,dayIndex,titleName;
@synthesize dataarr,mansekarr;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
      self.view.frame=CGRectMake(0, 0, 320, 430*highf);
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,430*highf);
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 45*highf, 320, 350*highf) style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=50.0*highf;
   // mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
  //  mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    [self loadData];
    [self fillData];
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 40*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =titleName;
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    titleLabel.numberOfLines=0;
    [self.view addSubview:titleLabel];
    
   /* NSString *mypath=[[NSBundle mainBundle] pathForResource:@"mainmenue" ofType:@"plist"];
	mainarr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
    sdic=[mainarr objectAtIndex:0];
    plist=[[NSString alloc] initWithFormat:@"%@15",@"secondmenue"];
    
    NSString *path=[[NSBundle mainBundle] pathForResource:plist ofType:@"plist"];
	menuearr=[[NSMutableArray alloc]initWithContentsOfFile:path ];*/
  
    /*if (flag==0) {
        adsView = [[AdsViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        adsView.adsID = 2;
        [adsView getURLS];
        if (adsView.statusID==0) {
            [self.view addSubview:adsView.view];
            [self performSelector: @selector(stopAds) withObject: nil afterDelay:10];
        }
        flag=1;
    }*/
    // Do any additional setup after loading the view from its nib.
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.navigationController.navigationBar.hidden=YES;
    if (type>1) {
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    }
}
/*
-(void)stopAds{
    flag=0;
    [adsView.view removeFromSuperview];
    //[adsView release];
}*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
    [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    [cell.contentView addSubview:bgView];
    [cell setBackgroundView:bgView];
    cell.backgroundColor = [UIColor clearColor];
    [bgView release];
    // Configure the cell...
	//NSMutableDictionary *dic=[menuearr objectAtIndex:indexPath.row];
	cell.textLabel.text=[menuearr objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.textLabel.numberOfLines = 0;
      cell.textLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
    cell.textLabel.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
    //cell.selectionStyle = UITableViewCellSelectionStyleGray;
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    lbl.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
    lbl.text = [NSString stringWithFormat:@"%i",indexPath.row +1];
    lbl.backgroundColor = [UIColor clearColor];
    cell.accessoryView = lbl;
    [lbl release];
    
    /*UIImageView *bggView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    [bggView setImage:[UIImage imageNamed:@"cellbgg.png"]];
    [cell setSelectedBackgroundView:bggView];
    [bggView release];*/
    
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    //[bgView setImage:[UIImage imageNamed:@"i3.png"]];
    //[[tableView cellForRowAtIndexPath:indexPath] setBackgroundView:bgView];
    if (type==1) {
        HajViewController *controller =[[HajViewController alloc]initWithNibName:@"ListViewController" bundle:nil];
        controller.type=2;
        controller.hajjType=indexPath.row;
        controller.titleName=[menuearr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
        
    }
    else if (type==2){
        dayIndex = indexPath.row;
        HajViewController *controller =[[HajViewController alloc]initWithNibName:@"ListViewController" bundle:nil];
        controller.type=3;
        controller.dayIndex=indexPath.row;
        controller.hajjType=self.hajjType;
        controller.titleName=[menuearr objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    else if (type==3){
        int txtID = [[mansekarr objectAtIndex:indexPath.row]intValue ];
        NSLog(@"%i",txtID);
        NSMutableDictionary *ddic = [[NSMutableDictionary alloc]init];
        for (int i=0; i<[dataarr count]; i++) {
           NSMutableDictionary *tmpdic =[dataarr objectAtIndex:i];
           /* int x = [[tmpdic valueForKey:@"ID"] intValue];
            NSLog(@"%i",x);*/
            if ([[tmpdic valueForKey:@"ID"] intValue]==txtID) {
                ddic = tmpdic;
               /* [tmpdic release];
                break;*/
            }
          //  [tmpdic release];
        }
        
        
        HajjTextViewController *detailViewController = [[HajjTextViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        detailViewController.sdic=ddic;
        detailViewController.titleName = [menuearr objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
       // [dic release];
        
    }
 
}
- (void)viewDidUnload
{
    [self setBgImage:nil];
    [self setMainTable:nil];
    [super viewDidUnload];
    
}
-(void)loadData{
    if (type==1) {
        menuearr = [[NSMutableArray alloc]initWithObjects:@"إفراد",@"قران",@"تمتع", nil];
        
    }
    else if (type==2){
         menuearr = [[NSMutableArray alloc]initWithObjects:@"أعمال ما قبل ٨ ذي الحجة",@"أعمال ٨ ذي الحجة",@"أعمال يوم عرفة",@"أعمال يوم النحر",@"أعمال أيام التشريق",@"أعمال اليوم الأخير", nil];
    }
    else if (type==3){
        if (hajjType==0) {
            if (dayIndex==0) {
                 menuearr = [[NSMutableArray alloc]initWithObjects:@"الإحرام",@"طواف القدوم",@"السعى",@"الإقامة بمكة", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"0",@"1",@"2",@"3", nil];
            }
            else if (dayIndex==1){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"المبيت بمنى", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"4", nil];
            }
            else if (dayIndex==2){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الوقوف بعرفة",@"الإفاضة إلى المزدلفة", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"5",@"6", nil];
            }
            else if (dayIndex==3){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الدفع إلى منى",@"رمى جمرة العقبة",@"ذبح الهدى",@"الحلق والتحلل",@"طواف الإفاضة",@"السعى لمن لم يسع", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"15",@"7",@"24",@"8",@"22",@"16", nil];
            }
            else if (dayIndex==4){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"رمى الجمرات",@"المبيت بمنى", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"10",@"11", nil];
            }
            else if (dayIndex==5){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"طواف الوداع", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"12", nil];
            }
            
        }
        else if (hajjType==1){
            if (dayIndex==0) {
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الإحرام",@"طواف القدوم",@"السعى",@"الإقامة بمكة", nil];
                mansekarr = [[NSMutableArray alloc]initWithObjects:@"17",@"1",@"18",@"3", nil];
            }
            else if (dayIndex==1){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"المبيت بمنى", nil];
                  mansekarr = [[NSMutableArray alloc]initWithObjects:@"4", nil];
            }
            else if (dayIndex==2){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الوقوف بعرفة",@"الإفاضة إلى المزدلفة", nil];
                  mansekarr = [[NSMutableArray alloc]initWithObjects:@"5",@"6", nil];
            }
            else if (dayIndex==3){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الدفع إلى منى",@"رمى جمرة العقبة",@"ذبح الهدى",@"الحلق والتحلل",@"طواف الإفاضة",@"السعى لمن لم يسع", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"15",@"7",@"14",@"8",@"9",@"16", nil];
            }
            else if (dayIndex==4){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"رمى الجمرات",@"المبيت بمنى", nil];
                 mansekarr = [[NSMutableArray alloc]initWithObjects:@"10",@"11", nil];
            }
            else if (dayIndex==5){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"طواف الوداع", nil];
                mansekarr = [[NSMutableArray alloc]initWithObjects:@"12", nil];
            }

        }
        else if (hajjType==2){
            if (dayIndex==0) {
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الإحرام",@"طواف القدوم",@"السعى",@"الحلق والتحلل",@"الإقامة بمكة", nil];
                mansekarr = [[NSMutableArray alloc]initWithObjects:@"23",@"1",@"21",@"19",@"3", nil];
            }
            else if (dayIndex==1){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الإحرام من مكة",@"المبيت بمنى", nil];
                mansekarr = [[NSMutableArray alloc]initWithObjects:@"13",@"4", nil];
            }
            else if (dayIndex==2){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"الوقوف بعرفة",@"الإفاضة إلى المزدلفة", nil];
                  mansekarr = [[NSMutableArray alloc]initWithObjects:@"5",@"6", nil];
            }
            else if (dayIndex==3){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"لدفع إلى منى",@"رمى جمرة العقبة",@"ذبح الهدى",@"الحلق والتحلل",@"طواف الإفاضة",@"السعى لمن لم يسع", nil];
                   mansekarr = [[NSMutableArray alloc]initWithObjects:@"15",@"7",@"14",@"8",@"9",@"20", nil];
            }
            else if (dayIndex==4){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"رمى الجمرات",@"المبيت بمنى", nil];
                  mansekarr = [[NSMutableArray alloc]initWithObjects:@"10",@"11", nil];
            }
            else if (dayIndex==5){
                menuearr = [[NSMutableArray alloc]initWithObjects:@"طواف الوداع", nil];
                  mansekarr = [[NSMutableArray alloc]initWithObjects:@"12", nil];
            }

        }
    }
}


-(void)fillData{
    dataarr = [[NSMutableArray alloc]init];
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:0] forKey:@"ID"];
    [dic setValue:@"iihram" forKey:@"Men"];
    [dic setValue:@"ihramerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:1] forKey:@"ID"];
    [dic setValue:@"toaf" forKey:@"Men"];
    [dic setValue:@"toaferrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:2] forKey:@"ID"];
    [dic setValue:@"isai" forKey:@"Men"];
    [dic setValue:@"saierrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:3] forKey:@"ID"];
    [dic setValue:@"bakaa" forKey:@"Men"];
    [dic setValue:@"bakaaerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:4] forKey:@"ID"];
    [dic setValue:@"menna" forKey:@"Men"];
    [dic setValue:@"mennaerrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:5] forKey:@"ID"];
    [dic setValue:@"arfaa" forKey:@"Men"];
    [dic setValue:@"arfaaerrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:6] forKey:@"ID"];
    [dic setValue:@"mozdalefa1" forKey:@"Men"];
    [dic setValue:@"mozdalefaerrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:7] forKey:@"ID"];
    [dic setValue:@"akbaa" forKey:@"Men"];
    [dic setValue:@"akbaaerrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:8] forKey:@"ID"];
    [dic setValue:@"halk" forKey:@"Men"];
    [dic setValue:@"halkerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:9] forKey:@"ID"];
    [dic setValue:@"ifada" forKey:@"Men"];
    [dic setValue:@"toaferrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:10] forKey:@"ID"];
    [dic setValue:@"gamarat" forKey:@"Men"];
    [dic setValue:@"akbaaerrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:11] forKey:@"ID"];
    [dic setValue:@"menna2" forKey:@"Men"];
    [dic setValue:@"menna2errors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:12] forKey:@"ID"];
    [dic setValue:@"wadaa" forKey:@"Men"];
    [dic setValue:@"wadaaerrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:13] forKey:@"ID"];
    [dic setValue:@"ihram2" forKey:@"Men"];
    [dic setValue:@"ihram2errors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:14] forKey:@"ID"];
    [dic setValue:@"zabh" forKey:@"Men"];
    [dic setValue:@"zabherrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:15] forKey:@"ID"];
    [dic setValue:@"dafea" forKey:@"Men"];
    [dic setValue:@"dafeaerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:16] forKey:@"ID"];
    [dic setValue:@"sai2" forKey:@"Men"];
    [dic setValue:@"saierrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:17] forKey:@"ID"];
    [dic setValue:@"qihram" forKey:@"Men"];
    [dic setValue:@"ihramerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:18] forKey:@"ID"];
    [dic setValue:@"qsai" forKey:@"Men"];
    [dic setValue:@"saierrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:19] forKey:@"ID"];
    [dic setValue:@"thalk" forKey:@"Men"];
    [dic setValue:@"halkerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:20] forKey:@"ID"];
    [dic setValue:@"tsai" forKey:@"Men"];
    [dic setValue:@"saierrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:21] forKey:@"ID"];
    [dic setValue:@"qsai" forKey:@"Men"];
    [dic setValue:@"saierrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:22] forKey:@"ID"];
    [dic setValue:@"iifada" forKey:@"Men"];
    [dic setValue:@"toaferrors" forKey:@"Error"];
    [dic setValue:@"Y" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:23] forKey:@"ID"];
    [dic setValue:@"oihram" forKey:@"Men"];
    [dic setValue:@"ihramerrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
    
    dic = [[NSMutableDictionary alloc]init];
    [dic setValue:[NSNumber numberWithInt:24] forKey:@"ID"];
    [dic setValue:@"izabh" forKey:@"Men"];
    [dic setValue:@"zabherrors" forKey:@"Error"];
    [dic setValue:@"N" forKey:@"Type"];
    [self.dataarr addObject:dic];
    [dic release];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *currentSelectedIndexPath = [tableView indexPathForSelectedRow];
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
        [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    if (currentSelectedIndexPath != nil)
    {
        [[tableView cellForRowAtIndexPath:currentSelectedIndexPath] setBackgroundView:bgView];
    }
    
    return indexPath;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    if (cell.isSelected == YES)
    {
        //[bgView setImage:[UIImage imageNamed:@"i3.png"]];
        
        //[cell setBackgroundView:bgView];
        
    }
    else
    {
        
        if (indexPath.row%2==0) {
            [bgView setImage:[UIImage imageNamed:@"i1.png"]];
        }
        else{
            [bgView setImage:[UIImage imageNamed:@"i2.png"]];
        }
        [cell setBackgroundView:bgView];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
	[super dealloc];
}

@end
