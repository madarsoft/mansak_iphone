//
//  custom_pagecontrol.m
//  mutnav2
//
//  Created by mohamed on 8/16/14.
//
//

#import "custom_pagecontrol.h"

@interface custom_pagecontrol()

- (void)updateDots;

@end

@implementation custom_pagecontrol
@synthesize normalImage = _normalImage;
@synthesize highlightedImage = _highlightedImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.normalImage = [UIImage imageNamed:@"inactive_page_image.png"];
        self.highlightedImage = [UIImage imageNamed:@"active_page_image.png"];
        
    }
    return self;
}
- (void)setNormalImage:(UIImage *)normalImage
{
    _normalImage = normalImage;
    [self updateDots];
}
- (void)setHighlightedImage:(UIImage *)highlightedImage
{
    _highlightedImage = highlightedImage;
    [self updateDots];
}
-(void) views{
    for (int i = 0; i < [self.subviews count]; i++)
    {
        UIView* dot = [self.subviews objectAtIndex:i];
        dot.frame=CGRectMake(dot.frame.origin.x, dot.frame.origin.y, dot.frame.size.width+2.5, dot.frame.size.height+2.5);
        UIImageView *dot_image=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, dot.frame.size.width, dot.frame.size.height)];
        dot_image.image=[UIImage imageNamed:@"inactive_page_image.png"];
        [dot addSubview:dot_image];
    }
}
- (void)updateDots
{
    if(_normalImage || _highlightedImage)
    {
        NSArray *subviews = self.subviews;
        for(NSInteger i = 0;i<subviews.count;i++)
        {
            UIView *dot = [subviews objectAtIndex:i];
            if ([dot.subviews count]>0) {
                UIImageView *dot_view = [dot.subviews objectAtIndex:0];
                if ([[[UIDevice currentDevice] systemVersion] floatValue] <= 7.1) {
                    dot_view.image = self.currentPage == i ? _highlightedImage : _normalImage ;
                }
            }else{
                [self views];
                [self updateDots];
            }
        }
    }
}

- (void)setCurrentPage:(NSInteger)currentPage
{
    [super setCurrentPage:currentPage];
    [self updateDots];
}

@end
