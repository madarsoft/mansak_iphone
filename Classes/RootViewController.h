//
//  RootViewController.h
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>
#import "PrayTimeViewController.h"
#import "LocViewController.h"


@interface RootViewController : UITableViewController <NSFetchedResultsControllerDelegate> {
	
@private
    NSFetchedResultsController *fetchedResultsController_;
    NSManagedObjectContext *managedObjectContext_;
	NSMutableArray *mainarr;
    
}

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSMutableArray *mainarr;
@property (nonatomic, retain) IBOutlet UIImageView *backImg;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;


-(IBAction)buttonPressed:(id)sender;

           
@end
