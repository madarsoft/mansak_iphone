//
//  adsViewController.h
//  testnewads
//
//  Created by hossam fathy on 8/21/13.
//  Copyright (c) 2013 hossam fathy. All rights reserved.
//

#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "SKMenu.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

@class Reachability;
@interface adsViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic,retain) NSString *mnc;
@property(nonatomic,retain) NSString *mcc;
@property(nonatomic,retain) UIWebView *footerad;
@property(nonatomic,retain) UIButton *btnclosefooter;
@property(nonatomic,retain) NSString *footerurl;
@property(nonatomic,retain) UIWebView *splashad;
@property(nonatomic,retain) UIImageView *splashadImg;
@property(nonatomic,retain) UIButton *btnsplashurl;
@property(nonatomic,retain) UIButton *btnclosesplash;
@property(nonatomic,retain) NSString *splashurl;
@property(nonatomic,retain) UIButton *btnthirdad;
@property(nonatomic,retain) NSString *thirdURL;
@property int place,country,closedsplash;
@property (nonatomic, retain) SKMenu *myData;

@property (nonatomic,retain) NSString *splURL;

@property(nonatomic,retain) NSXMLParser *adsparser;
@property(nonatomic,retain) NSMutableArray *ads;
@property(nonatomic,retain) NSMutableDictionary *ad;
@property(nonatomic,retain) NSString *currentElement;
@property(nonatomic,retain) NSMutableString *ElementValue;
@property BOOL errorParsing;


//@property(nonatomic,retain)CLGeocoder *geoCoder;
//@property(nonatomic,retain)CLLocationManager *locationManager;
//@property(nonatomic,retain) NSString *latitude;
//@property(nonatomic,retain) NSString *langitude;

- (void)parseXMLFileAtURL:(NSString *)URL;
////////

@property (nonatomic, retain) IBOutlet UIButton *adsButton;
@property (nonatomic, retain) NSString *imageURl;
@property (nonatomic, retain) NSString *buttonURL;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *thridContent;


@property (nonatomic, retain) IBOutlet UIImage *img;
@property int statusID;

@property int adsType;
@property int duration;

@property int ttt;

-(UIImage *) loadImage:(NSString *)fileName inDirectory:(NSString *)directoryPath;
-(void) saveImage:(UIImage *)image;
-(void) saveToDataBase:(NSString *)imgName WithPath:(NSString *)imgPath;
-(void) getImageFromDataBase;
-(void) getContentFromDataBase;
-(void) removeImageFromDataBase;
-(void) loadSplashContent;
-(void) loadThirdContent;
-(void) saveContent;
-(int) returnStatus;
-(void) removeContentFromDataBase;
-(void)getPlace;
-(int)getFooter;
-(void)drawSplash;
-(void)checkSplashName;
-(void)checkThird;

@end
