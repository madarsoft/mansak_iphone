//
//  HaramViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 4/1/15.
//
//
#import "Global.h" 
#import <UIKit/UIKit.h>
#import "SiteViewController.h"


@interface HaramViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UITableView *mainTable;
@property(nonatomic,strong)NSMutableArray *menuearr;
@property(nonatomic,strong)NSMutableArray *imgarr;

@property (nonatomic,retain) UIView *news_view;
@property (retain, nonatomic) IBOutlet UIWebView *myWeb;
@property (nonatomic,retain) UIImageView *botimg;



@property int type;


@end
