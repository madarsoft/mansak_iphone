//
//  NewsViewController.h
//  mutnav2
//
//  Created by walid nour on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@class Reachability;


@interface NewsViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (retain, nonatomic) IBOutlet UIWebView *myWeb;

@property(nonatomic,retain)NSMutableArray *menuearr;
@property(nonatomic,retain)NSMutableArray *urlarr;
@end
