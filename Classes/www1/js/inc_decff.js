function getsize()
{
    return localStorage.getItem("fontsize");
}
function setsize(size)
{
    $('.mansak').css('font-size', size);
    $('.number').css('font-size', size);
    $('.sub_title').css('font-size', size + 2 );
    $('.main_title').css('font-size', size + 4);
    $('.doaa2').css('font-size', size);
    localStorage.setItem("fontsize",size);

}
function test_fun(){
    inc();
    window.location  = 'ios:webToNativeCall';
}
function inc(){
    var oldFontSize =getsize();
    var f1 = parseInt(oldFontSize) + 2
    setsize(f1);
    window.location  = 'ios:webToNativeCall';
}
function dec(){
    var oldFontSize =getsize();
    var f1 = parseInt(oldFontSize - 2)
    setsize(f1);
    window.location  = 'ios:webToNativeCall';
}
$(function(){
  if (localStorage.getItem("fontsize")==null){
  localStorage.setItem("fontsize",14);
  }
  $('.increase').click(function(e) {
                       inc();
                       });
  
  $('.decrease').click(function(e) {
                       
                       dec();
                       });
});