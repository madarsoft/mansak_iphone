//
//  PrayTimeViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 2/11/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "prayertimecalculator.h"
#import "HijriTime.h"
@interface PrayTimeViewController : UIViewController


@property (nonatomic,retain)  NSArray *prayerTimes;
@property (nonatomic,retain)  NSArray *prayerTimesMa;
@property (retain, nonatomic) IBOutlet UIView *header_view;
@property (retain, nonatomic) IBOutlet UIView *footer_view;
@property (nonatomic,retain)  NSArray *prayerTimesIcons;
@property (nonatomic,retain)  NSArray *prayerTimesIcons_ON;
@property (nonatomic,retain)  NSArray *prayTimes;
-(void)drawPrayTimes;
-(int)  getPrayerPM;


@end
