//
//  AboutViewController.h
//  itour
//
//  Created by walid nour on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AdsViewController.h"
#import <BugSense-iOS/BugSenseController.h>

@interface AboutViewController : UIViewController <UIScrollViewDelegate , UIWebViewDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *bgImage;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

//@property (nonatomic, retain) IBOutlet AdsViewController *adsView;



-(IBAction)btnPressed:(id)sender ;
-(void)firstView;
-(void)stopAds;

-(void)updataView;
@end
