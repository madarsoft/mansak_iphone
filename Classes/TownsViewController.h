//
//  TownsViewController.h
//  GadawalV1
//
//  Created by waleed on 9/2/13.
//
//

#import <UIKit/UIKit.h>
#import "SKMenu.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "sqlite3.h"
@class Reachability;

@interface TownsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    sqlite3 *db;
}

@property(nonatomic,retain) NSString *mcc;
@property (nonatomic, retain) SKMenu *myData;

@property (nonatomic, retain) IBOutlet UITableView *townTable;

@property(nonatomic,retain) NSXMLParser *townparser;
@property(nonatomic,retain) NSMutableArray *towns;
@property(nonatomic,retain) NSMutableDictionary *town;
@property(nonatomic,retain) NSString *currentElement;
@property(nonatomic,retain) NSMutableString *ElementValue;
@property int selectedTown;
@property BOOL errorParsing;
@property int type;
-(void)saveData;
- (void)parseXMLFileAtURL:(NSString *)URL;

@end
