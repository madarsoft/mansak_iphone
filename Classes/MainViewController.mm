//
//  MainViewController.m
//  mutnav2
//
//  Created by walid nour on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MainViewController.h"
#import "Reachability.h"
#import "RXMLElement.h"

@implementation MainViewController

@synthesize prayerTimes;
@synthesize backImage;
@synthesize tawef,saai;
@synthesize prayTimes;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
      /*  if ([self isViewLoaded]) {
            self.view = nil;
            [self viewDidLoad];
        }*/
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
   
  /*  tawef = [[NSMutableArray alloc] init];
    saai = [[NSMutableArray alloc] init];
    [self drawHeader];
    [self drawTimePlayers];
    [self drawFooter];
    self.navigationController.navigationBar.hidden = YES;*/
}

-(void) viewWillAppear:(BOOL)animated{
   
    /*backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    backImage.frame = CGRectMake(0, 0, 320,420);
    [self.view addSubview:backImage];*/
    
    for (UIView *v in self.view.subviews) {
        [v removeFromSuperview];
    }
    
    backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    backImage.frame = CGRectMake(0, 0, 320,420);
    [self.view addSubview:backImage];
    [self drawFooter];
    tawef = [[NSMutableArray alloc] init];
    saai = [[NSMutableArray alloc] init];
    [self drawHeader];
    [self drawTimePlayers];
    
    [self drawTemp];
    [self drawCrowd];
    self.navigationController.navigationBar.hidden = YES;
  /*  adsView = [[AdsViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    adsView.adsID = 2;
    [self.view addSubview:adsView.view];
    [self performSelector: @selector(stopAds) withObject: nil afterDelay:10];
*/
     [super viewWillAppear:animated];
}




- (void)viewDidUnload
{
    [self setBackImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) drawHeader{
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    img.frame = CGRectMake(1, 0, 318, 16);
    [self.view addSubview:img];
    [img release];
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"EEEE:MM/dd"];
    //Get the string date
    NSString *str = [NSString stringWithFormat:@"مكة %@",[formatter stringFromDate:date]];
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(55, 0, 160, 16)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.alpha = 1;
    timeLabel.text = str;
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.textAlignment = UITextAlignmentRight;
    timeLabel.font = [UIFont fontWithName:@"Arial" size:14];
    [self.view addSubview:timeLabel];
    [timeLabel release];
    }

-(void) drawTimePlayers
{
    NSDate* curDate = [NSDate date];
    NSCalendar* cal = [NSCalendar currentCalendar]; 
    NSDateComponents* dateOnlyToday = [cal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)fromDate:curDate];
    int d = [dateOnlyToday day];
    int m = [dateOnlyToday month];
    int y = [dateOnlyToday year];
    PrayersCaculator *calM = [[PrayersCaculator alloc]initWithDay:d Month:m Year:y Lat:21.4266667 Long:39.8261111 Zone:3 Mazhab:1 Way:3 DLS:0];
    self.prayerTimes = [calM getPrayersStrings];
    self.prayTimes = [calM getPrayersDoubles];
    
   // [calM release];
    
   NSMutableArray *imgActive = [[NSMutableArray alloc] initWithObjects:@"06.png",@"05.png",@"04.png",@"03.png",@"02.png",@"01.png", nil];
    NSMutableArray *imgUnActive = [[NSMutableArray alloc] initWithObjects:@"006.png",@"005.png",@"004.png",@"003.png",@"002.png",@"001.png", nil];
    
    int idd = [self getPrayerID];
      
    int j=0;
     for (int i=0; i<6; i++) {
 //    NSString *imgPath = [[NSString alloc]init];
             NSString *imgPath ;
     if (i == idd) {
     imgPath = [imgUnActive objectAtIndex:i];
     //    imgPath = [imgUnActive objectAtIndex:4-idd];
     }
     else
     {
     imgPath = [imgActive objectAtIndex:i];
     }
     UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgPath]];
     //      [imgPath release];
      img.frame = CGRectMake((i*53), 16, 53, 92);
     
     [self.view addSubview:img];
       
     [img release];
      //   [imgActive release];
      //   [imgUnActive release];
     
     UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(((i*53)+5), 80, 53, 20)];
     timeLabel.backgroundColor = [UIColor clearColor];
     timeLabel.alpha = 0.7;
     timeLabel.text = [prayerTimes objectAtIndex:5-i];
     timeLabel.textColor = [UIColor whiteColor];
     timeLabel.font = [UIFont boldSystemFontOfSize:12];
     [self.view addSubview:timeLabel];
     [timeLabel release];
         j++;
     }
    
  
}

-(void) drawFooter{
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footer.png"]];
    img.frame = CGRectMake(8, 220, 304, 180);
    [self.view addSubview:img];
    [img release];
    
  
}

-(int)  getPrayerID
{
     Date dt;
    
    double curTimeDouble = dt.getClockDouble();
    
    
    int idd=5;
    for (int i=0; i<6; i++) {
        double time = [[prayTimes objectAtIndex:5-i]doubleValue];
      
        if (curTimeDouble > time) {
    
            return idd=i;
        }
    }
    

    return idd;
}

-(NSMutableArray *) getTemp{
    NSMutableArray * temps = [[NSMutableArray alloc] init ];
    NSMutableArray * cities = [[NSMutableArray alloc] initWithObjects:@"1939897",@"1937801", nil];
    
    Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {
        for (int i=0; i<2; i++) {
            NSString *tmpUrl = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=%@&u=c", [cities objectAtIndex:i]];
            
            RXMLElement *parser = [RXMLElement elementFromURL:[NSURL URLWithString:tmpUrl]];
           
            
            [parser iterateWithRootXPath:@"//item" usingBlock: ^(RXMLElement *item) {
                NSLog(@"Player #5: %@", [item child:@"description"]);
                 NSString *txt  = [NSString stringWithFormat:@"%@",[item child:@"description"]];
                NSArray *tmp = [txt componentsSeparatedByString:@" "];
                NSString *currentTemp;
                if ([[tmp objectAtIndex:5]length ]>2) {
                    currentTemp = [tmp objectAtIndex:6];
                }
                else {
                    currentTemp = [tmp objectAtIndex:5];
                }
                // NSLog(@"====%@",currentTemp);
                if (currentTemp==NULL) {
                    currentTemp = @"";
                }
                NSLog(@"%@",currentTemp);
                [temps addObject:currentTemp];
              
            }];
           
            
            
        }
    }
    else
    {
        for (int i=0; i<2; i++) {
            [temps addObject:@"0"];
        }
    }
    
    //[parser release];
    
     
    
    return temps;
}

-(void) drawTemp {
   NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    NSMutableArray *temps = [self getTemp];
    
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(140, 250, 25, 15)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.alpha = 0.7;
    timeLabel.text = [temps objectAtIndex:0];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont boldSystemFontOfSize:20];
    [self.view addSubview:timeLabel];
    [timeLabel release];
    
    
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 250, 25, 15)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.alpha = 0.7;
    timeLabel.text = [temps objectAtIndex:1];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont boldSystemFontOfSize:20];
    [self.view addSubview:timeLabel];
    [timeLabel release];
     [pool release];
 }

-(void) drawCrowd {
   
    Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {
   // if (netStatus) {

       /* for (int i=0; i<3; i++) {
            [tawef addObject:@"__"];
            [saai addObject:@"__"];
        }
        UIAlertView *connectionAlert = [[UIAlertView alloc] initWithTitle:@"تنبيه" message:@"لا يوجد اتصال" delegate:self cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil];
        [self.view addSubview:connectionAlert];
        NSLog(@"%i",netStatus);
        
        
    }
    else
    {*/
    
    NSURL *url = [NSURL URLWithString:@"http://hinewz.com/testlink.aspx?pid=0&lid=2"];
    NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        NSLog(@"%@",content);
    NSArray *allLines = [content componentsSeparatedByString: @";"];
    for (int x=0; x <[allLines count]-1; x++) {
        NSString *line = [NSString stringWithFormat:@"%@",[allLines objectAtIndex:x]];
        NSLog(@"%@",line);
        NSArray *Lines = [line componentsSeparatedByString: @","];
        [tawef addObject:[Lines objectAtIndex:2]];
    }
    
    url = [NSURL URLWithString:@"http://hinewz.com/testlink.aspx?pid=1&lid=2"];
    content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    allLines = [content componentsSeparatedByString: @";"];
    for (int x=0; x <[allLines count]-1; x++) {
        NSString *line = [NSString stringWithFormat:@"%@",[allLines objectAtIndex:x]];
         NSLog(@"%@",line);
        NSArray *Lines = [line componentsSeparatedByString: @","];
        [saai addObject:[Lines objectAtIndex:2]];
    }
    }
    
    for (int x=0; x<2; x++) {
        for (int y =0; y<3; y++) {
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake((35+(x*100)), (310+(y*32)), 70, 15)];
            timeLabel.backgroundColor = [UIColor clearColor];
            //timeLabel.alpha = 0.7;
            timeLabel.textAlignment=UITextAlignmentRight;
            if (x==0) {
                if ([saai count]==0) {
                    timeLabel.text = @"__";
                }
                else
                {
                timeLabel.text = [saai objectAtIndex:y];
                }
            }
            else if(x==1)
            {
                if ([tawef count]==0) {
                    timeLabel.text = @"__";
                }
                else
                {
             timeLabel.text = [tawef objectAtIndex:y];
                }
            }
            
            timeLabel.textColor = [UIColor blackColor];
            timeLabel.font = [UIFont boldSystemFontOfSize:16];
            [self.view addSubview:timeLabel];
            [timeLabel release];
            }
        }
}

@end
