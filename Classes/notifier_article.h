//
//  notifier_article.h
//  queen
//
//  Created by hossam fathy on 9/1/14.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@class AppDelegate;
@interface notifier_article : UIView{
    UIButton *btntitle;
    UIWebView *mywebview;
    UIButton* btnWebCloser;
    AppDelegate *appDelegate;
    UIView *webHeader;
}
@property(nonatomic,strong) NSString *title;
@property int id;
- (id)init;
-(void)show :(NSString*)title id:(int)id;
-(void)closeWebView;
@end
