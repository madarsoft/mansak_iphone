//
//  SearchListController.m
//  itour
//
//  Created by walid nour on 3/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SearchListController.h"


@implementation SearchListController

@synthesize bgImage, mainTable;
@synthesize mainarr, filearr ,iddarr;
@synthesize townID, listType, buildID;
@synthesize titleName ,fileName;
@synthesize searchID;
@synthesize sv;
@synthesize checkID;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    mainarr = [[NSMutableArray alloc] init];
    iddarr  = [[NSMutableArray alloc] init];
    filearr = [[NSMutableArray alloc] init];
    
    [self fillArr];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"عودة" style:UIBarButtonItemStyleBordered                        target:nil  action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,370*highf);
 
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0.0, 320,370*highf) style:UITableViewStyleGrouped];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=45.0*highf;
    mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
 //   [mainTable setBackgroundView:bgImage];
  [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    self.title = titleName;
    
}


- (void)viewDidUnload
{
    [self setMainarr:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mainarr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
        
    }

    if (indexPath.row==checkID) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
        // Configure the cell...
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
        cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
        cell.textLabel.numberOfLines = 0;

    return cell;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for(int iterator=0;iterator<mainarr.count;iterator++){
        UITableViewCell *eachCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:iterator inSection:0]];
        [eachCell setSelected:NO animated:YES];
        [eachCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    [newCell setSelected:YES animated:YES];
    [newCell setAccessoryType:UITableViewCellAccessoryCheckmark];
    if (listType==1) {
        sv.townID = indexPath.row;
        townName = [mainarr objectAtIndex:indexPath.row];
    }
    else if (listType==2) {
        sv.buildID = indexPath.row;
        buildName =[mainarr objectAtIndex:indexPath.row];
    }
    else if (listType==3) {
         sv.searchID = indexPath.row;
        typeName  =[mainarr objectAtIndex:indexPath.row];
    }
   
    
}


- (void) fillArr
{
    if(listType==1)
    {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"الرياض",@"المدينة النبوية",@"مكة المكرمة",@"الطائف",@"جدة",@"الدمام",@"الأحساء",@"الجبيل",@"الخبر", nil];
    }
    else if(listType==2)
    {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"الفنادق",@"وحدات سكنية",@"منتجعات سياحية",@"المطاعم", @"الحدائق",@"متاحف",@"مراكز تجارية",@"المنشآت الرياضية",@"بيوت الشباب",@"منظمو مؤتمرات",@"مستشفيات",@"تأجير السيارات",@"حجوزات", nil];
        filearr = [[NSMutableArray alloc] initWithObjects: @"hotel",@"apart",@"park",@"rest",@"parks",@"museum",@"retail",@"sport",@"hostel",@"exhib",@"hospital",@"trans",@"travel", nil];
    }
    else if(listType==3)
    {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"ابحث باسم المنشأة",@"بحث بشارع المنشأة", nil];
        
    }
    else
    {
        NSString *pathRoot = [[NSString alloc] initWithFormat:@"lists"];
        fileName = pathRoot;
        NSString *path=[[NSBundle mainBundle] pathForResource:pathRoot ofType:@"txt"];
        NSError *error;
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        NSArray *allLines = [content componentsSeparatedByString: @"\n"];
        int count = [allLines count];
        for (int i=0; i<count; i++) {
            NSString *line = [allLines objectAtIndex:i];
            NSArray *items = [line componentsSeparatedByString: @"|"];
            if ([[items objectAtIndex:0] intValue] ==townID ) {
                [self.mainarr addObject:[items objectAtIndex:1]];
                [self.iddarr addObject:[items objectAtIndex:2]];
                [self.filearr addObject:[items objectAtIndex:3]];
            }
        }
    }
}

@end
