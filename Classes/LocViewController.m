    //
    //  LocViewController.m
    //  mutnav2
    //
    //  Created by walid nour on 2/22/12.
    //  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
    //

    #import "LocViewController.h"
    #import "MapViewAnnotation.h"
//#import <GoogleMaps/GoogleMaps.h>
@implementation LocViewController{
  //  GMSMapView *mapview_;
}
    @synthesize myMapView, locationManager, locationController;

    - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
    {
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
        if (self) {
            // Custom initialization
        }
        return self;
    }

    - (void)didReceiveMemoryWarning
    {
        // Releases the view if it doesn't have a superview.
        [super didReceiveMemoryWarning];
        
        // Release any cached data, images, etc that aren't in use.
    }

    #pragma mark - View lifecycle

    - (void)viewDidLoad
    {
        [super viewDidLoad];
        
      //  self.title = @"تحديد";
       /* myMapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
        myMapView.delegate =self;
        myMapView.mapType = MKMapTypeStandard;
        myMapView.showsUserLocation = YES;*/
        type1=0;
        
        UILabel *titleView = (UILabel *)self.navigationItem.titleView;
        if (!titleView) {
            titleView = [[UILabel alloc] initWithFrame:CGRectZero];
            titleView.backgroundColor = [UIColor clearColor];
            titleView.font = [UIFont boldSystemFontOfSize:20.0];
            titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
            
            titleView.textColor = [UIColor whiteColor]; // Change to desired color
            
            self.navigationItem.titleView = titleView;
            [titleView release];
        }
        titleView.text = @"تحديد";
        [titleView sizeToFit];
        [self.view addSubview:myMapView];
       // [NSThread detachNewThreadSelector:@selector(displayMap) toTarget:self withObject:nil];
        [self displayMap];
        locationController = [[MyCLController alloc] init];
        locationController.delegate = self;
        [locationController.locationManager startUpdatingLocation];    
        // Do any additional setup after loading the view from its nib.
        UIBarButtonItem *locButton = [[UIBarButtonItem alloc] initWithTitle:@"نوع الخريطة" style:UIBarButtonItemStyleBordered target:self action:@selector(changetype)];
        [self.navigationItem setRightBarButtonItem:locButton];
        [locButton release];
    }


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        
    }
    
}

    - (void)viewDidUnload
    {
        [super viewDidUnload];
        //[self setMyMapView:nil];
        [myMapView release];
        myMapView=nil;
        // Release any retained subviews of the main view.
        // e.g. self.myOutlet = nil;
    }
- (void) changetype {
   /* if (type1==2) {
        [mapview_ setMapType:kGMSTypeNormal];
         type1=0;
    }
    else if (type1==0) {
        [mapview_ setMapType:kGMSTypeHybrid];
         type1=1;
    }
    else if (type1==1) {
        [mapview_ setMapType:kGMSTypeTerrain];
        type1=2;

    }*/
   // [self.view layoutIfNeeded];
    }
-(void)displayMap{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    CLLocationCoordinate2D location; 
    location.latitude = 21.4266667;
    location.longitude = 39.8261111;
   /* GMSCameraPosition *camera=[GMSCameraPosition cameraWithLatitude:location.latitude longitude:location.longitude zoom:15];
    mapview_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapview_.myLocationEnabled=YES;
    mapview_.settings.myLocationButton=YES;
    self.view=mapview_;*/
    [self setAnnotations];  
    [pool release];
}

    -(void)setAnnotations{
        NSString *path=[[NSBundle mainBundle] pathForResource:@"makkabuldings" ofType:@"dat"];
        NSError *error;
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        NSArray *allLines = [content componentsSeparatedByString: @"\n"];
        int count=[allLines count];
        for (int i=0;i<count ;i++){
            NSArray *items = [[allLines objectAtIndex:i] componentsSeparatedByString: @"|"];
            NSString *title = [[NSString alloc] initWithFormat:@"%@",[items objectAtIndex:1]];
            CLLocationCoordinate2D location;
            location.latitude = [[items objectAtIndex:4]floatValue];
            location.longitude =[[items objectAtIndex:5]floatValue];
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location.latitude, location.longitude);
          /*  GMSMarker *marker = [GMSMarker markerWithPosition:position];
            marker.title = title;
            marker.map = mapview_;*/
        }
        
    }

    - (void)locationUpdate:(CLLocation *)location {
        //locationLabel.text = [location description];
        //NSLog(@"%f", location.coordinate.latitude);
    }

    - (void)locationError:(NSError *)error {
        //locationLabel.text = [error description];
    }
-(MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    MKPinAnnotationView *pinView=nil;
    
    if(annotation!=mapView.userLocation){
        static NSString *defaultPinID=@"com.invasivecode.pin";
        
        pinView=(MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        
        if(pinView==nil)
            
            pinView=[[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:defaultPinID]autorelease];
        
        pinView.pinColor=MKPinAnnotationColorRed;
        
        pinView.canShowCallout=YES;
        pinView.animatesDrop  =YES;
        
        [defaultPinID release];
    }
    return pinView;
}


    - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
    {
        // Return YES for supported orientations
        return (interfaceOrientation == UIInterfaceOrientationPortrait);
    }

    - (void)dealloc {
        [myMapView release];
        [locationController release];
        [super dealloc];
    }
    @end
