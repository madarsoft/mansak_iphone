//
//  SiteViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 4/2/15.
//
//

#import "SiteViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

@interface SiteViewController ()

@end

@implementation SiteViewController

@synthesize urlString,title,type;

#pragma mark -
#pragma mark Application Lifecycle

- (void)loadView
{
    
    UIView *contentView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    contentView.autoresizesSubviews = YES;
    self.view = contentView;

    
    }

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ Screen/Iphone ",title ]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    whirl = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    whirl.frame = CGRectMake(self.view.frame.size.width/2 -10, self.view.frame.size.height/2 -10, 20.0, 20.0);
   whirl.center = self.view.center;
    
    //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView: whirl];
   // self.navigationController.navigationBarHidden =YES;
     if (type!=3) {
    [self.navigationController setToolbarHidden:NO animated:YES];
     }
    [self.view addSubview:whirl];
    
    
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =title;
    
   
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    
    
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
   // first_title.text=NSLocalizedString([sdic objectForKey:@"name"], @"");
  //  [_header_view addSubview:second_image];
    [_header_view addSubview:header_image];
  //  [_header_view addSubview:first_title];
    [ _header_view  addSubview:titleLabel];
    //set the web frame size
    CGRect webFrame = [[UIScreen mainScreen] applicationFrame];
    webFrame.origin.y = 0;
    
    if (type==2) {
         theWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,webFrame.size.height)];
         theWebView.scalesPageToFit = YES;
    }
    else if(type==3){
           [self.view addSubview:_header_view];
        theWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, _header_view.frame.size.height-30, self.view.frame.size.width,webFrame.size.height)];
        theWebView.scrollView.scrollEnabled = NO;
        theWebView.scrollView.bounces = NO;
         theWebView.scalesPageToFit = NO;
     
    }
    else{
     theWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, _header_view.frame.size.height, self.view.frame.size.width,webFrame.size.height-(_header_view.frame.size.height))];
    [self.view addSubview:_header_view];
         theWebView.scalesPageToFit = YES;
    }
    
 
    
    //add the web view
   
    
    theWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
   
    theWebView.delegate = self;
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    [theWebView loadRequest:req];
    if (type!=3) {
           [self.view addSubview: theWebView];
    }
    else{
        [self.view insertSubview:theWebView belowSubview:_header_view];
    }
 

    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
   // [self.view addSubview:_footer_view];
   
        [self updateToolbar];

    
}

-(void)updateToolbar {
    
    UIBarButtonItem *backButton =	[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backIcon.png"] style:UIBarButtonItemStylePlain target:theWebView action:@selector(goBack)];
    UIBarButtonItem *forwardButton =	[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"forwardIcon.png"] style:UIBarButtonItemStylePlain target:theWebView action:@selector(goForward)];
    
    [backButton setEnabled:theWebView.canGoBack];
    [forwardButton setEnabled:theWebView.canGoForward];
    
    UIBarButtonItem *refreshButton = nil;
    if (theWebView.loading) {
        refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:theWebView action:@selector(stopLoading)];
    } else {
        refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:theWebView action:@selector(reload)];
    }
    
    UIBarButtonItem *openButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareAction)];
    UIBarButtonItem *spacing       = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray *contents = [[NSArray alloc] initWithObjects:backButton, spacing, forwardButton, spacing, spacing, spacing, openButton, nil];
    //NSArray *contents = [[NSArray alloc] initWithObjects:backButton, spacing, forwardButton, spacing, refreshButton, spacing, openButton, nil];
        
    [self setToolbarItems:contents animated:NO];
    
    
    
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
        
        // [backButton release];
        
    }
    
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
    
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return ((interfaceOrientation == UIInterfaceOrientationPortrait) | UIInterfaceOrientationLandscapeLeft);
}

#pragma mark UIWebView delegate methods

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    return YES;
}

- (void) webViewDidStartLoad: (UIWebView * ) webView {
    [whirl startAnimating];
    [self updateToolbar];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"%@",webView.request.URL.absoluteString);
    [self updateToolbar];
    self.title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    [whirl stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self updateToolbar];
    [whirl stopAnimating];
    
    //handle error
}

#pragma mark -
#pragma mark ActionSheet methods

- (void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0 && theWebView.request.URL != nil) {
        [[UIApplication sharedApplication] openURL:theWebView.request.URL];
    }
}

- (void)shareAction {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                    cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Open in Safari", nil];
    
    [actionSheet showInView: self.view];
  
    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    //deallocate web view
    if (theWebView.loading)
        [theWebView stopLoading];
    
    theWebView.delegate = nil;
   
    theWebView = nil;
}

- (void)dealloc
{
    
   
    
    //make sure that it has stopped loading before deallocating
    if (theWebView.loading)
        [theWebView stopLoading];
    
    //deallocate web view
    theWebView.delegate = nil;
    
    theWebView = nil;
    
}


@end