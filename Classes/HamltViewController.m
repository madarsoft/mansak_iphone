//
//  HamltViewController.m
//  mutnav2
//
//  Created by waleed on 6/9/13.
//
//

#import "HamltViewController.h"

@interface HamltViewController ()

@end

@implementation HamltViewController
@synthesize bgImage,btnBg,leftView,myMapView,mainTable,dataArr,infoArr,titleLab;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,420*highf);
    [self.view addSubview:bgImage];
    leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 80, 270,330)];
    leftView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:leftView];
    
    UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 40*highf)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor greenColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"دليل الحملات";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [self.view addSubview:titleLabel];
    
    UIImageView *hh =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"headH.png"]];
    hh.frame=CGRectMake(0, 40, 320, 40*highf);
    [self.view addSubview:hh];
    
    titleLab = [[UILabel alloc]initWithFrame:CGRectMake(230, 42, 80, 36)];
    titleLab.text =@"برنامج اليوم";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.backgroundColor = [UIColor clearColor];
    titleLab.textAlignment = NSTextAlignmentCenter;
     titleLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [self.view addSubview:titleLab];
    
    
    UIImageView *rSide =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rightbg.png"]];
    rSide.frame=CGRectMake(270, 80, 50, 330*highf);
    [self.view addSubview:rSide];
    
    btnBg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"btnBg.png"]];
    btnBg.frame=CGRectMake(270, 80, 50, 66*highf);
    [self.view addSubview:btnBg];
    
    
    NSMutableArray *path =[[NSMutableArray alloc]initWithObjects:@"prog.png",@"map.png",@"not.png",@"ben.png",@"con.png", nil];
    for (int i=0; i<5; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setImage:[UIImage imageNamed:[path objectAtIndex:i]] forState:UIControlStateNormal];
        btn.frame = CGRectMake(275, 98+(66*i), 40, 30);
        btn.tag = i;
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
	// Do any additional setup after loading the view.
}
-(IBAction)btnClicked:(id)sender{
    int x = [sender tag];
    [btnBg setFrame:CGRectMake(270, 80+(66*x), 50, 66*highf)];
    
    for (UIView *v in [leftView subviews]) {
        [v removeFromSuperview];
    }
   
    if (x==0) {
        for (int i=0; i<5; i++) {
            dataArr = [[NSMutableArray alloc]init];
            UIView *vv = [self drawItemCell];
            [dataArr addObject:vv];
            
        }
        mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0,0,270,330) style:UITableViewStylePlain];
        mainTable.delegate = self;
        mainTable.dataSource = self;
        mainTable.autoresizesSubviews = YES;
        mainTable.backgroundColor=[UIColor clearColor];
        mainTable.rowHeight=75;
        //  mainTable.separatorColor = [UIColor blackColor];
        [self.leftView addSubview:self.mainTable];
    }
   else if (x==1) {
       [titleLab setText:@"الخريطة"];
        myMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, 270,330)];
        myMapView.delegate =self;
        myMapView.mapType = MKMapTypeStandard;
        [self.leftView addSubview:myMapView];
        [NSThread detachNewThreadSelector:@selector(displayMap) toTarget:self withObject:nil];

    }
   else if (x==2){
    [titleLab setText:@"التنبيهات"];
   }
   else if (x==3){
       [titleLab setText:@"المميزات"];
   }
   else if (x==4){
       [titleLab setText:@"الدليل"];
   }
    
}
-(NSMutableArray *)getData{
    infoArr = [[NSMutableArray alloc]init];
    return infoArr;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
       
}

-(void) displayMap{
    
//    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    CLLocationCoordinate2D location;
    span.latitudeDelta=0.02;
    span.longitudeDelta=0.02;
 
            location.latitude = 21.4130812;
            location.longitude = 39.8937178;
    
    
    
    region.span=span;
    region.center=location;
    [self setAnnotations];
    
    [myMapView setRegion:region animated:TRUE];
    [myMapView regionThatFits:region];
    
   // [pool release];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
}
-(void)setAnnotations{
    NSError *error;
    NSString *str = @"Mena";
    
    NSString *path=[[NSBundle mainBundle] pathForResource:str ofType:@"dat"];
    NSLog(@"%@",path);
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
    NSArray *allLines = [content componentsSeparatedByString: @"\n"];
    int count = [allLines count];
    NSLog(@"%i",count);
    for (int i=0; i<count-1; i++) {
        NSLog(@"---------%i",i);
        NSArray *items = [[allLines objectAtIndex:i] componentsSeparatedByString: @"|"];
        NSString *title = [[NSString alloc] initWithFormat:@"%@",[items objectAtIndex:0]];
        //     NSString *title = [NSString stringWithFormat:@"%@",[items objectAtIndex:0]];
        CLLocationCoordinate2D location;
        
        location.latitude =  [[items objectAtIndex:1]doubleValue];
        NSLog(@"%f", location.latitude);
        location.longitude = [[items objectAtIndex:2]doubleValue];
        NSLog(@"%f", location.longitude);
        // Add the annotation to our map view
        MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:title andCoordinate:location];
        [self.myMapView addAnnotation:newAnnotation];
//[newAnnotation release];
        //   [title release];
    }
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 5;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = nil;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
       
    }
    else
    {
        // the cell is being recycled, remove old embedded controls
        UIView *viewToRemove = nil;
        viewToRemove = [cell.contentView viewWithTag:kMyTag];
        if (viewToRemove)
            [viewToRemove removeFromSuperview];
    }
    UIView *  tmpView = [self drawItemCell];
    //     tmpView = [self.dataArr objectAtIndex:indexPath.row];
    
    [cell.contentView addSubview:tmpView];
    return cell;
}

-(UIView *)drawItemCell{
    CGFloat ff=0;
    CGRect frame = CGRectMake(0, 0, 270, 75);
    
    UIView *itemView = [[UIView alloc] initWithFrame:frame];
    itemView.backgroundColor = [UIColor clearColor];
    
    UIImageView *nameBg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"titHamBg.png"]];
    nameBg.frame= CGRectMake(0, ff, 270, 25);
    [itemView addSubview:nameBg];
    
    UILabel *nameLab = [[ UILabel alloc] initWithFrame:CGRectMake(120, ff, 100, 25)];
    nameLab.backgroundColor = [UIColor clearColor];
    nameLab.textColor = [UIColor blackColor];
    nameLab.textAlignment = NSTextAlignmentRight;
    nameLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    nameLab.text =@"حملة الإيمان";
    [itemView addSubview:nameLab];
   // [nameLab release];
    
    ff += 25;
    
    UILabel *townLab = [[ UILabel alloc] initWithFrame:CGRectMake(10, ff, 50, 20)];
    townLab.textColor = [UIColor blackColor];
    townLab.backgroundColor = [UIColor clearColor];
    townLab.textAlignment = NSTextAlignmentRight;
    townLab.text = @"8:30 ";
    townLab.font = [UIFont fontWithName:@"Arial" size:12];
    [itemView addSubview:townLab];
   // [townLab release];
    UILabel *townBg = [[ UILabel alloc] initWithFrame:CGRectMake(60, ff, 60, 20)];
    townBg.textColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1.0];
    townBg.backgroundColor = [UIColor clearColor];
    townBg.textAlignment =NSTextAlignmentRight;
    townBg.text = @"الساعة:";
    // timeLab.text = @"9:15  8:30";
    townBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:townBg];
    //[townBg release];
    
    
    UILabel *stateLab = [[ UILabel alloc] initWithFrame:CGRectMake(130, ff, 80, 20)];
    stateLab.textColor = [UIColor blackColor];
    stateLab.backgroundColor = [UIColor clearColor];
    stateLab.textAlignment = NSTextAlignmentRight;
    stateLab.text = @"أمام المخيم";
    stateLab.font = [UIFont fontWithName:@"Arial" size:12];
    [itemView addSubview:stateLab];
   // [stateLab release];
    UILabel *stateBg = [[ UILabel alloc] initWithFrame:CGRectMake(210, ff, 50, 20)];
    stateBg.textColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1.0];
    stateBg.backgroundColor = [UIColor clearColor];
    stateBg.textAlignment = NSTextAlignmentRight;
    stateBg.text = @"المكان:";
    // timeLab.text = @"9:15  8:30";
    stateBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:stateBg];
  //  [stateBg release];
    
    ff += 25;
    UILabel *distLab = [[ UILabel alloc] initWithFrame:CGRectMake(10, ff, 50, 20)];
    distLab.textColor = [UIColor blackColor];
    distLab.backgroundColor = [UIColor clearColor];
    distLab.textAlignment = NSTextAlignmentLeft;
    distLab.text = @"محمد علي";
    distLab.font = [UIFont fontWithName:@"Arial" size:12];
    [itemView addSubview:distLab];
  //  [distLab release];
    UILabel *distBg = [[ UILabel alloc] initWithFrame:CGRectMake(60, ff, 60, 20)];
    distBg.textColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1.0];
    distBg.backgroundColor = [UIColor clearColor];
    distBg.textAlignment = NSTextAlignmentRight;
    distBg.text = @"المسئول:";
    // timeLab.text = @"9:15  8:30";
    distBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:distBg];
  //  [distBg release];
    
    UILabel *trainLab = [[ UILabel alloc] initWithFrame:CGRectMake(130, ff, 60, 20)];
    trainLab.textColor = [UIColor blackColor];
    trainLab.backgroundColor = [UIColor clearColor];
    trainLab.textAlignment = NSTextAlignmentRight;
    trainLab.text = @"A";
    trainLab.font = [UIFont fontWithName:@"Arial" size:12];
    [itemView addSubview:trainLab];
   // [trainLab release];
    UILabel *trainBg = [[ UILabel alloc] initWithFrame:CGRectMake(200, ff, 60, 20)];
    trainBg.textColor = [UIColor colorWithRed:0.51 green:0.51 blue:0.51 alpha:1.0];
    trainBg.backgroundColor = [UIColor clearColor];
    trainBg.textAlignment = NSTextAlignmentRight;
    trainBg.text = @"المجموعة:";
    // timeLab.text = @"9:15  8:30";
    trainBg.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    [itemView addSubview:trainBg];
   // [trainBg release];
    
       
    return itemView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
