//
//  draw2D.m
//  GadawalV1
//
//  Created by walid nour on 11/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "draw2D.h"

@implementation draw2D

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    self.backgroundColor = [UIColor clearColor];
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 2.0);
    
       CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
   // CGContextMoveToPoint(context, self.frame.size.width - 45, self.frame.origin.y);
   // CGContextAddLineToPoint(context, self.frame.size.width - 45, self.frame.size.height);
    
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, self.frame.size.width, 0);

    
    CGContextStrokePath(context);
 
}


@end
