//
//  BuildingViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 2/15/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapViewAnnotation.h"
//#import <BugSense-iOS/BugSenseController.h>

@interface BuildingViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (retain, nonatomic) IBOutlet MKMapView *myMap;

@property (nonatomic, retain) NSString *myId;

@property (nonatomic, retain) NSString *myName ;
@property (nonatomic, retain) NSString *grade ;

@property (nonatomic, retain) NSMutableArray *info;
@property (nonatomic, retain) NSMutableArray *infolabels;
@property (nonatomic, retain) NSMutableArray *services;

@property (nonatomic, retain) NSMutableArray *room;
@property (nonatomic, retain) NSMutableArray *build;
@property (nonatomic, retain) NSMutableArray *buildLabels;
@property (nonatomic, retain) NSMutableArray *gradeLabels;

@property int status;



-(void) getbuildingDetails;

-(void) displayMap;

-(IBAction)mapBtnPressed:(id)sender;

-(IBAction)contactPressed:(id)sender;

@end
