//
//  webviewcontroller.h
//  gadaweltest
//
//  Created by amr on 9/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.

#import <UIKit/UIKit.h>
#import "Global.h"
@class Reachability;

@interface webviewcontroller : UIViewController<UIWebViewDelegate> {
	IBOutlet UIWebView *webDisplayiPad;
	NSString *mypage;
	double viewwidth;
IBOutlet	UIView *myview;
	IBOutlet	UIView *mypreloadingview;
	IBOutlet	UIWebView *mywebview;
	IBOutlet UILabel *lblpreloading;
	IBOutlet	UIActivityIndicatorView *imgpreloading;
	
	//Reachability* internetReachable;
	//Reachability* hostReachable;
}
@property(nonatomic,strong) UIWebView *webDisplayiPad;
@property(nonatomic,strong)UIView *myview;
@property(nonatomic,strong)UIView *mypreloadingview;
@property(nonatomic,strong)NSString *mypage;
@property(nonatomic,strong)IBOutlet	UIWebView *mywebview;
@property double viewwidth;
@property(nonatomic,strong)IBOutlet	UILabel *lblpreloading;
@property(nonatomic,strong)IBOutlet	UIActivityIndicatorView *imgpreloading;
//-(void) checkNetworkStatus:(NSNotification *)notice;
@property int type;
@property (weak, nonatomic) IBOutlet UIToolbar *webToolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backWebView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardWebView;
@property (weak, nonatomic) NSString *urlStr;
- (IBAction)goBack:(id)sender;

@end
