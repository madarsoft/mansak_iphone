//
//  imageviewcontroller.m
//  mutnav2
//
//  Created by amr on 1/14/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "imageviewcontroller.h"


@implementation imageviewcontroller
@synthesize myimg,sdic,myid,plist,scrollView;

CGFloat previousScale;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
	myid=[sdic objectForKey:@"id"];
	NSString *img=[[NSString alloc] initWithFormat:@"%@%@",plist,myid];
	
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =@"صور توضيحية";
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text=[sdic objectForKey:@"name"];
    [_header_view addSubview:second_image];
    [_header_view addSubview:header_image];
    [_header_view addSubview:first_title];
    [ _header_view  addSubview:titleLabel];
    
    myimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 460)];
    myimg.image=[UIImage imageNamed:img];
    
   // scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, 460)];
    scrollView =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 90, 320, 460)];
    [scrollView setBackgroundColor:[UIColor clearColor]];
	[scrollView setCanCancelContentTouches:NO];
	scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	scrollView.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
	scrollView.scrollEnabled = YES;

    [self.scrollView addSubview:myimg];
    [self.view addSubview:scrollView];
    self.view.clipsToBounds = YES;
    
    
    
      //  [scrollView setContentSize:CGSizeMake(600, 600)];
      [scrollView setContentSize:myimg.frame.size];
    
    [self.view addSubview:_header_view];
    
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
    
    UIPinchGestureRecognizer *pinchTap = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    
    [scrollView addGestureRecognizer:pinchTap];
    [pinchTap release];

	
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer{
    if([recognizer state] == UIGestureRecognizerStateEnded) {
        previousScale = 1.0;
        return;
    }
    CGFloat newScale = 1.0 - (previousScale - [recognizer scale]);
   // CGAffineTransform currentTransformation = self.scrollView.transform;
     CGAffineTransform currentTransformation = self.myimg.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransformation, newScale, newScale);
        self.myimg.transform = newTransform;
    self.scrollView.transform = newTransform;
  [scrollView setContentSize:myimg.frame.size];
    
    previousScale = [recognizer scale];
//

}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
        
       // [backButton release];
        
    }
    
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[myimg release];
	[myid release];
	[plist release];
	[sdic release];
}


@end
