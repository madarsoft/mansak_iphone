//
//  CalCellView.h
//  Almosaly
//
//  Created by Sara Ali on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CalCellView;

@protocol CalCellViewDelegate <NSObject>

-(void)didSelectCalCellView:(CalCellView*)me;

@end

@interface CalCellView : UIView {
    UILabel* meladyLabel;
    UILabel* hjriLabel;
    UILabel* dayLabel;
    
    BOOL _dotted;
}

- (void)drawCell;
// public api to set the 2 labels
- (void) setMelady:(NSString*)melady andHijri:(NSString*)hijri;
- (void) setHighlited:(BOOL)h;

-(void) setDotted:(BOOL)d;

@property (nonatomic,strong) UILabel* meladyLabel;
@property (nonatomic,strong) UILabel* hjriLabel;
@property (nonatomic,strong) UILabel* dayLabel;
@property BOOL drawCellRect;
@property (strong) NSDate* date;
@property BOOL dotted;

@property (strong) UIColor* highliteColor;

@property (weak) id<CalCellViewDelegate> delegate;
@end
