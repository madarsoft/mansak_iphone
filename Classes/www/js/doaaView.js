function doaaView (){
    this.doaaController = new doaaController();
}


doaaView.prototype.showDoaa=function (DoaaList){
   var DoaaListLength = DoaaList.length;    
    for (var i = 0 ; i < DoaaListLength ; i++){    
    $('#doaas').append("<div class='doaa2'><p class='mansak'>"+DoaaList[i].text+"</p><p class='number doaaSource'>"+DoaaList[i].source+"</p></div>")    
             this.initializeFontSize();
    }
}

doaaView.prototype.initializeFontSize=function(){ 
    
var initialFontSize = parseInt(this.getFontSize());
$('.mansak').css('font-size', initialFontSize);
$('.ayat').css('font-size', initialFontSize);
$('.number').css('font-size', initialFontSize);   
$('.sub_title').css('font-size', initialFontSize + 2 );
$('.main_title').css('font-size', initialFontSize + 4);
$('.doaa2').css('font-size', initialFontSize);
$('.paragraph').css('font-size', initialFontSize);
$('.header').css('font-size', initialFontSize);

    }

doaaView.prototype.getFontSize=function(){
var fontSize = localStorage.getItem('fontsize');
    return (fontSize);
}

doaaView.prototype.setFontSize=function(newFontSize){
localStorage.setItem('fontsize',newFontSize);
}



doaaView.prototype.inc=function (){	
var myobj = this;    
var oldFontSize =myobj.getFontSize(); 
    var f1 = parseInt(oldFontSize) + 2
  if(f1>30){
    f1 = 30; 
    }    
myobj.setFontSize(f1);
		  if(f1<=30){		
				$('.mansak').css('font-size', f1);
				$('.number').css('font-size', f1);
              $('.paragraph').css('font-size', f1);
               $('.header').css('font-size', f1);
			   $('.ayat').css('font-size', f1);
			   
		  }
		var f2= parseInt($('.sub_title').css('font-size')) + 2;
			if(f2<=32){
				$('.sub_title').css('font-size', f2);
			}		  
			var f3= parseInt($('.main_title').css('font-size')) + 2;
			if(f3<=34){			
				$('.main_title').css('font-size', f3);
			}
    
    window.location  = 'ios:webToNativeCall';
	JSInterface.setsize_android(f1);
}


doaaView.prototype.dec=function (){	   
    var myobj = this;    
    var oldFontSize =myobj.getFontSize(); 
    var f1 = parseInt(oldFontSize - 2)
    if(f1<8){
    f1 = 8;    
    }    
myobj.setFontSize(f1);
	  if(f1>=8){	
			$('.mansak').css('font-size', f1);
			$('.number').css('font-size', f1);
           $('.paragraph').css('font-size', f1);
           $('.header').css('font-size', f1);
		    $('.ayat').css('font-size', f1);

	  } 
	   var  f2= parseInt($('.sub_title').css('font-size')) - 2;
	  if(f2>=10){	
			$('.sub_title').css('font-size', f2);
	  }	  
	  var  f3= parseInt($('.main_title').css('font-size')) - 2;
	  if(f3>=12){	
			$('.main_title').css('font-size', f3);
	  }
    
    window.location  = 'ios:webToNativeCall';
	JSInterface.setsize_android(f1);
}


$(function(){    
    var doaa_view = new doaaView();
    
    if (localStorage.getItem('fontsize')==null){
    localStorage.setItem('fontsize',14);
    }else{doaa_view.initializeFontSize()}

	$('#name3').click(function(e) {
		 $('#doaas').html('');                
         doaa_view.doaaController.retrieveFromXML();        
        doaa_view.initializeFontSize(); 
    });	
	
	$('.increase').click(function(e) {
        
         doaa_view.inc();
    });
	
	$('.decrease').click(function(e) {
         doaa_view.dec();
    });
    
    
	
    
});