//
//  TextCellView.h
//  Almosaly
//
//  Created by Sara Ali on 2/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TextCellView : UIView {
    
    
    UILabel* textLabel;
    NSString* txt;
}
//public api
@property (nonatomic,strong) UILabel* textLabel;
@property BOOL drawCellRect;
- (void)setText:(NSString*)text;
@end
