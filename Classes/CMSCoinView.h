//
//  CMSCoinView.h
//  FlipViewTest
//
//  Created by Rebekah Claypool on 10/1/13.
//  Copyright (c) 2013 Coffee Bean Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "custom_pagecontrol.h"
@interface CMSCoinView : UIView<UIWebViewDelegate,UIGestureRecognizerDelegate,UIScrollViewDelegate>{
    int check;
    CGRect sender_btn;
    NSMutableArray *manasik_array;
    NSMutableArray *manasik_days;
    int current_index;
    int current_day;
    
    NSString *mansak_plist_file;
}
@property (strong, nonatomic) custom_pagecontrol *pageControl;
@property (strong, nonatomic) UIView *footer_view;
@property (nonatomic, strong) UIScrollView* scrollView;
@property (nonatomic, strong) UIImageView *view1;
@property (nonatomic, strong) UIImageView *view2;
@property (nonatomic, strong) UIWebView *web_content;
@property (nonatomic, strong) UIButton *back_btn;
@property (nonatomic, strong) UILabel *title_lbl;
@property (nonatomic, strong) UILabel *title_lbl_right;
@property (nonatomic, strong) UIImageView *title_image_right;
@property (nonatomic, strong) UIView *header_view_inner;

-(void)doTransitionWithType:(UIViewAnimationTransition)animationTransitionType header:(UIView *)header_view sender_frame:(CGRect )sender_frame;
-(void)setUpView :(id)target plist_name:(NSString *)plist_file mansak_id:(NSInteger)mansak_id mansak_day:(NSInteger)mansak_day;
-(void)close;
@end
