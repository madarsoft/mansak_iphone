//
//  HamalListViewController.h
//  mutnav2
//
//  Created by waleed on 6/4/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#define kMyTag 0
@interface HamalListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (nonatomic, retain) NSMutableArray *dataArr ;
@property (nonatomic, retain) NSMutableArray *infoArr ;

-(UIView *)drawItemCell ;
-(NSMutableArray *)getData;


@end
