//
//  NMViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 7/7/14.
//
//

#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "prayertimecalculator.h"
//#import "date.h"

@class RXMLElement;

@interface NMViewController : UIViewController<UIGestureRecognizerDelegate>

@property (nonatomic,retain)  NSArray *prayerTimes;
@property (nonatomic,retain)  NSArray *prayTimes;
@property (retain, nonatomic) IBOutlet UIImageView *backImage;
@property (retain, nonatomic) NSMutableArray *tawef;
@property (retain, nonatomic) NSMutableArray *saai;
@property (retain, nonatomic) NSMutableArray *crowd;


@property (nonatomic,retain) IBOutlet UIView *closeView;
@property (nonatomic,retain) IBOutlet UIView *openView;
@property (nonatomic,retain) IBOutlet UIView *floorView;
@property (nonatomic,retain) IBOutlet UIView *crowdView;
@property (nonatomic,retain) IBOutlet UILabel *leftLab;
@property (nonatomic,retain) IBOutlet UILabel *centLab;
@property (nonatomic,retain) IBOutlet UILabel *rightLab;
@property int viewId;
@property int floorId;
@property int crowdId;



-(NSString *) getMacTemp;
-(void) drawHeader;
-(void) drawTimePlayers;
-(void) drawFooter;
-(void) drawTemp;
-(void) getCrowd;
-(void) loadFoot;
-(int)  getPrayerID;
-(IBAction)moveView:(id)sender;




@end
