//
//  GridViewController.m
//  Almosaly
//
//  Created by Sara Ali on 2/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GridViewController.h"
#import "GridView.h"
#import "CalendarBusiness.h"
#import "HijriTime.h"



#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

static NSArray* corrOptions = [NSArray arrayWithObjects:@"-4",@"-3",@"-2",@"-1",@"0",@"1",@"2",@"3",@"4" , nil];


@implementation GridViewController
@synthesize correctionPicker ,  containerView , selectedCell;
@synthesize timeDif,firstDayflag;


// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
    
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat width= CGRectGetWidth(screen);
    CGFloat height=CGRectGetHeight(screen);
    float xRatio=width/320;
    float yRatio=height/480;
    //float yRatio=1;
    offset = 0 ;
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *correctTime= nil;
    
    if (standardUserDefaults)
        correctTime = [standardUserDefaults objectForKey:@"correctTime"];
    timeDif = [correctTime intValue];
    UIView *myView = [[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]] autorelease];
    self.view = myView;
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =NSLocalizedString(@"hijriDate", @"tare5 hijri");
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text=NSLocalizedString(@"hjriDateCore", @"tas7e7 elTare5");
    [_header_view addSubview:second_image];
    [_header_view addSubview:header_image];
    [_header_view addSubview:first_title];
    [ _header_view  addSubview:titleLabel];
    
    
    Date melady_today;
    meladyyear_number = melady_today.year();
    meladymonth_number = melady_today.month();
    
    // hijri date label
    CGRect textFrame;
    UIFont* txtFont=[UIFont fontWithName:@"Arial-BoldMT" size:14];
    textFrame = CGRectMake(215*xRatio,130*yRatio, 100*xRatio, 20*yRatio);
    hegryDateLabel =  [[UILabel alloc] initWithFrame:textFrame];
    NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
    
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hijridate objectAtIndex:i] intValue];
    
    month_number = hegryToday[1];
    year_number = hegryToday[2];
    
    NSDateComponents *meladyTemp = [[NSDateComponents alloc] init];
    [meladyTemp setYear:hegryToday[2]];
    [meladyTemp setMonth:hegryToday[1]];
    [meladyTemp setDay:hegryToday[0]];
    
    NSCalendar *gregorian11 = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDate *hegryday = [gregorian11 dateFromComponents:meladyTemp];
    NSDateFormatter *dateFormat1= [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat1 setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString_first1 = [dateFormat1 stringFromDate:hegryday];
    
    //set today's date in hijri format
    hegryDateLabel.text = dateString_first1;
    hegryDateLabel.font = txtFont;
    hegryDateLabel.textAlignment = NSTextAlignmentCenter;
    hegryDateLabel.textColor = [UIColor whiteColor];
    hegryDateLabel.backgroundColor = [UIColor colorWithRed:0.318 green:0.318 blue:0.318 alpha:1];
    
    
    // melady date label
    CGRect textFrame1;
    UIFont* txtFont1= [UIFont boldSystemFontOfSize:17];
    textFrame1 = CGRectMake(5*xRatio,130*yRatio, 100*xRatio, 20*yRatio);
    meladyDateLabel =  [[UILabel alloc] initWithFrame:textFrame1];
    
    //set today's date in melady format
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSString *dateString_first = [dateFormat stringFromDate:melady_today.date];
    
    meladyDateLabel.text = dateString_first;
    meladyDateLabel.font = txtFont1;
    meladyDateLabel.textAlignment = NSTextAlignmentCenter;
    meladyDateLabel.textColor = [UIColor whiteColor];
    meladyDateLabel.backgroundColor =[UIColor colorWithRed:0.318 green:0.318 blue:0.318 alpha:1];
    
    
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(40*xRatio,140*yRatio, 240*xRatio, 60*yRatio)];
    bgView.backgroundColor = [UIColor colorWithRed:0.318 green:0.318 blue:0.318 alpha:1];
    
    // hegry month name label
    CGRect textFrame2;
    UIFont* txtFont2= [UIFont fontWithName:@"Arial-BoldMT" size:18];
    textFrame2 = CGRectMake(100*xRatio,140*yRatio, 100*xRatio, 30*yRatio);
    hegryMonthLabel =  [[UILabel alloc] initWithFrame:textFrame2];
    NSString* month_name = [HijriTime getArabicMonth:month_number];
    hegryMonthLabel.text = NSLocalizedString(month_name, @"") ;
    hegryMonthLabel.font = txtFont2;
    hegryMonthLabel.textAlignment = NSTextAlignmentRight;
    hegryMonthLabel.textColor = [UIColor whiteColor];
    hegryMonthLabel.backgroundColor = [UIColor clearColor];
    hegryMonthLabel.adjustsFontSizeToFitWidth = YES;
    
    //hegry year label
    CGRect textFrame3;
    UIFont* txtFont3= [UIFont systemFontOfSize:14];
    textFrame3 = CGRectMake(80*xRatio,142*yRatio, 40*xRatio, 26*yRatio);
    hegryYearLabel=  [[UILabel alloc] initWithFrame:textFrame3];
    
    NSString* year=[NSString stringWithFormat:@"%ld",(long)year_number];
    hegryYearLabel.text = year;
    hegryYearLabel.font = txtFont3;
    hegryYearLabel.textAlignment = NSTextAlignmentLeft;
    hegryYearLabel.textColor = [UIColor whiteColor];
    hegryYearLabel.backgroundColor = [UIColor clearColor];
    
    
    
    // melady month name label
    CGRect textFrame4;
    UIFont* txtFont4= [UIFont fontWithName:@"Arial-BoldMT" size:16];
    textFrame4 = CGRectMake(160*xRatio,140*yRatio, 100*xRatio, 30*yRatio);
    meladyMonthLabel=  [[[UILabel alloc] initWithFrame:textFrame4] retain];
    NSString* meladymonth_year=[NSString stringWithFormat:@"%@",[HijriTime getGeogorianMonth:meladymonth_number] ];
    meladyMonthLabel.text = meladymonth_year;
    meladyMonthLabel.font = txtFont4;
    meladyMonthLabel.textAlignment = NSTextAlignmentCenter;
    meladyMonthLabel.textColor = [UIColor whiteColor];
    meladyMonthLabel.backgroundColor =[UIColor colorWithRed:0.318 green:0.318 blue:0.318 alpha:1];
    
    
    //melady year label
    CGRect textFrame5;
    UIFont* txtFont5= [UIFont boldSystemFontOfSize:14];
    textFrame5 = CGRectMake(80*xRatio,122*yRatio, 50*xRatio, 20*yRatio);
    meladyYearLabel =  [[[UILabel alloc] initWithFrame:textFrame5] retain];
    NSString* melady_year=[NSString stringWithFormat:@"%ld",(long)meladyyear_number];
    meladyYearLabel.text =  melady_year;
    meladyYearLabel.font = txtFont5;
    meladyYearLabel.textAlignment = NSTextAlignmentCenter;
    meladyYearLabel.textColor = [UIColor whiteColor];
    meladyYearLabel.backgroundColor = [UIColor colorWithRed:0.318 green:0.318 blue:0.318 alpha:1];
    
    if(self.mode)
    {
        hegryMonthLabel.text = meladyMonthLabel.text;
        hegryYearLabel.text = meladyYearLabel.text;
    }
    
    //forward button
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(45*xRatio,130*yRatio, 13,20);
    UIImage *img = [UIImage imageNamed:@"calLeft.png"];
    [button setImage:img forState:UIControlStateNormal];
    //listen for clicks
    [button addTarget:self action:@selector(forwardButtonPressed)
     forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    //backward button
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.frame = CGRectMake(260*xRatio,130*yRatio, 13,20);
    UIImage *img1 = [UIImage imageNamed:@"calRight.png"];
    [button1 setImage:img1 forState:UIControlStateNormal];
    
    //listen for clicks
    [button1 addTarget:self action:@selector(backwardButtonPressed)
      forControlEvents:UIControlEventTouchUpInside];
    
    //gridview
    CGRect frame = CGRectMake(40*xRatio, 200*yRatio, 240*xRatio,166*yRatio);
    calGrid =[[GridView alloc]initWithFrame:frame];
    calGrid.backgroundColor = [UIColor colorWithRed:0.886 green:0.878 blue:0.882 alpha:1];
    calGrid.opaque = NO;
    
    //daysgridview
    CGRect frameTitle = CGRectMake(40*xRatio, 170*yRatio, 240*xRatio,30*yRatio);
    daysGrid =[[GridView alloc]initWithFrame:frameTitle];
    daysGrid.backgroundColor = [UIColor clearColor];
    daysGrid.opaque = NO;
    // set viewarray
    
    int todayCellIndex;
    NSMutableArray* container = nil;
    if(!self.mode)
        container = [CalendarBusiness hegryCalendar:offset TodayCellIndex:&todayCellIndex];
    else
        container = [CalendarBusiness meladyCalendar:offset TodayCellIndex:&todayCellIndex];
    
    
    
    
    NSMutableArray* hegryCal = [container objectAtIndex:0];
    NSMutableArray* meladCal = [container objectAtIndex:1];
    NSArray* datesArray = [container objectAtIndex:2];
    
    NSMutableArray* myCells = [NSMutableArray arrayWithCapacity:42];
    for( int x = 0 ; x < [hegryCal count] ; x++ ) // 7 * 5 = 35 cell
    {
        // I choose here the CalCellView class to make my cells ( any other UIView child class can be used)
        CalCellView* tempCell = [[CalCellView alloc] init];
        tempCell.delegate = self;
        NSString* hcell = [hegryCal objectAtIndex:x];
        if ([hcell isEqualToString:@"1"]) {
            firstDayflag = x;
        }
        
        NSString* mcell = [meladCal objectAtIndex:x];
        
        if(self.mode)
            [tempCell setMelady:mcell andHijri:hcell];
        else
            [tempCell setMelady:hcell andHijri:hcell];
        
        [myCells addObject:tempCell]; //the array retain the object
        tempCell.drawCellRect=NO;
        
        tempCell.date = [datesArray objectAtIndex:x];
        
        
        if(x == todayCellIndex)
        {
            [tempCell setBackgroundColor:[UIColor colorWithRed:0.153 green:0.714 blue:0.706 alpha:1]] ;
            tempCell.highliteColor = [UIColor whiteColor];
            [tempCell setHighlited:YES];
        }
        
        [tempCell release];
        
    }
    
    dayNames = [[NSArray alloc] initWithObjects:@"friday",@"tuesday",@"wednesday",@"thursday",@"monday",@"sunday",@"saturday",nil];
    
    // make the title week days cells
    NSMutableArray* weekCells = [NSMutableArray arrayWithCapacity:7];
    for( int x = 0 ; x < 7 ; x++ )
    {
        // I choose here the CalCellView class to make my cells ( any other UIView child class can be used)
        TextCellView* tempCell = [[TextCellView alloc] init];
        [tempCell setText:NSLocalizedString([dayNames objectAtIndex:x], @"")];
        
        tempCell.textLabel.textColor = [UIColor colorWithRed:0.773 green:0.831 blue:0.2 alpha:1];
        tempCell.textLabel.font = [UIFont boldSystemFontOfSize:10];
        tempCell.drawCellRect=NO;
        [weekCells addObject:tempCell];
        [tempCell release];
    }
    
    //calendar grid
    calGrid.viewsArray = myCells;
    calGrid.leftToRight = NO;
    [calGrid.layer setCornerRadius:0.0f];
    [calGrid.layer setMasksToBounds:YES];
    
    if (firstDayflag==6) {
        [calGrid drawGrid:6 col:7];
    }
    else{
        [calGrid drawGrid:5 col:7];
    }
    
    //week days grid
    daysGrid.viewsArray = weekCells;
    [daysGrid.layer setCornerRadius:0.0f];
    [daysGrid.layer setMasksToBounds:YES];
    
    [daysGrid drawGrid:1 col:7];
    
    [self.view addSubview:_header_view];
    int diff = 2;
    difBtns =[[NSMutableArray alloc]init];
    difLabels =[[NSMutableArray alloc]init];
    
    for (int i=0;i<5;i++){
        UIButton *tmpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        tmpBtn.tag = i;
        tmpBtn.titleLabel.tag = diff;
        [tmpBtn setImage:[UIImage imageNamed:@"cal_03.png"] forState:UIControlStateNormal];
        if (diff==timeDif) {
            [tmpBtn setImage:[UIImage imageNamed:@"cal_06.png"] forState:UIControlStateNormal];
        }
        tmpBtn.frame = CGRectMake(80+(i*32), 370*yRatio, 25, 25) ;
        [tmpBtn addTarget:self action:@selector(difButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [difBtns addObject:tmpBtn];
        [self.view addSubview:tmpBtn];
        
        UILabel *tmpLabel = [[UILabel alloc]initWithFrame:CGRectMake(80+(i*30), 395*yRatio, 30, 20)];
        tmpLabel.backgroundColor = [UIColor clearColor];
        tmpLabel.text =[NSString stringWithFormat:@"%d",diff];
        tmpLabel.textColor = [UIColor colorWithRed:0.58 green:0.58 blue:0.58 alpha:1];
        if (diff==timeDif) {
            tmpLabel.textColor = [UIColor blackColor];
            
        }
        tmpLabel.textAlignment = NSTextAlignmentCenter;
        tmpLabel.tag = i;
        [difLabels addObject:tmpLabel];
        [self.view addSubview:tmpLabel];
        
        diff--;
        
    }
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(80, 415*yRatio, 160, 25) ;
    saveBtn.backgroundColor = [UIColor colorWithRed:0.149 green:0.714 blue:0.714 alpha:1];
    [saveBtn setTitle:NSLocalizedString(@"save", @"saveButton") forState:UIControlStateNormal];
    [saveBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(correctSavePressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:saveBtn];
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, (self.view.frame.size.height-30), self.view.frame.size.width*xRatio,49*yRatio)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
    [self.view addSubview:bgView];
    [self.view addSubview:hegryYearLabel];
    [self.view addSubview:hegryMonthLabel];
    [self.view addSubview:calGrid];
    [self.view addSubview:daysGrid];
    [self.view addSubview:titleLabel];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        [containerView setFrame:CGRectMake(0, 0, width, height/3 - 30)];
    }
    [self.view setFrame:CGRectMake(0, 0, width, height)];
    containerView.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [hegryDateLabel release];
    [meladyDateLabel release];
    [hegryMonthLabel release];
    [hegryYearLabel release];
    [meladyYearLabel release];
    [meladyMonthLabel release];
    [calGrid release];
    [daysGrid release];
}

-(void) viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame =CGRectMake(10, 20, 30, 30);
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    
    
}


-(void)popViewControllerWithAnimation {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *correctTime= nil;
    
    if (standardUserDefaults)
        correctTime = [standardUserDefaults objectForKey:@"correctTime"];
    
    int corTime =[correctTime intValue];
    
    if(timeDif != corTime){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"warning", @"")
                                                          message:NSLocalizedString(@"hejriCorMsg", @"")
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"no", @"")
                                                otherButtonTitles:NSLocalizedString(@"yes", @""), nil];
        [message performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        
        /*
         here missing -->[ message show]
         but we disable it now
         */
        return;
    }
    else{
        [self.view removeFromSuperview];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    
    
    if([title isEqualToString:@"نعم"]||[title isEqualToString:@"Yes"])
    {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        if (standardUserDefaults) {
            [standardUserDefaults setObject:[NSNumber numberWithInt:timeDif] forKey:@"correctTime"];
            [standardUserDefaults synchronize];
        }
        [self updateView];
        
    }
    [self.view removeFromSuperview];
    
}


-(void)forwardButtonPressed {
    
    offset= offset+1;
    month_number=month_number+1;
    meladymonth_number=meladymonth_number+1;
    //begin in new hegry year
    if (month_number>12) {
        offset= offset%12;
        month_number= 1;
        year_number = year_number+1;
    }
    
    if (meladymonth_number>12) {
        meladymonth_number= 1;
        meladyyear_number = meladyyear_number+1;
    }
    
    [self updateView];
    
}
-(IBAction) difButtonPressed:(id)sender{
    int x = (int)[sender tag];
    UIButton *bbtun = (UIButton *)sender;
    timeDif =(int) bbtun.titleLabel.tag;
    for(int i=0;i<5;i++){
        UIButton *tmpBtn = (UIButton *)[difBtns objectAtIndex:i];
        [tmpBtn setImage:[UIImage imageNamed:@"cal_03.png"] forState:UIControlStateNormal];
        if (i==x) {
            [tmpBtn setImage:[UIImage imageNamed:@"cal_06.png"] forState:UIControlStateNormal];
        }
        UILabel *tmpLabel = (UILabel *)[difLabels objectAtIndex:i];
        if (i==x) {
            [tmpLabel setTextColor:[UIColor blackColor]];
        }
        else{
            [tmpLabel setTextColor:[UIColor colorWithRed:0.58 green:0.58 blue:0.58 alpha:1]];
            
        }
        
        
    }
    
}


-(void)backwardButtonPressed {
    
    offset= offset-1;
    month_number=month_number-1;
    meladymonth_number=meladymonth_number-1;
    //for the last year
    if(month_number==0)
    {
        month_number=12;
        year_number = year_number-1;
    }
    
    if(meladymonth_number==0)
    {
        meladymonth_number=12;
        meladyyear_number = meladyyear_number-1;
    }
    [self updateView];
    
}


-(void) correctButtonPressed:(id) sender{
    
    if(!self.correctView)
        [[NSBundle mainBundle] loadNibNamed:@"CorrectDate" owner:self options:nil];
    
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat height=CGRectGetHeight(screen);
    float yRatio=height/480;
    
    CGRect rect = self.correctView.frame;
    rect.origin.y = 41*yRatio;
    self.correctView.frame = rect;
    
    [self.view addSubview:self.correctView];
    
}

-(IBAction) okButtonPressed:(id)sender
{
    containerView.hidden = YES;
    [self updateView];
}

-(IBAction) resetButtonPressed:(id)sender
{
    [correctionPicker selectRow:4 inComponent:0 animated:YES];
    containerView.hidden = YES;
    [self updateView];
}

-(void) updateView
{
    Date melady_today;
    meladyyear_number = melady_today.year();
    meladymonth_number = melady_today.month();
    
    NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
    
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hijridate objectAtIndex:i] intValue];
    
    month_number = hegryToday[1];
    year_number = hegryToday[2];
    
    NSString* month_name = [HijriTime getArabicMonth:month_number];
    hegryMonthLabel.text =NSLocalizedString(month_name, @"") ;
    
    
    NSString* year=[NSString stringWithFormat:@"%ld",(long)year_number];
    hegryYearLabel.text = year;
    
    NSString* meladymonth_year=[NSString stringWithFormat:@"%@", [HijriTime getGeogorianMonth:meladymonth_number]];
    meladyMonthLabel.text = meladymonth_year;
    
    NSString* melady_year=[NSString stringWithFormat:@"%ld",(long)meladyyear_number];
    meladyYearLabel.text =  melady_year;
    
    if(self.mode)
    {
        hegryMonthLabel.text = meladyMonthLabel.text;
        hegryYearLabel.text = meladyYearLabel.text;
    }
    
    int today;
    NSMutableArray* container = nil;
    if(!self.mode)
        container = [CalendarBusiness hegryCalendar:offset TodayCellIndex:&today];
    else
        container = [CalendarBusiness meladyCalendar:offset TodayCellIndex:&today];
    
    
    
    
    
    
    NSMutableArray* hegryCal = [container objectAtIndex:0];
    NSMutableArray* meladCal = [container objectAtIndex:1];
    NSArray* datesArray = [container objectAtIndex:2];
    
    NSMutableArray* myCells = [NSMutableArray arrayWithCapacity:35];
    for( int x = 0 ; x < [hegryCal count] ; x++ ) // 7 * 5 = 53 cell
    {
        // I choose here the CalCellView class to make my cells ( any other UIView child class can be used)
        CalCellView* tempCell = [[CalCellView alloc] init];
        tempCell.delegate = self;
        NSString* hcell = [hegryCal objectAtIndex:x];
        NSString* mcell = [meladCal objectAtIndex:x];
        if ([hcell isEqualToString:@"1"]) {
            firstDayflag = x;
        }
        if(self.mode)
            [tempCell setMelady:mcell andHijri:hcell];
        else
            [tempCell setMelady:hcell andHijri:hcell];
        tempCell.drawCellRect=NO;
        
        if(x < [datesArray count])
            tempCell.date = [datesArray objectAtIndex:x];
        [myCells addObject:tempCell]; //the array retain the object
        
        if(x == today)
        {
            [tempCell setBackgroundColor:[UIColor colorWithRed:0.153 green:0.714 blue:0.706 alpha:1] ];
            tempCell.highliteColor = [UIColor whiteColor];
            [tempCell setHighlited:YES];
        }
        
        [tempCell release];
        
    }
    
    
    // make the title week days cells
    NSMutableArray* weekCells = [NSMutableArray arrayWithCapacity:7];
    for( int x = 0 ; x < 7 ; x++ )
    {
        // I choose here the CalCellView class to make my cells ( any other UIView child class can be used)
        TextCellView* tempCell = [[TextCellView alloc] init];
        [tempCell setText:NSLocalizedString([dayNames objectAtIndex:x], @"")];
        tempCell.textLabel.textColor = [UIColor colorWithRed:0.773 green:0.831 blue:0.2 alpha:1];
        tempCell.textLabel.font = [UIFont boldSystemFontOfSize:10];
        tempCell.drawCellRect=NO;
        [weekCells addObject:tempCell];
        [tempCell release];
    }
    
    
    //calendar grid
    calGrid.viewsArray = myCells;
    if (firstDayflag==6) {
        [calGrid drawGrid:6 col:7];
    }
    else{
        [calGrid drawGrid:5 col:7];
    }

    //week days grid
    daysGrid.viewsArray = weekCells;
    [daysGrid drawGrid:1 col:7];
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 9;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


//called by the picker requesting a string for one row - will called iteratively untill the picker is full
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return [corrOptions objectAtIndex:row];
}



- (void)toogleType:(id)sender
{
    self.mode = !self.mode;
    UIButton* toggleButton = (UIButton *)self.navigationItem.leftBarButtonItem.customView;
    if(!self.mode)
    {
        
        [toggleButton setImage:[UIImage imageNamed:@"hegrySwich.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [toggleButton setImage:[UIImage imageNamed:@"calHilal.png"] forState:UIControlStateNormal];
    }
    [self updateView];
    [self.tableView reloadData];
}

#pragma mark CalCellDeelgate
-(void)didSelectCalCellView:(CalCellView*)me
{
    UIViewController* entryVC = [[[NSBundle mainBundle] loadNibNamed:@"CalEntryEditor" owner:self options:nil] objectAtIndex:0];
    self.entryModalViewController = entryVC;
    self.selectedCell = me;
    
    [self.view addSubview:self.entryModalViewController.view];
    Date date(me.date);
    int month = date.month();
    int year = date.year();
    
    NSString* dayS = [NSString stringWithFormat:@"%d %@ %d" , date.day() , [HijriTime getGeogorianMonth:month] , year];
    NSString* higriS = [NSString stringWithFormat:@"%@ %@ %@" , me.meladyLabel.text , self->hegryMonthLabel.text , self->hegryYearLabel.text];
    
    self.HijriEntryViewLable.text = higriS;
    self.meladyEntryViewLabel.text = dayS;
    
}


#pragma mark UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark ---
- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
    // Release any retained subviews of the main view.
    [self setEntryModalView:nil];
    [self setTimePicker:nil];
    [self setMeladyEntryViewLabel:nil];
    [self setHijriEntryViewLable:nil];
    [self setEntryNameTextField:nil];
    [self setEntryModalViewController:nil];
    [self setCorrectView:nil];
    [self setCorrectSlider:nil];
    [super viewDidUnload];
    
}


- (void)dealloc {
    [dayNames release];
    [correctionPicker release];
    [containerView release];
    [_EntryModalView release];
    [_timePicker release];
    [_meladyEntryViewLabel release];
    [_HijriEntryViewLable release];
    [_entryNameTextField release];
    [_entryModalViewController release];
    [selectedCell release];
    
    [_correctView release];
    [self.events release];
    [self.tableView release];
    [_correctSlider release];
    [self.currentEvents release];
    [super dealloc];
}


- (IBAction)correctBackPressed:(id)sender {
    
    [self.correctView removeFromSuperview];
}

- (IBAction)correctSavePressed:(id)sender {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:[NSNumber numberWithInt:timeDif] forKey:@"correctTime"];
        [standardUserDefaults synchronize];
    }
    [self updateView];
    
}

- (IBAction)correctSliderValueChanged:(id)sender {
    
    int value = rint( self.correctSlider.value);
    self.correctSlider.value = value;
}

- (IBAction)checkMarkAction:(id)sender {
    
    if(![self.entryNameTextField.text length])
    {
        UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error !!", nil) message:NSLocalizedString(@"Missing required data!", nil ) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil] autorelease];
        
        [alert show];
        
        return;
    }
    self.selectedCell.highliteColor = [UIColor magentaColor];
    [self.selectedCell setHighlited:YES];
    
    
    Date wantedDate(self.timePicker.date);
    Date cellDate(self.selectedCell.date);
    
    wantedDate.setDay(cellDate.day());
    wantedDate.setMonth(cellDate.month());
    wantedDate.setYear(cellDate.year());
    [self.entryModalViewController.view removeFromSuperview];
    
}

- (IBAction)deleteAction:(id)sender {
    
    
    [self.selectedCell setHighlited:NO];
    Date wantedDate(self.timePicker.date);
    Date cellDate(self.selectedCell.date);
    
    wantedDate.setDay(cellDate.day());
    wantedDate.setMonth(cellDate.month());
    wantedDate.setYear(cellDate.year());
    [self.entryModalViewController.view removeFromSuperview];
    
}


//tableview configuration
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.currentEvents count];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray* objs = [[NSBundle mainBundle] loadNibNamed:@"CalTableCell" owner:nil options:nil];
        cell = [objs objectAtIndex:0];
    }
    
    
    // to display contact's  name
    
    NSDictionary* event = [self.currentEvents objectAtIndex:indexPath.row];
    
    NSString *event_name = [event objectForKey:@"name"];
    
    UILabel* name = (UILabel*)[cell viewWithTag:2];
    name.text = event_name;
    
    if(!self.mode)
    {
        UILabel* day = (UILabel*)[cell viewWithTag:1];
        day.text = [NSString stringWithFormat:@"%@" , [event objectForKey:@"higryDay"]  ];
        
        
        int dayh =[day.text intValue];
        int monthh = [[event objectForKey:@"higryMonth"] intValue];
        int yearh = (int)year_number;
        
        
        
        UILabel* meladyLabel = (UILabel*)[cell viewWithTag:3];
        NSMutableArray* melady = [HijriTime getMelady:dayh month:monthh year:yearh];
        NSString* monthString = [HijriTime getGeogorianMonth:[[melady objectAtIndex:1] integerValue]];
        meladyLabel.text = [NSString stringWithFormat:@"%@ %@" , [melady objectAtIndex:0] , monthString];
    }
    else
    {
        int dayh = [[event objectForKey:@"higryDay"]  intValue];
        int monthh =[[event objectForKey:@"higryMonth"] intValue];
        int yearh = (int)year_number;
        NSMutableArray* melady = [HijriTime getMelady:dayh month:monthh year:yearh];
        
        UILabel* day = (UILabel*)[cell viewWithTag:1];
        day.text = [NSString stringWithFormat:@"%@" , [melady objectAtIndex:0]];
        
        NSString* monthString =NSLocalizedString( [HijriTime getArabicMonth:monthh], @"");
        UILabel* meladyLabel = (UILabel*)[cell viewWithTag:3];
        meladyLabel.text = [NSString stringWithFormat:@"%d %@" , dayh , monthString];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat height=CGRectGetHeight(screen);
    float yRatio=height/480;
    
    return 48*yRatio;
}

-(BOOL) isIslamicEventDay:(int)hDay month:(int)hmonth
{
    for(NSDictionary* event in self.events)
    {
        NSNumber* day = [event objectForKey:@"higryDay"];
        NSNumber* month = [event objectForKey:@"higryMonth"];
        
        if([day integerValue] == hDay && [month integerValue] == hmonth)
            return YES;
        
    }
    
    return NO;
}

-(void) getCurrentIslamicEvents
{
    NSMutableArray* array = [NSMutableArray array];
    for (NSDictionary* dic in self.events) {
        NSNumber* month = [dic objectForKey:@"higryMonth"];
        if([month integerValue] == month_number)
            [array addObject:dic];
    }
    
    self.currentEvents = array;
    [self.tableView reloadData];
}

@end
