//
//  myMath.h
//  location
//
//  Created by Radwa on ٢٣‏/٢‏/٢٠١١.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface myMath : NSObject {

}


+(double) fixAngle:(double) a;
+(double) fixHours:(double) a;
+(double) dSin: (double) DegreeAngle;    //degree Sin
+(double) dCos: (double) DegreeAngle;
+(double) dTan: (double) DegreeAngle;
+(double) dASin:(double) x;
+(double) dACos:(double) x; // degree arccos
+(double) dATan:(double) x;
+(double) dATan2:(double)y :(double)x;  // degree arctan2
+(double) cotan: (double) i;   // degree cot
+(double) dACot: (double) x; // degree arccot
+(double) roundTime:(double) time;
+(int) round:(double) input;
//normalize value to be in range 0....1
+(double) normalize:(double) x;
+(double)degreeToRadian:(double)d;
+(double)radiansToDegree:(double)r;
@end
