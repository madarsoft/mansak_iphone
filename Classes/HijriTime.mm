//
//  HijriTime.m
//  AlMosaly
//
//  Created by Sara Ali on 2/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HijriTime.h"


@implementation HijriTime


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

+ (NSMutableArray*) toHegry:(Date) melady1{
    
    //c++
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *correctTime= nil;
    
    if (standardUserDefaults)
        correctTime = [standardUserDefaults objectForKey:@"correctTime"];
    
    int diff = [correctTime intValue];
    
    melady1 = melady1.addDays(diff);
    
    NSInteger a,b;
    
    NSInteger day =   melady1.day();
    NSInteger month =   melady1.month();
    NSInteger year =   melady1.year();
    
    
    if (month <= 2) {
        a = month + 12;
        b = year - 1;
    } else {
        a = month;
        b = year;
    }
    
    NSInteger c = (NSInteger) b / 100;
    
    NSInteger d = (NSInteger) b / 400;
    NSInteger e = 2 - c + d;
    NSInteger o = (NSInteger) ((b + 4716) * 365.25);
    NSInteger z = (NSInteger) ((a + 1) * 30.6001);
    double h = day + z + o + e - 1524.5;
    double t = h - 1937806.5;
    NSInteger k = (NSInteger) ((t - 1) / 10631);
    double l = (t + 354) - 10631 * k;
    NSInteger m = (NSInteger) ((10985 - l) / 5316);
    NSInteger n = (NSInteger) ((50 * l) / 17719);
    NSInteger s = (NSInteger) (l / 5670);
    NSInteger f = (NSInteger) ((43 * l) / 15238);
    NSInteger kk = m * n + s * f;
    NSInteger ss = (NSInteger) ((30 - kk) / 15);
    NSInteger r = (NSInteger) ((17719 * kk) / 50);
    NSInteger sh = (NSInteger) (kk / 16);
    NSInteger teh = (NSInteger) ((15238 * kk) / 43);
    double th = l - ss * r - sh * teh + 29;
    
    NSInteger hugriMonthinit = (NSInteger) ((24 * th) / 709);
    NSNumber* hugriMonth = [NSNumber  numberWithInt:(int)hugriMonthinit];
    
    NSInteger gh = (NSInteger) ((709 * hugriMonthinit) / 24);
    
    NSInteger hugriDayinit = (NSInteger) (th - gh);
    NSNumber* hugriDay = [NSNumber numberWithInt:(int)hugriDayinit];
    
    NSInteger hugriYearinit=(NSInteger) (30 * k + kk - 30);
    NSNumber* hugriYear = [NSNumber numberWithInt:(int)hugriYearinit];
    
    NSMutableArray *hegryToday = [[NSMutableArray alloc] init];
    [hegryToday insertObject:hugriDay atIndex:0];
    [hegryToday insertObject:hugriMonth atIndex:1];
    [hegryToday insertObject:hugriYear atIndex:2];
    return hegryToday;
    
    
}


+ (NSMutableArray*) todayInHegry{
    
    Date today;
    
    NSMutableArray* todayhegry;
    
    todayhegry= [self toHegry:today];
    
    return todayhegry ;
}


+ (NSMutableArray*) getMelady:(NSInteger) D month:(NSInteger) M year:(NSInteger) Y{
    
    NSInteger  HD;
    NSInteger Z;
    NSInteger X;
    double  J;
    double JD;
    NSInteger H;
    NSInteger A = 0;
    NSInteger Melady_Month ;
    NSInteger  Melady_Year = 0;
    
    
    Z= (NSInteger) ((Y-1)*(354.3667) + 0.5);
    
    X=(NSInteger)((M-1)*(29.5305) + 0.5) + D;
    
    HD=Z+X;
    
    JD = HD + 1948437.5;
    
    J = JD + 0.5;
    
    H = (NSInteger) ((J - 1867216.25)/(36524.25));
    if (J<2299161)
    {
        A = (NSInteger) J ;
    }
    else  if ( J>2299161)
    {
        A = (NSInteger) (J + 1 + H  - (NSInteger) (H / 4));
    }
    
    NSInteger B = A + 1524;
    
    NSInteger C = (int) ((B-122.1)/(365.25));
    
    NSInteger E = (NSInteger) (365.25*C);
    
    NSInteger F = (NSInteger) ((B-E)/(30.6001));
    NSInteger Melady_Day = B - E - (NSInteger) ((30.6001)*(F));
    
    if  (F>13.5)
        Melady_Month = F - 13 ;
    else
        Melady_Month= F - 1 ;
    
    if ( Melady_Month>2.5)
        Melady_Year = C - 4716 ;
    
    else if  (Melady_Month<2.5)
        Melady_Year = C - 4715 ;
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSNumber *correctTime= nil;
    
    if (standardUserDefaults)
        correctTime = [standardUserDefaults objectForKey:@"correctTime"];
    
    int diff = [correctTime intValue];
    
    
    Date meladyTemp((int)Melady_Day ,(int)Melady_Month , (int)Melady_Year );
    meladyTemp = meladyTemp.addDays(-diff);
    
    Melady_Day = meladyTemp.day();
    Melady_Month = meladyTemp.month();
    Melady_Year = meladyTemp.year();
    
    NSNumber* meladyday =   [NSNumber numberWithInt:(int)Melady_Day];
    NSNumber* meladymonth = [NSNumber numberWithInt:(int)Melady_Month];
    NSNumber* meladyyear =  [NSNumber numberWithInt:(int)Melady_Year];
    
    
    NSMutableArray *meladyToday = [[NSMutableArray alloc] init];
    [meladyToday insertObject:meladyday atIndex:0];
    [meladyToday insertObject:meladymonth atIndex:1];
    [meladyToday insertObject:meladyyear atIndex:2];
    
    return meladyToday;
}


+ (NSString*) getArabicMonth:(NSInteger) index{
    NSArray* hegryMonthes = @[@"moharam",@"safar",@"rabe3Awl",@"rabe3Thany",@"gmadAwl",@"gmadA5r",@"ragb",@"sh3ban",@"ramadan",@"shwal",@"zoElK3eda",@"zoEl7ega"];
    
    NSString* month= [hegryMonthes objectAtIndex:index-1];
    return month;
    
}

+ (NSString*) getGeogorianMonth:(NSInteger) index{
    NSArray* hegryMonthes = [[NSArray alloc] initWithObjects: NSLocalizedString(@"January",@""),
                             NSLocalizedString(@"February",@""),
                             NSLocalizedString(@"March",@""),
                             NSLocalizedString(@"April",@""),
                             NSLocalizedString(@"May",@""),
                             NSLocalizedString(@"June",@""),
                             NSLocalizedString(@"July",@""),
                             NSLocalizedString(@"August",@""),
                             NSLocalizedString(@"September",@""),
                             NSLocalizedString(@"October",@""),
                             NSLocalizedString(@"November",@""),
                             NSLocalizedString(@"December",@""), nil];
    NSString* month= [hegryMonthes objectAtIndex:index-1];
    return month;
    
}


@end
