//
//  WebController.m
//  mutnav2
//
//  Created by waleed on 7/28/13.
//
//

#import "WebController.h"
#import "Reachability.h"

@interface WebController ()

@end

@implementation WebController
@synthesize type,page;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    page = [[UIWebView alloc]initWithFrame:CGRectMake(0, 30*highf,320, 400*highf)];
    [self.view addSubview:page];
    
   	
    NSString *urlAddress;
        urlAddress =[NSString stringWithFormat:@"http://api.madarsoft.com/mobileads_NFcqDqu/v1/pages/page.aspx?programid=6&name=wemutawef"] ;
    
	
    
	//Create a URL object.
	NSURL *url = [NSURL URLWithString:urlAddress];
	
	//URL Requst Object
	NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    Reachability *curReach = [[Reachability reachabilityForInternetConnection] retain];
    [curReach startNotifier];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    //NSLog(@"netstatus is %@",netStatus);
        
    if (netStatus !=NotReachable) {
        [self.page loadRequest:requestObj];
    }
    else {
		//[self afterconnectionfail];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"تنبيه"
                                                          message:@"لا يوجد اتصال بالإنترنت."
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
        
    }
    
	// Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    
        self.navigationController.navigationBarHidden=YES;
        UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
        headImg.frame=CGRectMake(0, 0, 320, 50*highf);
        [self.view addSubview:headImg];
        
        UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment=UITextAlignmentCenter;
        titleLabel.textColor = [UIColor greenColor];
        titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
                   titleLabel.text =@"من نحن";
        titleLabel.font = [UIFont boldSystemFontOfSize:18];
        titleLabel.lineBreakMode=UILineBreakModeWordWrap;
        titleLabel.numberOfLines=0;
        [self.view addSubview:titleLabel];

}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"web did started loading");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
	NSLog(@"web did finished loading");    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
