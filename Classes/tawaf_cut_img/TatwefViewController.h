//
//  TatwefViewController.h
//  mutnav2
//
//  Created by Shaima Magdi on 12/1/14.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface TatwefViewController : GAITrackedViewController{
    UIView *header_view;
    UIView *center_view;
    UIView *footer_view;
    UIView *counter_view;
    UIView *size_button_view;
    UIScrollView *scroll_text_view;
    UILabel *text_label;
    UILabel *view_title;
    UIImageView *circle_bg;
    UIImageView *counter_back_img;
    UIImageView *footer_back_img;
    UIImageView *center_image_tawaf;
    UIImageView *center_image_sa3i;
    UIImageView *tawaf_sa3i_image;
    UILabel *count_label;
    UIButton *minus_btn;
    UIButton *counter_btn;
    UIButton *manasik;
    UIButton *do3aa;
    UIButton *tawaaf;
    UIButton *sa3i;
    UIButton *right_btn;
    UIButton *left_btn;
    UIButton *font_button;
    UIButton *increase_font;
    UIButton *decrease_font;
    UILabel *manasikLabel;
    UILabel *do3aaLabel;
 
}

@property(nonatomic,strong) UIView *tawaf_sa3i_view;


@end
