//
//  SafartiViewController.m
//  mutnav2
//
//  Created by walid nour on 7/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SafartiViewController.h"

@interface SafartiViewController ()

@end

@implementation SafartiViewController
@synthesize htmlString,type,bgImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboDetBg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320, 400*highf);
    [self.view addSubview:bgImage];

    [self getDetails];
    [self drawView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
-(void)drawView{
    UIWebView *textWeb = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 35*highf, 320, 330*highf)]autorelease];
    textWeb.delegate= self;
    
    
    textWeb.autoresizingMask = YES;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    
    NSString *cssCode = @" \n"
    "body{font-family:Times New Roman,Times,serif;font-size:14px;line-height:17px;color:#000;direction:rtl;padding: 0px;margin: 0px;width:80%;margin-left:auto;margin-right:auto;background-color:transparent;text-align:justify;}\n"
    "h1 {color:#8b29ef;font-size:36px;}\n"
    "h3 {color:#8b29ef;font-size:24px;}\n"
    "h2 {color:#8b29ef;text-decoration:underline;}\n"
    "p  {margin:0px;padding: 5px 20px;font-size: 20px;line-height: 25px;}\n"
    "table{border:2px solid #000000;width:80%;margin: 0px auto;}\n"
    "ol,ul{font-size:18px;line-height:15px;}\n"
    "ul{list-style:disc;}\n"
    "ol ul ul,ul ul{list-style:circle;}\n"
    "ol li,ul li{line-height:30px;}\n"
    ".lbl{font-size:18px;line-height:15px;}\n"
    ".info_div{width:90%;margin:0 auto;text-align:center;line-height:20px;}\n"
    ".info_div table{border:none;}\n"
    ".info_div table tr{height:30px;}\n";
    
    NSString *htmlCode = [NSString stringWithFormat:@""
                          "<html> "
                          "     <head>"
                          "     <title></title> "
                          "     <style>%@</style></head>" //CSS
                          "     <body>%@</body>\n\n"
                          "</html>  \n",cssCode,[htmlString stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
    
    textWeb.opaque = NO;
    textWeb.backgroundColor = [UIColor clearColor];
    [textWeb loadHTMLString:htmlCode baseURL:baseURL];
    
    
    [self.view addSubview:textWeb];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden=YES;
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboHead.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 40*highf);
    [self.view addSubview:headImg];
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 270, 40*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=UITextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text =@"سفراتي";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.lineBreakMode=UILineBreakModeWordWrap;
    titleLabel.numberOfLines=0;
    [self.view addSubview:titleLabel];

    
}
-(void)getDetails{
    NSString *pathh = [[NSBundle mainBundle] pathForResource:@"a1" ofType:@"txt"];
	NSFileHandle *readHandle = [NSFileHandle fileHandleForReadingAtPath:pathh];
    
	htmlString = [[NSString alloc] initWithData: 
                            [readHandle readDataToEndOfFile] encoding:NSUTF8StringEncoding];

    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
