//
//  AboutDetailViewController.m
//  mutnav2
//
//  Created by walid nour on 6/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AboutDetailViewController.h"

@implementation UINavigationBar (CustomImage)

- (void)drawRect:(CGRect)rect {
    UIImage *image = [UIImage imageNamed: @"gg.png"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}
@end

@implementation AboutDetailViewController

@synthesize bgImage;
@synthesize pathArr,titleArr,pid;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    pathArr = [[NSMutableArray alloc]initWithObjects:@"a1",@"a2",@"a3",@"a4",@"a5",@"a6",@"a7",@"a8",@"a9",@"a10",@"a11",@"b1",@"b2",@"b3",@"b4",@"b5",@"b6",@"b7",@"b9", nil];
    titleArr =[[NSMutableArray alloc]initWithObjects:@"سفراتي",@"المصلى",@"المطوف",@"قيراط",@"لك القرار",@"سياحي",@"الملكة",@"رياض الصالحين",@"مفكرة الإسلام",@"جامع السنة",@"رمضان",@"شركاؤنا",@"عملاؤنا",@"رسائل الجوال",@"إدارة المحتوى",@"الهوية الإعلامية - التصميم والبرمجة",@"برامج نت",@"الدعم الفني",@"مدار البرمجة", nil];
    
    
   /*UINavigationBar *navBar = [self.navigationController navigationBar];
     if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
     {
         // set globablly for all UINavBars
         [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"gg.png"] forBarMetrics:UIBarMetricsDefault];
         
         // could optionally set for just this navBar
         //[navBar setBackgroundImage:...
     }*/
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboDetBg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320, 420);
    [self.view addSubview:bgImage];
    
 //   UIImageView *headImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboHead.png"]];
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboHead.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 40);
    [self.view addSubview:headImg];
    
 //   UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 270, 40)];
       UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 270, 40)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=UITextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text =[titleArr objectAtIndex:pid];
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.lineBreakMode=UILineBreakModeWordWrap;
    titleLabel.numberOfLines=0;
    [self.view addSubview:titleLabel];
    
    
 //   UIWebView *textWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 35, 320, 350)];
       UIWebView *textWeb = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 35, 320, 350)]autorelease];
    textWeb.delegate= self;
    

    textWeb.autoresizingMask = YES;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
        
    NSString *cssCode = @" \n"
    "body{font-family:Times New Roman,Times,serif;font-size:14px;line-height:17px;color:#000;direction:rtl;padding: 0px;margin: 0px;width:80%;margin-left:auto;margin-right:auto;background-color:transparent;text-align:justify;}\n"
    "h1 {color:#8b29ef;font-size:36px;}\n"
    "h3 {color:#8b29ef;font-size:24px;}\n"
    "h2 {color:#8b29ef;text-decoration:underline;}\n"
    "p  {margin:0px;padding: 5px 20px;font-size: 20px;line-height: 25px;}\n"
    "table{border:2px solid #000000;width:80%;margin: 0px auto;}\n"
    "ol,ul{font-size:18px;line-height:15px;}\n"
    "ul{list-style:disc;}\n"
    "ol ul ul,ul ul{list-style:circle;}\n"
    "ol li,ul li{line-height:30px;}\n"
    ".lbl{font-size:18px;line-height:15px;}\n"
    ".info_div{width:90%;margin:0 auto;text-align:center;line-height:20px;}\n"
    ".info_div table{border:none;}\n"
    ".info_div table tr{height:30px;}\n";
    
    NSString *htmlCode = [NSString stringWithFormat:@""
                          "<html> "
                          "     <head>"
                          "     <title></title> "
                          "     <style>%@</style></head>" //CSS
                          "     <body>%@</body>\n\n"
                          "</html>  \n",cssCode,[[self getDetails] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
    
    textWeb.opaque = NO;
    textWeb.backgroundColor = [UIColor clearColor];
    [textWeb loadHTMLString:htmlCode baseURL:baseURL];
    
    
    [self.view addSubview:textWeb];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    self.navigationController.navigationBar.hidden=YES;
    
    
}
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSString *)getDetails{
    NSString *pathh = [[NSBundle mainBundle] pathForResource:[pathArr objectAtIndex:pid] ofType:@"txt"];
	NSFileHandle *readHandle = [NSFileHandle fileHandleForReadingAtPath:pathh];
    
	NSString *htmlString = [[NSString alloc] initWithData: 
                            [readHandle readDataToEndOfFile] encoding:NSUTF8StringEncoding];
    return htmlString;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
