//
//  HaramViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 4/1/15.
//
//
#import <QuartzCore/QuartzCore.h>
#import "HaramViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
#import "HamlatViewController.h"

@interface HaramViewController ()

@end

@implementation HaramViewController
@synthesize mainTable,menuearr,imgarr,type,news_view,myWeb;

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    if (type==1) {
          [tracker set:kGAIScreenName value:@"خدمات الرئاسة Screen/Iphone "];
    }
    else if (type==2){
          [tracker set:kGAIScreenName value:@"شركاؤنا Screen/Iphone "];
    }
    else if (type==3){
          [tracker set:kGAIScreenName value:@"البث المباشر Screen/Iphone "];
    }
  
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    CGRect screensize=[[UIScreen mainScreen] bounds];
    if (type==1) {
        menuearr = [[NSMutableArray alloc]initWithObjects:@"أخبار الرئاسة",@"اقترح فكرة",@"بلغ عن منكرات", nil];
        imgarr = [[NSMutableArray alloc]initWithObjects:@"news.png",@"contact.png",@"report.png", nil];
    }
    else if (type==2){
   // menuearr = [[NSMutableArray alloc]initWithObjects:@"",@"",@"", nil];
   // imgarr = [[NSMutableArray alloc]initWithObjects:@"risa.png",@"",@"baldh.png", nil];
         menuearr = [[NSMutableArray alloc]initWithObjects:@"",@"", nil];
         imgarr = [[NSMutableArray alloc]initWithObjects:@"risa.png",@"", nil];
    }
    else if (type==3){
        menuearr = [[NSMutableArray alloc]initWithObjects:@"",@"",@"", nil];
        imgarr = [[NSMutableArray alloc]initWithObjects:@"live1.png",@"",@"live2.png", nil];
    }
    else if (type==4){
        menuearr = [[NSMutableArray alloc]initWithObjects:@"هل أنت حاج؟",@"",@"أم مسئول حملة؟", nil];
        imgarr = [[NSMutableArray alloc]initWithObjects:@"",@"",@"", nil];
    }
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];

    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    if (type==1) {
        titleLabel.text =@"خدمات الرئاسة";
    }
    else if (type==2){
        titleLabel.text =@"شركاؤنا";
    }
    else if (type==3){
        titleLabel.text =@"البث المباشر";
    }
    else if (type==4){
        titleLabel.text =@"حملتي";
    }
    
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text = @"الرئاسة العامة للحرم المكي والمسجد النبوي";

    if (type==1) {
        [_header_view addSubview:second_image];
         [_header_view addSubview:first_title];
    }
   
    [_header_view addSubview:header_image];
    [ _header_view  addSubview:titleLabel];
    [self.view addSubview:_header_view];
     mainTable = [[UITableView alloc] initWithFrame:CGRectMake(41, 140, 320, 300*highf) style:UITableViewStyleGrouped];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
        mainTable.rowHeight=60.0;
    mainTable.separatorColor = [UIColor clearColor];
    mainTable.hidden = NO;
    mainTable.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
    [self.view addSubview:mainTable];
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
    
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
        
        // [backButton release];
        
    }
    
}

/// pop the view from navigation to show the previous one.
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if (type==1) {
       
        cell.backgroundColor = [UIColor whiteColor];
        UIView *bgView = [[UIView alloc]initWithFrame:cell.frame];
        
        
        UILabel *bgLab = [[UILabel alloc]initWithFrame:CGRectMake(cell.frame.size.width-85, 0,10, 60)];
        bgLab.backgroundColor = [UIColor colorWithRed:0.914 green:0.914 blue:0.914 alpha:1] ;
        [bgView addSubview:bgLab];
        
        
        UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(cell.frame.size.width-100, 10, 40, 40)];
        [bgImg setImage:[UIImage imageNamed:[imgarr objectAtIndex:indexPath.row]]];
        [bgView addSubview:bgImg];
        
        UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(50, 10,150, 40)];
        titleLab.text =[menuearr objectAtIndex:indexPath.row];
        titleLab.textAlignment = NSTextAlignmentRight;
        titleLab.lineBreakMode =NSLineBreakByCharWrapping;
        titleLab.numberOfLines = 0;
        titleLab.textColor = [UIColor blackColor];
        titleLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
        [bgView addSubview:titleLab];
        
        UIImageView *arrImg = [[UIImageView alloc] initWithFrame:CGRectMake(10, 23, 14, 19)];
        [arrImg setImage:[UIImage imageNamed:@"arrow2.png"]];
        [bgView addSubview:arrImg];
        
        [cell.contentView addSubview:bgView];
        [cell addSubview:bgView];
    }
    else if (type==4) {
        if (indexPath.row != 1) {
        //    NSMutableDictionary *dic=[menuearr objectAtIndex:indexPath.row];
                       cell.backgroundColor = [UIColor whiteColor];

            
            
            UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 238, 40)];
            [bgView setImage:[UIImage imageNamed:@"item_bg.png"]];
            [cell.contentView addSubview:bgView];
            [cell addSubview:bgView];
            cell.backgroundColor = [UIColor clearColor];
            
            UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(20, 4,198, 36)];
            titleLab.text=[menuearr objectAtIndex:indexPath.row];
            titleLab.textAlignment = NSTextAlignmentCenter;
            titleLab.lineBreakMode =NSLineBreakByCharWrapping;
            titleLab.numberOfLines = 0;
            titleLab.textColor = [UIColor whiteColor];
            titleLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
            [cell addSubview:titleLab];
            return cell;
        }
     
    }

    
    else{
        
        cell.backgroundColor = [UIColor whiteColor];
        
        UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 230, 60)];
        [bgImg setImage:[UIImage imageNamed:[imgarr objectAtIndex:indexPath.row]]];
        [cell addSubview:bgImg];
        
       
    }

    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    if (type==1) {
        if(indexPath.row==0){
            SiteViewController *controller = [[SiteViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
            controller.urlString= @"http://www.gph.gov.sa/section/2/%D8%A7%D9%84%D8%A7%D8%AE%D8%A8%D8%A7%D8%B1";
            controller.title =[menuearr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:controller animated:YES];
        }
        
      
        else if(indexPath.row==1)
        {
            SiteViewController *controller = [[SiteViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
            controller.urlString= @"http://api.madarsoft.com/contact_us/v1/contact/suggest/?programid=28&platform=0";
             controller.type =3;
            controller.title =[menuearr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
        
        else if(indexPath.row==2)
        {
            SiteViewController *controller = [[SiteViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
            controller.urlString= @"http://api.madarsoft.com/contact_us/v1/contact/report/?programid=28&platform=0";
            controller.type =3;
            controller.title =[menuearr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:controller animated:YES];
            
        }
    }
    else if (type==2){
        if(indexPath.row==0){
            SiteViewController *controller = [[SiteViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
            controller.urlString= @"http://www.gph.gov.sa";
            controller.title =@"رئاسة الحرمين";
            [self.navigationController pushViewController:controller animated:YES];
        }
        
      
/*else if(indexPath.row==2)
        {
            SiteViewController *controller = [[SiteViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
            controller.urlString= @"http://www.momra.gov.sa/";
            controller.title =@"وزارة الشئون البلدية";
            [self.navigationController pushViewController:controller animated:YES];
            
        }*/
        
        
      
    }
    else if (type==3){
        
  news_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        news_view.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
        
        /* UIView *news_header=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
         news_header.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
         [_news_view addSubview:news_header];*/
        UILabel *header_lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width,30)];
        //  header_lbl.text=NSLocalizedString(@"muatefNews", @"News");
        header_lbl.text=@"قنوات";
        [header_lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
        header_lbl.textAlignment=NSTextAlignmentCenter;
        header_lbl.textColor=[UIColor whiteColor];
        [news_view addSubview:header_lbl];
        UIButton *back_btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
        back_btn.layer.cornerRadius=12.5;
        back_btn.backgroundColor=[UIColor clearColor];
        [back_btn setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
        [back_btn addTarget:self action:@selector(close_view:) forControlEvents:UIControlEventTouchUpInside];
        
        
    myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
        myWeb.delegate=self;
        [news_view setFrame:CGRectMake( -self.view.frame.size.width,  0,self.view.frame.size.width, self.view.frame.size.height)]; //notice this is OFF screen!
        [UIView beginAnimations:@"animateTableView" context:nil];
        [UIView setAnimationDuration:0.4];
        [self.view addSubview:news_view];
        [news_view setFrame:CGRectMake( 0, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
        [UIView commitAnimations];
        [news_view addSubview:myWeb];
        [news_view addSubview:back_btn];
        //  NSString *urlAddress = [urlarr objectAtIndex:indexPath.row];
        NSURL *url;
        if (indexPath.row==0) {
            url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video" ofType:@"html" inDirectory:@"www"]];
        }
        else{
            url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video2" ofType:@"html" inDirectory:@"www"]];
        }
        
        /*  NSString *URLString = [url absoluteString];
         NSString *queryString =[NSString stringWithFormat:@"?id=%@",urlAddress];
         NSString *URLwithQueryString = [URLString stringByAppendingString: queryString];
         NSURL *finalURL = [NSURL URLWithString:URLwithQueryString];*/
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:(NSTimeInterval)10.0 ];
        [myWeb loadRequest:request];
        //[self.view bringSubviewToFront:botimg];
    }
    else if (type==4){
        if(indexPath.row==0){
            HamlatViewController *controller = [[HamlatViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
            controller.type= 2;
            controller.title =@"حملتي";
            [self.navigationController pushViewController:controller animated:YES];
        }
        else if(indexPath.row==2)
        {
            SiteViewController *controller = [[SiteViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
             NSString *deviceUDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
            controller.urlString= [NSString stringWithFormat:@"http://204.187.101.75/mutawef2/login.aspx?device=%@&type=1",deviceUDID ];
            controller.type =2;
          //  controller.title =[menuearr objectAtIndex:indexPath.row];
            [self.navigationController pushViewController:controller animated:YES];
            
        }

    }
    
    
    
}

-(void)close_view:(id)sender{
    [UIView animateWithDuration:0.4 delay:0  options:0 animations: ^{
        [news_view setFrame:CGRectMake( -self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
    } completion: ^(BOOL completed) {
        [news_view removeFromSuperview];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
