//
//  NewEMViewController.h
//  mutnav2
//
//  Created by waleed on 6/17/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>

@interface NewEMViewController : UIViewController<UITextViewDelegate,UIScrollViewDelegate >

@property (retain,nonatomic) IBOutlet UIImageView *bgupdate;

@property (retain,nonatomic) IBOutlet UIView *timelineView;
@property (retain,nonatomic) IBOutlet UIScrollView *mainView;
@property (retain,nonatomic) IBOutlet UIImageView *bgImage;
@property (nonatomic,retain) IBOutlet UIImageView *elecmutawef;
@property(nonatomic,retain) IBOutlet UITextView *mytext;
@property(nonatomic,retain) IBOutlet NSMutableArray *arrdoaa;
@property(nonatomic,retain) IBOutlet NSMutableArray *arrmanasek;
@property(nonatomic,retain) NSMutableDictionary *sdic;
@property(nonatomic,retain) NSString *from;
@property(nonatomic,retain) IBOutlet UIButton *douBtn;
@property(nonatomic,retain) IBOutlet UIButton *manBtn;
@property(nonatomic,retain) IBOutlet UIButton *pushBtn;
@property(nonatomic,retain) IBOutlet UIButton *closeBtn;
@property(nonatomic,retain) IBOutlet UIButton *decBtn;
@property int imgindex;
@property int btnindex;
@property int type;
@property int hajindex;
-(IBAction) next_tawaf:(id)sender;
-(IBAction) previous_tawaf:(id)sender;
-(void)addfiletotextview:(NSString*)filename;
-(IBAction)changefile:(id) sender;
-(IBAction)changeView:(id)sender;
-(IBAction)closeView:(id)sender;
@property int tt;
@end
