//
//  MyListViewController.m
//  mutnav2
//
//  Created by Shaima Magdi on 11/16/14.
//
//

#import "MyListViewController.h"
#import "MyTextViewController.h"
#import "MyInfoViewController.h"
#import "MyMapViewController.h"
#import "Arrays.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

@interface MyListViewController ()

@end

@implementation MyListViewController

@synthesize mainTable;
@synthesize mainTitle,secondTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ Screen/Iphone",mainTitle  ] ];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad
{
    /*!
     @discussion it draw the footer & header view programmatically and set title and second title according property that was set before
     */
    
    [super viewDidLoad];
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    // draw headerView
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text= NSLocalizedString(mainTitle, @"");
    
    //???:must check .. if _isChild is equal YES that mean this view is called from itself
    if (_isChild ==YES) {
        UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
        [second_image setImage:[UIImage imageNamed:@"title.png"]];
        UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
        first_title.backgroundColor=[UIColor clearColor];
        first_title.tag=5;
        first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
        
        first_title.textColor=[UIColor whiteColor];
        first_title.textAlignment=NSTextAlignmentCenter;
        first_title.text=NSLocalizedString(secondTitle, @"") ;
        [_header_view addSubview:second_image];
        [_header_view addSubview:first_title];
        
    }
    /// set table view data
    if([self.navigationController.viewControllers objectAtIndex:0] != self){
        mainTable = [[UITableView alloc] initWithFrame:CGRectMake(41, 140, 238, 300*highf) style:UITableViewStyleGrouped];
    }
    else{
        mainTable = [[UITableView alloc] initWithFrame:CGRectMake(41, 120, 238, 300*highf) style:UITableViewStyleGrouped];
    }
    [_header_view addSubview:header_image];
    [ _header_view  addSubview:titleLabel];
    [self.view addSubview:_header_view];
    
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=50.0;
    mainTable.separatorColor = [UIColor clearColor];
    mainTable.hidden = NO;
    mainTable.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
    
    [self.view addSubview:mainTable];
    
    //draw footerView
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
    
}

/// pop the view from navigation to show the previous one.
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    /// draw back botton
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
    }
    
}

#pragma mark Table view delegate & dataSource
// number of sections.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

/// number of rows @warning Note that it must be count -1 as the first index hold type not actual data

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_listArray count]-1;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    // Configure the cell...
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, 238, 40)];
    [bgView setImage:[UIImage imageNamed:@"item_bg.png"]];
    [cell.contentView addSubview:bgView];
    [cell addSubview:bgView];
    cell.backgroundColor = [UIColor clearColor];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(20, 4,198, 36)];
    titleLab.text=NSLocalizedString([_listArray objectAtIndex:indexPath.row+1], @"");
    titleLab.textAlignment = NSTextAlignmentCenter;
    titleLab.lineBreakMode =NSLineBreakByCharWrapping;
    titleLab.numberOfLines = 0;
    titleLab.textColor = [UIColor whiteColor];
    titleLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
    [cell addSubview:titleLab];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // send message to goToTargetView and pass indexpath.row +1 as parameter
    [self goToTargetView:(int)indexPath.row +1];
}

/*!
 * @discussion this method take the text and search in "Arrays Class" for array or dictoinary for the targetView
 * @param selectedIndex it is int use to search in array of strings
 * @warning NOTE if you want to use this implementation you must pass [indexPath.row +1]
 */
-(void)goToTargetView :(int)selectedIndex{
    
    NSString *selectorName=_listArray[selectedIndex];
    SEL s = NSSelectorFromString(selectorName);
    if ([Arrays performSelector:s]) {
        NSArray *mylist= [Arrays performSelector:s];
        if ([mylist[0]isEqualToString:@"list"]) {
            MyListViewController *list=[[MyListViewController alloc]init];
            list.listArray=mylist;
            list.mainTitle=mainTitle;
            list.secondTitle=_listArray[selectedIndex];
            list.isChild=YES;
            [self.navigationController pushViewController:list animated:YES];
            
        }else if([mylist[0]isEqualToString:@"text"]){
            MyTextViewController *texView =[[MyTextViewController alloc]initWithNibName:@"MyTextViewController" bundle:nil];
            texView.textArray=mylist;
            texView.mainTitle=mainTitle;
            texView.secondTitle=_listArray[selectedIndex];
            [self.navigationController pushViewController:texView animated:YES];
            
        }else if([mylist[0]isEqualToString:@"info"]){
            MyInfoViewController *infoView =[[MyInfoViewController alloc]initWithNibName:@"MyInfoViewController" bundle:nil];
            
            SEL s = NSSelectorFromString(mylist[1]);
            infoView.infoDic=[Arrays performSelector:s];
            [self.navigationController pushViewController:infoView animated:YES];
            
        }else if([mylist[0]isEqualToString:@"map"]){
            
            MyMapViewController *mapView =[[MyMapViewController alloc]initWithNibName:@"MyMapViewController" bundle:nil];
            mapView.selectedRow=selectedIndex;
            mapView.mapTitle=_listArray[selectedIndex];
            [self.navigationController pushViewController:mapView animated:YES];
            
        }
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return Portrait interface
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
