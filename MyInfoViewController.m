//
//  MyInfoViewController.m
//  mutnav2
//
//  Created by Shaima Magdi on 11/18/14.
//
//

#import "MyInfoViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"


@interface MyInfoViewController (){
}

@end

@implementation MyInfoViewController
@synthesize  mainTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"hospitals Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidLoad
{
    /*!
     @discussion it draw the footer & header view programmatically and set title and hospitalName title according property that was set before & adding information of hospital to mainTable
     */
    
    [super viewDidLoad];
    
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    // draw header view
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =NSLocalizedString(@"hospitals", @"");
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor colorWithRed:0.294 green:0.255 blue:0.157 alpha:1] /*#4b4128*/;
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text=NSLocalizedString([_infoDic valueForKey:@"name"], @"");
    [_header_view addSubview:second_image];
    [_header_view addSubview:header_image];
    [_header_view addSubview:first_title];
    [ _header_view  addSubview:titleLabel];
    [self.view addSubview:_header_view];
    
    // right image
    UIImageView *info_image=[[UIImageView alloc] initWithFrame:CGRectMake(140, 150, 160, 38)];
    [info_image setImage:[UIImage imageNamed:@"infolable.png"]];
    UILabel *info_title=[[UILabel alloc] initWithFrame:CGRectMake(160, 154, 100, 30)];
    info_title.backgroundColor=[UIColor clearColor];
    info_title.tag=5;
    info_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    info_title.textColor=[UIColor colorWithRed:0.22 green:0.212 blue:0.216 alpha:1] /*#383637*/;
    info_title.textAlignment=NSTextAlignmentCenter;
    info_title.text=NSLocalizedString(@"generalInfo", @"");
    [self.view addSubview:info_image];
    [self.view addSubview:info_title];
    
     // draw footer view
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    [self.view addSubview:_footer_view];
    
    // init & set data to mainTable
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(80,200, 220, 200) style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 0.8;
    mainTable.rowHeight=40;
    mainTable.hidden = NO;
    mainTable.scrollEnabled = NO;
    mainTable.allowsSelection = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:mainTable];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
    
    // draw back button
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame =CGRectMake(10, 20, 30, 30);
    [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    
    
    
}

/// pop the view from navigation to show the previous one.
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 *  this method used to make a call to the hospital phone.
 *  take sender number and add the key then open dial application.
 *  @param sender phoneBtn that its tag was set to phoneNumber.
 */
-(IBAction)buttonContact:(id)sender{
    
    NSString *contactstring=[[NSString alloc] initWithFormat:@"tel://0096612%@",NSLocalizedString([_infoDic valueForKey:@"phone"], @"")];
   //  NSLog(@"%ld",(long)[sender tag]);
    NSLog(@"%@",contactstring);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:contactstring]];
}

#pragma mark - table view delegate
// number of sections.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
// number of rows.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Configure the cell...
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [mainTable dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    // static data used as subtitle(key) for dictionary values
    NSArray *data=@[@"beds",@"phone",@"fax",@"address"];
    NSString *keystring=data[indexPath.row];
    NSString *finalstring=[[NSString alloc] initWithFormat:@"%@",[_infoDic valueForKey:keystring]];
    NSLog(@"%@",NSLocalizedString(finalstring, @""));
    if (indexPath.row ==1 ) {
        /// phone number cell  .. draw & add contactBtn a
        NSString *contactstring=[[NSString alloc] initWithFormat:@"%@",[_infoDic valueForKey:@"phone"]];
        UIButton *contactBtn=[[UIButton alloc] initWithFrame:CGRectMake(0,5, 30,30)];
        contactBtn.alpha = 1;
        contactBtn.tag = [contactstring intValue];
        [contactBtn setBackgroundImage:[UIImage imageNamed:@"contactBtn.png"]	forState:UIControlStateNormal];
        [contactBtn addTarget:self action:@selector(buttonContact:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:contactBtn];
    }
    // draw label to hold subtitle from data array according indexPath
    UILabel *key_title=[[UILabel alloc] initWithFrame:CGRectMake(180, 5, 40, 30)];
    key_title.backgroundColor=[UIColor clearColor];
    key_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    key_title.textColor=[UIColor colorWithRed:0.522 green:0.651 blue:0.004 alpha:1] ;
    key_title.textAlignment=NSTextAlignmentRight;
    key_title.text=NSLocalizedString(keystring, @"") ;
    key_title.lineBreakMode = NSLineBreakByCharWrapping;
    key_title.numberOfLines=0;
    [cell addSubview:key_title];
    
    // draw label to hold values of dic according indexPath
    UILabel *info_title=[[UILabel alloc] initWithFrame:CGRectMake(30, 5, 150, 30)];
    info_title.backgroundColor=[UIColor clearColor];
    info_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    info_title.textColor=[UIColor colorWithRed:0.341 green:0.341 blue:0.341 alpha:1] ;
    info_title.textAlignment=NSTextAlignmentLeft;
    info_title.text=NSLocalizedString(finalstring, @"") ;
    info_title.lineBreakMode = NSLineBreakByCharWrapping;
    info_title.numberOfLines=0;
    [cell addSubview:info_title];
   
   
    
    return cell;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return Portrait interface
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
