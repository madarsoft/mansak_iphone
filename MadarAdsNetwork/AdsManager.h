//
//  AdsManager.h
//  testadsiphone
//
//  Created by hossam fathy on 12/2/14.
//  Copyright (c) 2014 hossam fathy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CloseView.h"
#import "ObjectFactory.h"
//hf


@interface AdsManager : NSObject

@property(nonatomic,strong)ObjectFactory *factory;
@property(nonatomic,strong)CloseView *closing;


+ (id)sharedManagerWithProgramID :(int) programID andPlaceId:(int) placeId;
+ (id)sharedManagerWithProgramID :(int) programID andPlaceId:(int) placeId andGoogleTrackingID :(NSString*) googleTrackingId;
+(AdsManager*)getInstance;
-(void)initDefaultClosingView;
-(void)initCustomClosingView:(UIView*)view;

-(void)getBannerAdToRoot:(UIViewController*)root  andCompletionHandler:(void(^)(UIView*))completionHandler;
-(void)getBannerAdToRoot:(UIViewController*)root withPositionNumber:(int)positionNumber  andCompletionHandler:(void(^)(UIView*))completionHandler;
-(void)getInterstitialAdToRoot:(UIViewController*)root andCompletionHandler:(void(^)(UIView*))completionHandler;
-(void)getInterstitialAdToRoot:(UIViewController*)root withPositionNumber:(int)positionNumber andCompletionHandler:(void(^)(UIView*))completionHandler;


@end
