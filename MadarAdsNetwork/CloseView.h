//
//  CloseView.h
//  testadsiphone
//
//  Created by Shaima Magdi on 12/21/14.
//  Copyright (c) 2014 hossam fathy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CloseView : UIView

- (instancetype)init;
- (instancetype)initWithCustomView:(UIView *)view;
@end
