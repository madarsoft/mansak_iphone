//
//  MyMapViewController.m
//  mutnav2
//
//  Created by Shaima Magdi on 11/20/14.
//
//

#import "MyMapViewController.h"
#import "Reachability.h"
#import "MapViewAnnotation.h"
#import "Global.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

@interface MyMapViewController (){
    /**
     *  @brief MapViewController Mapview this display map when the target is map.
     */
    NSString *plist;
    /**
     *  @brief MapViewController Mapview this display map when the target is map.
     */
    IBOutlet MKMapView *myMapView;
}

@end

@implementation MyMapViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Maps Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)viewDidLoad {
    /*!
     @discussion it draw the footer & header view programmatically and set title and mapName title according property that was set before
     * then check if the selected row :-
     -if it is image : display image and add GestureRecognizer to it.
     -if it is mapKit : display map and add annotation according its plist.
     */
    
    [super viewDidLoad];
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    
    // draw header view
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text =NSLocalizedString(@"maps", @"");
    
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text=NSLocalizedString(_mapTitle, @"");
    [_header_view addSubview:second_image];
    [_header_view addSubview:header_image];
    [_header_view addSubview:first_title];
    [ _header_view  addSubview:titleLabel];
    
    // check selected row if 1 or 6 it will display image and add "GestureRecognizer"
   /* if (_selectedRow==1 || _selectedRow==6) {
        
        [self layoutScrollImages];
    }
    else
    {
        // else then must display map .. so check if there internet connection or not
        Reachability *curReach = [Reachability reachabilityForInternetConnection];
        [curReach startNotifier];
        NetworkStatus netStatus = [curReach currentReachabilityStatus];
        
        // if there isn't problems in connection .. call "displayMap"
        if (netStatus !=NotReachable) {
            [self displayMap];
        }
        else {
            //else display alert to let user know the problem.
            NSString *alertTitle=NSLocalizedString(@"warning", @"");
            NSString *alertMsg=NSLocalizedString(@"noInternet", @"");
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertTitle                                                              message:alertMsg
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
        }
    }*/
    
    // else then must display map .. so check if there internet connection or not
    Reachability *curReach = [Reachability reachabilityForInternetConnection];
    [curReach startNotifier];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    
    // if there isn't problems in connection .. call "displayMap"
    if (netStatus !=NotReachable) {
        [self displayMap];
    }
    else {
        //else display alert to let user know the problem.
        NSString *alertTitle=NSLocalizedString(@"warning", @"");
        NSString *alertMsg=NSLocalizedString(@"noInternet", @"");
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertTitle                                                              message:alertMsg
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }

    
    [self.view addSubview:_header_view];
    
    // draw footer view
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    [self.view addSubview:_footer_view];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
    [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        //draw back button
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
    }
    
}

/// pop the view from navigation to show the previous one.
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}
/**
 *  this method check the selectedRow var to indicate which map should display and set plist of locations to "setAnnotations" method.
 */
- (void)displayMap{
    @autoreleasepool {
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        CLLocationCoordinate2D location;
        span.latitudeDelta=0.02;
        span.longitudeDelta=0.02;
        switch (_selectedRow) {
            case 1:
                location.latitude = 21.4266667;
                location.longitude = 39.8261111;
                plist = @"MakaMap";
                break;
            case 2:
                location.latitude = 21.4130812;
                location.longitude = 39.8937178;
                plist = @"MenaMap";
                break;
            case 3:
                location.latitude = 21.3923041;
                location.longitude = 39.9015713;
                plist = @"MozdalefaMap";
                break;
            case 4:
                location.latitude = 21.3479033;
                location.longitude = 39.9741411;
                plist = @"ArafatMap";
                break;
                
        }
        region.span=span;
        region.center=location;
        
        // init mapView
        myMapView = [[MKMapView alloc] initWithFrame:self.view.bounds];
        myMapView.delegate =self;
        myMapView.mapType = MKMapTypeStandard;
        [self.view addSubview:myMapView];
        
        //set annotations
        [self setAnnotations];
        
        //make map open on target location
        [myMapView setRegion:region animated:TRUE];
        [myMapView regionThatFits:region];
    }
}

/**
 *  this method get locations data(name ,latitude,longitude) from plist and add annotation to map
 */
-(void)setAnnotations{
    NSString *path=[[NSBundle mainBundle] pathForResource:plist ofType:@"plist"];
    NSArray *content=[NSArray arrayWithContentsOfFile:path];
    
    for (NSDictionary *dic in content ) {
        NSString *title=NSLocalizedString([dic valueForKey:@"name"], @"") ;
        CLLocationCoordinate2D location;
        location.latitude = [[dic valueForKey:@"latitude"]doubleValue];
        location.longitude = [[dic valueForKey:@"longitude"]doubleValue];
        
        // init annotationObj and pass name &location to it.
        MapViewAnnotation *newAnnotation = [[MapViewAnnotation alloc] initWithTitle:title andCoordinate:location];
        // add annotationObj to map
        [myMapView addAnnotation:newAnnotation];
    }
    
}

/**
 *  this method create imageView and add image & gesture according selectedRow.
 */
- (void)layoutScrollImages{
    // init imgView and set target location to  map center
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0,_header_view.frame.size.height+10,self.view.frame.size.width ,self.view.frame.size.height-(_header_view.frame.size.height+_footer_view.frame.size.height+40))];
    
    if (_selectedRow==6) {
        // add pinGesture to add zooming to image
        UIPinchGestureRecognizer *pinchTap = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
        
        [_scrollView addGestureRecognizer:pinchTap];
        
        //set imgName to imageView's image
       imgView.image=[UIImage imageNamed:@"mokhatat.png"];
        
    }else if(_selectedRow==1){
        // add tapGesture to display data about this map
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doSingleTap)];
        singleTap.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:singleTap];
        
        //set imgName to imageView's image
         imgView.image=[UIImage imageNamed:@"ehraming.png"];
        
    }
    [self.view addSubview:imgView];
    
}
/**
 *  this method handel "PinchGestureRecognizer" to add zooming to imageView
 *
 *  @param recognizer recognizer .
 */
- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer{
    NSLog(@"handlePinch pressed");
}

/**
 *  this method handel "TapGestureRecognizer" to display description text to imageView.
 *  it creat view and add textView & closeBtn to it. then display the view.
 */
- (void) doSingleTap{
    // init temporary view
    UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(20, 150, 280, 260)];
    tmpView.backgroundColor=[UIColor whiteColor];
    
    // get saved textSize from userDefault.
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"size_text"];
    text_size=[savedValue intValue];
    
    // init temporary TextView and add locaization sring to it.
    UITextView *tmpText = [[UITextView alloc]initWithFrame:CGRectMake(0,10 , 280, 250)];
    tmpText.backgroundColor=[UIColor clearColor];
    tmpText.text=NSLocalizedString(@"mo5attMas3erStr", @"");
    tmpText.textAlignment = NSTextAlignmentRight;
    tmpText.editable = NO;
    tmpText.textColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    tmpText.font = [UIFont fontWithName:@"Arial" size:text_size];
    [tmpView addSubview:tmpText];
    
    // draw closeBtn and add it to temporary view
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    closeBtn.frame = CGRectMake(0,0, 30, 30);
    closeBtn.backgroundColor = [UIColor clearColor];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"Close.png"]	forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeBtn:) forControlEvents:UIControlEventTouchUpInside];
    [tmpView addSubview:closeBtn];
    
    // display the temporary view
    [self.view addSubview:tmpView];
}
/**
 *  simple method that it remove the temporary view from the superView
 *
 *  @param sender closeBtn
 */
- (IBAction)closeBtn:(id)sender{
    [[sender superview]removeFromSuperview];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return Portrait interface
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
