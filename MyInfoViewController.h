//
//  MyInfoViewController.h
//  mutnav2
//
//  Created by Shaima Magdi on 11/18/14.
//
//

/*!
 @discussion This is custom view used to display information about hospitals (can use it as key & value)
    it display items as a list (table view) according to an dictionary was set to it.

 
 
 use this class :-
 1- You just need to import it to your view.
 2- set dictionary of this class .
 3-set the other items (hospitalName) before push this view.

 */


#import <UIKit/UIKit.h>

@interface MyInfoViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UITableView *mainTable;
@property (strong,nonatomic)  NSString *hospitalName;

/*!
* @brief InfoViewController NSDictionary that hold all needed data.
*/
@property (strong ,nonatomic) NSDictionary *infoDic;


@end
