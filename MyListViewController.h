//
//  MyListViewController.h
//  mutnav2
//
//  Created by Shaima Magdi on 11/16/14.
//
//

/*!
 @discussion This class display items as a list (table view) according to an array was set to it.
 in action of selected row (didSelectedRowAtIndex) you can check the var (target type at index 0) and determine the next view even if it will the same view (you can call this class from itself ).
 
 
 use this class :-
 1- You just need to import it to your view.
 2- set array of this class with type of target view at index zero.
 3- if the class is the parent set isChild =no;
 4-set the other items (mainTitle,secondTitle) before push this view.

 
 */


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Global.h"

@interface MyListViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong)UINavigationController *navController;
@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UITableView *mainTable;
@property (nonatomic, strong) NSString *mainTitle;
@property (nonatomic, strong) NSString *secondTitle;

/*!
 *@brief isChild used to check if that is it first one to see list or that list you called the list from list --> as second one (child) has image title the first one not.
 */
@property BOOL isChild;
/*!
 *@brief MyListViewController list object that hold all data need to target view.
 */
@property(nonatomic,strong)NSArray *listArray;





@end
