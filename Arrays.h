//
//  Arrays.h
//  mutnav2
//
//  Created by Shaima Magdi on 11/17/14.
//
//

#import <Foundation/Foundation.h>

@interface Arrays : NSObject
/*
 this class used to save all lists in app
 1- zyara   2-se7a    3-gna2ez 4-maps -- and what they containe
 
 each array has target type at index 0 then the rest it is the actual array
 
 you check index 0 if hold "list" then you push MyListViewController
                   if hold "text" then you push MyTextViewController
                   if hold "map" then you push MyMapViewController
                   if hold "info" then you push MyInfoViewController
 and set the array of target view with the rest -->(from index 1 to the end)
 each index it is method name in this class use to get array or dictionary
 and each value in the array or dic it is name of method and so on to reach to the last view you needed (text or mapview or infolist)
 
 
 to use this class :-
 1- You just need to import it to your view and all the methods was static.
 2- Use selected index as name of method by (SEL FromString method) and you will get array or dic
 3 - NOTE when you set array of target you must set it from 1 not 0 (as 0 has target).
 4- Each time of useing the array check index 0 to determine what the view should present.
 
 */


@end
