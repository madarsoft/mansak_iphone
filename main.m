//
//  main.m
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[]) {
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil,NSStringFromClass([AppDelegate class]));
        return retVal;
    }
}
